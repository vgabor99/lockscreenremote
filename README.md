## Lock Screen Remote (discontinued)

Lock Screen Remote is a remote control app for the [Logitech Harmony Hub](https://support.myharmony.com/en-us/hub). It allows controlling home media devices (TV set, Hi-Fi equipment, video and audio streaming devices, smart TV boxes, game consoles) with the Android media controls. As media controls live in a notification, visible on the lock screen, this unusual choice of UI brings home media control to the lock screen, without the need to unlock the phone.

![](screenshots/phone_lock_screen.png)

## Motivation

The Logitech Harmony Hub was a programmable, activity-based remote hub that controls media devices. The Android app provided by Logitech was unsatisfactory. As the device exposes an open API to the local network, I decided to write a replacement app.

## Technical details

The Harmony Hub exposes an (undocumented, officially unsupported) XMPP API. Using reverse engineered parts of the protocol available on the internet, as well as my own reverse engineering efforts, Lock Screen Remote does the following:

- Detects Harmony Hub devices on the local network
- Retrieves the Hub configuration (controlled devices, activities, commands)
- Automatically connects to the active Hub
- Performs two-way communication to the connected Hub, displaying status information, and sending commands to control the Hub
- Heuristics are used to provide setup-free, hassle-free operation. Everything works out of the box (although users have the ability to customize the media controls - see [screenshots](screenshots) for an example of how button customization UI looks).

## Discontinuation

Logitech discontinuted the Logitech Harmony Hub product line in 2021. As a consequence, this project is also discontinued and will not receive any updates. No new versions will be published to Google Play.
