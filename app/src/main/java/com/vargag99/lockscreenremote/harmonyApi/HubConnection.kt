package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.generic.toProperties
import org.jivesoftware.smack.*
import org.jivesoftware.smack.filter.PacketIDFilter
import org.jivesoftware.smack.packet.IQ
import org.jivesoftware.smack.packet.Packet
import org.jivesoftware.smack.provider.ProviderManager
import timber.log.Timber
import java.io.IOException

/**
 * Hub connection.
 * After logging in we can send commands.
 */
internal class HubConnection(host: String, port: Int, connectionListener: ConnectionListener?, hubEventListener: HubEventProvider.HubEventListener?) {
    private val connection: XMPPConnection
    private var loggedInUser: String? = null // Logged in username.

    init {
        val cc = ConnectionConfiguration(host, port)
        cc.socketFactory = HubSocketFactory(SOCKET_CONNECT_TIMEOUT_MILLIS)
        cc.isDebuggerEnabled = BuildConfig.DEBUG
        connection = XMPPConnection(cc)
        connection.addConnectionListener(connectionListener)
        if (ProviderManager.getInstance().getIQProvider(OaIq.ELEMENT_NAME, OaIq.NAMESPACE) == null) {
            ProviderManager.getInstance().addIQProvider(OaIq.ELEMENT_NAME, OaIq.NAMESPACE, OaIqProvider())
        }

        if (ProviderManager.getInstance().getExtensionProvider(HubEvent.ELEMENTNAME, HubEvent.NAMESPACE) == null) {
            ProviderManager.getInstance().addExtensionProvider(HubEvent.ELEMENTNAME, HubEvent.NAMESPACE, HubEventProvider(hubEventListener))
        } else {
            (ProviderManager.getInstance().getExtensionProvider(HubEvent.ELEMENTNAME, HubEvent.NAMESPACE) as HubEventProvider).hubEventListener = hubEventListener
        }
    }

    @Throws(XMPPException::class)
    fun login(username: String?, password: String?) {
        Timber.d("login")
        try {
            connection.connect()
            SASLAuthentication.supportSASLMechanism("PLAIN", 0)
            connection.login(username, password)
            loggedInUser = username
        } catch (e: Exception) {
            connection.disconnect()
            throw e
        }

    }

    @Throws(Exception::class)
    fun requestSessionId(loginToken: String?): Data.SessionId {
        Timber.d("requestSessionId")

        val iq = SessionTokenPacket(loginToken).apply {
            from = loggedInUser
            type = IQ.Type.GET
        }
        val response = connection.sendReceivePacket(iq)
        val sessionId = response.data ?: throw IOException("Invalid session id")
        return parseSessionId(sessionId)
    }

    @Throws(Exception::class)
    private fun parseSessionId(text: String): Data.SessionId {
        // <![CDATA[serverIdentity=ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1:hubId=106:identity=ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1:status=succeeded:protocolVersion={XMPP="1.0", HTTP="1.0", RF="1.0", WEBSOCKET="1.0"}:hubProfiles={Harmony="2.0"}:productId=Pimento:friendlyName=Harmony Hub]]>
        val props = text.toProperties(":", "=")
        return Data.SessionId(
                status = props.getProperty("status")!!,
                identity = props.getProperty("identity")!!,
                friendlyName = props.getProperty("friendlyName")!!
        )
    }

    @Throws(Exception::class)
    fun sendHubCommand(actionType: String, command: String, waitForResponse: Boolean): String? {
        Timber.d("sendHubCommand actionType=$actionType, command=$command")

        val iq = HubCommandPacket(actionType, command).apply {
            type = IQ.Type.GET // The original harmony app uses a "RENDER" request instead of "GET. but it seems to work anyway.
            from = loggedInUser
        }

        return if (waitForResponse) {
            connection.sendReceivePacket(iq).data
        } else {
            connection.sendPacket(iq)
            null
        }
    }

    private fun XMPPConnection.sendReceivePacket(packet: Packet): OaIq {
        val packetId = packet.packetID
        val collector = this.createPacketCollector(PacketIDFilter(packetId))
        val timeout = PACKET_RESPONSE_TIMEOUT_MILLIS
        try {
            this.sendPacket(packet)
            return collector.nextResult(timeout) as OaIq?
                    ?: throw IOException("No response in $timeout ms")
        } finally {
            collector.cancel()
        }
    }

    fun disconnect() {
        Timber.d("disconnect")
        loggedInUser = null
        connection.disconnect()
    }

    private inner class HubCommandPacket internal constructor(private val actionType: String,
                                                              private val command: String) : IQ() {

        override fun getChildElementXML(): String {
            return if (command.isEmpty()) {
                "<oa xmlns=\"connect.logitech.com\" mime=\"vnd.logitech.harmony/vnd.logitech.harmony.engine?" + this.actionType + "\"/>"
            } else {
                "<oa xmlns=\"connect.logitech.com\" mime=\"vnd.logitech.harmony/vnd.logitech.harmony.engine?" + this.actionType + "\">" + this.command + "</oa>"
            }
        }
    }

    private inner class SessionTokenPacket internal constructor(private val token: String?) : IQ() {

        override fun getChildElementXML(): String {
            return "<oa xmlns=\"connect.logitech.com\" mime=\"vnd.logitech.connect/vnd.logitech.pair\">token=" + this.token + ":name=foo#iOS6.0.1#iPhone" + "</oa>"
        }
    }

    companion object {
        private const val SOCKET_CONNECT_TIMEOUT_MILLIS = 10000
        private const val PACKET_RESPONSE_TIMEOUT_MILLIS = 2000L
    }
}
