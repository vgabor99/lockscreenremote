package com.vargag99.lockscreenremote.harmonyApi

import org.jivesoftware.smack.packet.IQ
import org.jivesoftware.smack.provider.IQProvider
import org.xmlpull.v1.XmlPullParser
import timber.log.Timber

/**
 * IQ provider that can parse the "oa" extension that the harmony hub uses.
 * Sets the content of the extension into an OaIq packet.
 */
class OaIqProvider : IQProvider {

    @Throws(Exception::class)
    override fun parseIQ(parser: XmlPullParser): IQ {
        var oaText: String? = null
        var done = false
        var errorResponse = false

        //check the returned status code is 1xx or 2xx
        for (i in 0 until parser.attributeCount) {
            val attr = parser.getAttributeName(i)
            val attrVal = parser.getAttributeValue(i)

            if (attr == "errorcode" && (attrVal == null || attrVal.isEmpty() || Integer.parseInt(attrVal) >= 300)) {
                Timber.e("errorcode: %s", attrVal!!)

                for (j in 0 until parser.attributeCount) {
                    if (parser.getAttributeName(j) == "errorstring") {
                        Timber.e("error on parser: %s", parser.getAttributeValue(j))
                        break
                    }
                }
                errorResponse = true
                break
            }
        }

        //get the oa tag text.
        //make sure to never go over the end tag of the oa extension or the packet will get lost.
        while (!done && parser.next() != XmlPullParser.END_DOCUMENT) {
            val name = parser.name

            when (parser.eventType) {
                XmlPullParser.TEXT -> if (!errorResponse) {
                    oaText = parser.text
                }

                XmlPullParser.END_TAG -> {
                    done = OaIq.ELEMENT_NAME.equals(name, ignoreCase = true)
                }
            }
        }

        return OaIq(oaText)
    }

}