package com.vargag99.lockscreenremote.harmonyApi

sealed class ConnectionEvent {
    object ConnectionClosed : ConnectionEvent()
    data class ConnectionClosedOnError(val exception: Exception) : ConnectionEvent()
    data class ReconnectingIn(val i: Int) : ConnectionEvent()
    object ReconnectionSuccessful : ConnectionEvent()
    data class ReconnectionFailed(val exception: Exception) : ConnectionEvent()
}