package com.vargag99.lockscreenremote.harmonyApi

interface IHubSessionFactory {
    fun createHubSession(host: String, port: Int): IHubSession
}