package com.vargag99.lockscreenremote.harmonyApi

import java.io.IOException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

import javax.net.SocketFactory

/**
 * Socket factory that overrides connect timeout.
 */
internal class HubSocketFactory(private var connectTimeout: Int) : SocketFactory() {

    @Throws(IOException::class)
    override fun createSocket(host: String, port: Int): Socket {
        return HubSocket(host, port, connectTimeout)
    }

    @Throws(IOException::class)
    override fun createSocket(host: String, port: Int, localHost: InetAddress, localPort: Int): Socket {
        return HubSocket(host, port, localHost, localPort, connectTimeout)
    }

    @Throws(IOException::class)
    override fun createSocket(host: InetAddress, port: Int): Socket {
        return HubSocket(host, port, connectTimeout)
    }

    @Throws(IOException::class)
    override fun createSocket(address: InetAddress, port: Int, localAddress: InetAddress, localPort: Int): Socket {
        return HubSocket(address, port, localAddress, localPort, connectTimeout)
    }

    private inner class HubSocket : Socket {
        var connectTimeout: Int = 0

        @Throws(IOException::class)
        constructor(host: String, port: Int, connectTimeout: Int) {
            this.connectTimeout = connectTimeout
            this.connect(InetSocketAddress(InetAddress.getByName(host), port), connectTimeout)
        }

        @Throws(IOException::class)
        constructor(host: String, port: Int, localHost: InetAddress, localPort: Int, connectTimeout: Int) {
            this.connectTimeout = connectTimeout
            this.bind(InetSocketAddress(localHost, localPort))
            this.connect(InetSocketAddress(InetAddress.getByName(host), port), connectTimeout)
        }

        @Throws(IOException::class)
        constructor(var2: InetAddress, var3: Int, connectTimeout: Int) {
            this.connectTimeout = connectTimeout
            this.connect(InetSocketAddress(var2, var3), connectTimeout)
        }

        @Throws(IOException::class)
        constructor(address: InetAddress, port: Int, localAddress: InetAddress, localPort: Int, connectTimeout: Int) {
            this.connectTimeout = connectTimeout
            this.bind(InetSocketAddress(localAddress, localPort))
            this.connect(InetSocketAddress(address, port), connectTimeout)
        }

        @Throws(IOException::class)
        override fun connect(endpoint: SocketAddress) {
            super.connect(endpoint, this.connectTimeout)
        }

        @Throws(IOException::class)
        override fun connect(address: SocketAddress, timeout: Int) {
            super.connect(address, this.connectTimeout)
        }
    }
}
