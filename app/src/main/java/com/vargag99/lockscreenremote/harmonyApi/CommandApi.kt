package com.vargag99.lockscreenremote.harmonyApi

import androidx.annotation.Nullable
import com.google.gson.Gson
import com.vargag99.lockscreenremote.generic.Callback
import com.vargag99.lockscreenremote.generic.Transformer
import timber.log.Timber
import java.util.regex.Pattern

/**
 * Command API - hub commands we call (or post).
 */
class CommandApi(private val hubSession: HubSession) {

    fun sendButtonPress(time: Long, action: String) {
        Timber.i("sendButtonPresss buttonCommand=$action")
        val act = action.replace(":".toRegex(), "::") // Weird escaping of colons a'la Logitech.
        hubSession.sendCommand("holdAction", "action=$act:status=press:timestamp=$time", false, null)
        hubSession.sendCommand("holdAction", "action=" + act + ":status=release" + ":timestamp=" + time + 500, false, null)

        /*
            <iq id="Ca0Lf-23" from="ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1@connect.logitech.com/gatorade." type="get">
                <oa xmlns="connect.logitech.com" mime="vnd.logitech.harmony/vnd.logitech.harmony.engine?holdAction">
                    action={"command"::"VolumeDown","type"::"IRCommand","deviceId"::"38896470"}:status=press:timestamp=1489578000161
                </oa>
            </iq>
         */
    }

    fun startActivity(activityId: String) {
        Timber.i("startActivity activityId=$activityId")
        hubSession.sendCommand("startActivity", "activityId=$activityId:timestamp=0", false, null)

        /*
            <iq type="startActivity" id="3580686812" from="757d218d-72ce-4be7-9ad4-af369434c5fd">
                <oa xmlns="connect.logitech.com" mime="vnd.logitech.harmony/vnd.logitech.harmony.engine?startActivity">
                    activityId=6932433:timestamp=0
                </oa>
            </iq>
        */
    }

    fun getCurrentActivity(callback: Callback<String>?) {
        Timber.i("getCurrentActivity")
        hubSession.sendCommand("getCurrentActivity", "", true, object : Transformer<String, String>(callback) {
            @Nullable
            @Throws(Exception::class)
            override fun transform(result: String?): String? {
                var activityId: String? = null
                if (result != null) {
                    val p = Pattern.compile("result=(.*)")
                    val m = p.matcher(result)
                    if (m.find()) {
                        activityId = m.group(1)
                    }
                }
                return activityId
            }
        })

        /*
            <iq id="Ca0Lf-13" from="ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1@connect.logitech.com/gatorade." type="get">
                <oa xmlns="connect.logitech.com" mime="vnd.logitech.harmony/vnd.logitech.harmony.engine?getCurrentActivity"/>
            </iq>
         */
    }

    fun getConfig(callback: Callback<Data.Config>?) {
        Timber.i("getConfig")
        hubSession.sendCommand("config", "", true, object : Transformer<String, Data.Config>(callback) {
            @Nullable
            @Throws(Exception::class)
            override fun transform(result: String?): Data.Config {
                return Gson().fromJson(result, Data.Config::class.java)
            }
        })

        /*
            <iq id="Ca0Lf-11" from="ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1@connect.logitech.com/gatorade." type="get">
                <oa xmlns="connect.logitech.com" mime="vnd.logitech.harmony/vnd.logitech.harmony.engine?config"/>
            </iq>
         */
    }

}
