package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.generic.Keep

/**
 * Data models.
 */
object Data {

    /**
     * Login token response.
     */
    @Keep
    data class SessionId(
            @JvmField var status: String, // "succeeded" or not
            @JvmField var identity: String,
            @JvmField var friendlyName: String
    )

    /**
     * Hub config data.
     */
    @Keep
    data class Config(
            @JvmField var activity: List<Activity>,
            @JvmField var device: List<Device>
    )

    /**
     * Harmony activity data.
     */
    @Keep
    data class Activity(
            @JvmField var id: String,
            @JvmField var label: String,
            @JvmField var activityOrder: Int,
            @JvmField var type: String, // "PowerOff", "VirtualTelevisionN", "VirtualDvd", "VirtualCdMulti", ...
            @JvmField var icon: String, // "userdata: 0x4454e0"
            @JvmField var controlGroup: List<ControlGroup>,
            @JvmField var fixit: Map<String, PowerState>
    ) {

        override fun toString(): String {
            return "Activity{id=$id label=$label}"
        }
    }

    /**
     * Harmony control group data.
     */
    @Keep
    data class ControlGroup(
            @JvmField var name: String, // "NumericBasic", "Volume", "Channel", "NavigationBasic", "TransportBasic", etc...
            @JvmField var function: List<Function>
    )

    /**
     * Harmony function data.
     */
    @Keep
    data class Function(
            @JvmField var action: String, // "{\"command\":\"VolumeDown\",\"type\":\"IRCommand\",\"deviceId\":\"38896470\"}",
            @JvmField var name: String, // "VolumeDown",
            @JvmField var label: String // "Volume Down"
    ) {

        override fun toString(): String {
            return "Function{name=$name}"
        }
    }

    /**
     * Device power state data (from fixit).
     */
    @Keep
    data class PowerState(
            @JvmField var id: String, // Device id
            @JvmField var Power: String // "On" or "Off"
    )

    /**
     * Harmony device data.
     */
    @Keep
    data class Device(
            @JvmField var id: String,
            @JvmField var label: String,
            @JvmField var controlGroup: List<ControlGroup>
    ) {

        override fun toString(): String {
            return "Device{id=" + id + "label=" + label + "}"
        }
    }

    /**
     * Activity started stats.
     */
    @Keep
    data class ActivityStarted(
            @JvmField var activityId: String
    )

    /**
     * State digest stats.
     */
    @Keep
    data class StateDigest(
            @JvmField var activityId: String,
            @JvmField var activityStatus: Int, // 0--No activity, 1--Activity starting, 2--Activity running, 3--Powering off
            @JvmField var accountId: String
    )

    // StateDigest activityStatus values. Not using enum, I have no idea of the complete value set.
    const val AS_NO_ACTIVITY = 0
    const val AS_ACTIVITY_STARTING = 1
    const val AS_ACTIVITY_RUNNING = 2
    const val AS_POWERING_OFF = 3
}

