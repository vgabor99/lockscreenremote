package com.vargag99.lockscreenremote.harmonyApi

import android.os.Handler

/**
 * Forward HubEventListener calls to a delegate, on a Handler.
 */
internal class HubEventDelivery(private val handler: Handler,
                                private val delegate: HubEventProvider.HubEventListener) : HubEventProvider.HubEventListener {

    override fun onEvent(event: HubEvent) {
        handler.post { delegate.onEvent(event) }
    }
}
