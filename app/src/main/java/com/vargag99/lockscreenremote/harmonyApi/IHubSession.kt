package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.generic.Optional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface IHubSession {
    fun connect(authToken: String?): Completable
    fun disconnect()
    fun connectionEvents(): Observable<ConnectionEvent>
    fun hubEvents(): Observable<HubEvent>
    fun getConfig(): Single<Optional<Data.Config>>
    fun getCurrentActivity(): Single<Optional<String>>
    fun sendButtonPress(time: Long, action: String)
    fun startActivity(activityId: String)
}