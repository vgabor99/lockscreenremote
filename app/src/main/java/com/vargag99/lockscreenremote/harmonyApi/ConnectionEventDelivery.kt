package com.vargag99.lockscreenremote.harmonyApi

import android.os.Handler
import org.jivesoftware.smack.ConnectionListener

/**
 * Forward ConnectionListener calls to a delegate, on a Handler.
 */
internal class ConnectionEventDelivery(private val handler: Handler,
                                       private val delegate: ConnectionListener) : ConnectionListener {

    override fun connectionClosed() {
        handler.post { delegate.connectionClosed() }
    }

    override fun connectionClosedOnError(e: Exception) {
        handler.post { delegate.connectionClosedOnError(e) }
    }

    override fun reconnectingIn(i: Int) {
        handler.post { delegate.reconnectingIn(i) }
    }

    override fun reconnectionSuccessful() {
        handler.post { delegate.reconnectionSuccessful() }
    }

    override fun reconnectionFailed(e: Exception) {
        handler.post { delegate.reconnectionFailed(e) }
    }
}
