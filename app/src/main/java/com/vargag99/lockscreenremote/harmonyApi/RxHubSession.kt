package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.generic.completable
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import org.jivesoftware.smack.ConnectionListener
import timber.log.Timber

/**
 * Hub session adapter.
 * Adapts the callback-style HubSession to the rx-style IHubSession interface
 */
class RxHubSession(host: String, port: Int) : IHubSession {
    private val session: HubSession
    private val connectionEvents = PublishSubject.create<ConnectionEvent>()
    private val hubEvents = PublishSubject.create<HubEvent>()

    init {
        val connectionListener = object : ConnectionListener {
            override fun connectionClosed() {
                Timber.d("connectionClosed")
                connectionEvents.onNext(ConnectionEvent.ConnectionClosed)
            }

            override fun connectionClosedOnError(e: Exception) {
                Timber.e(e, "connectionClosedOnError")
                connectionEvents.onNext(ConnectionEvent.ConnectionClosedOnError(e))
            }

            override fun reconnectingIn(i: Int) {
                Timber.d("reconnectingIn $i")
                connectionEvents.onNext(ConnectionEvent.ReconnectingIn(i))
            }

            override fun reconnectionSuccessful() {
                Timber.d("reconnectionSuccessful")
                connectionEvents.onNext(ConnectionEvent.ReconnectionSuccessful)
            }

            override fun reconnectionFailed(e: Exception) {
                Timber.e(e, "reconnectionFailed")
                connectionEvents.onNext(ConnectionEvent.ReconnectionFailed(e))
            }
        }

        val eventListener = object : HubEventProvider.HubEventListener {
            override fun onEvent(event: HubEvent) {
                Timber.d("onEvent ${event.data.toString()}")
                hubEvents.onNext(event)
            }
        }
        session = HubSession(host, port, connectionListener, eventListener)
    }

    override fun connect(authToken: String?): Completable {
        return completable { callback -> session.connect(authToken, callback) }
                .subscribeOn(AndroidSchedulers.mainThread())
    }

    override fun disconnect() {
        session.disconnect()
    }

    override fun connectionEvents(): Observable<ConnectionEvent> {
        return connectionEvents
    }

    override fun hubEvents(): Observable<HubEvent> {
        return hubEvents
    }

    override fun getConfig(): Single<Optional<Data.Config>> {
        // We need to defer because the command API is fetched async.
        fun doIt() = session.commandApi?.rx()?.getConfig() ?: Single.never()
        return Single.defer { doIt() }
    }

    override fun getCurrentActivity(): Single<Optional<String>> {
        // We need to defer because the command API is fetched async.
        fun doIt() = session.commandApi?.rx()?.getCurrentActivity() ?: Single.never()
        return Single.defer { doIt() }
    }

    override fun sendButtonPress(time: Long, action: String) {
        session.commandApi?.sendButtonPress(time, action)
    }

    override fun startActivity(activityId: String) {
        session.commandApi?.startActivity(activityId)
    }

}