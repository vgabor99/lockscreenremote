package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.generic.completable
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers

class HubSessionRx(private val hubSession: HubSession) {

    fun connect(authToken: String?): Completable {
        return completable { callback -> hubSession.connect(authToken, callback) }
                .subscribeOn(AndroidSchedulers.mainThread())
    }

}

fun HubSession.rx() = HubSessionRx(this)