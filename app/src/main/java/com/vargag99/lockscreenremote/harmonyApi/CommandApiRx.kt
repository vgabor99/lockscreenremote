package com.vargag99.lockscreenremote.harmonyApi

import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.generic.single
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

class CommandApiRx(private val commandApi: CommandApi) {

    fun getCurrentActivity(): Single<Optional<String>> {
        return single<String> { callback -> commandApi.getCurrentActivity(callback) }
                .subscribeOn(AndroidSchedulers.mainThread())
    }

    fun getConfig(): Single<Optional<Data.Config>> {
        return single<Data.Config> { callback -> commandApi.getConfig(callback) }
                .subscribeOn(AndroidSchedulers.mainThread())
    }
}

fun CommandApi.rx() = CommandApiRx(this)