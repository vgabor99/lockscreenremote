package com.vargag99.lockscreenremote.harmonyApi

import com.google.gson.Gson

import org.jivesoftware.smack.packet.PacketExtension
import org.jivesoftware.smack.provider.PacketExtensionProvider
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException

import java.io.IOException

/**
 * Hub event provider extension.
 */
class HubEventProvider(var hubEventListener: HubEventListener?) : PacketExtensionProvider {

    @Throws(Exception::class)
    override fun parseExtension(xmlPullParser: XmlPullParser): PacketExtension {
        if (xmlPullParser.eventType != XmlPullParser.START_TAG) throw IllegalStateException()
        val event = HubEvent()
        if (xmlPullParser.name == HubEvent.ELEMENTNAME) {
            val type = xmlPullParser.getAttributeValue("", "type")
            when (type) {
                "connect.stateDigest?notify" -> {
                    // <stats xmlns="connect.logitech.com" type="connect.stateDigest?notify"><![CDATA[{"sleepTimerId":-1,"runningZoneList":[],"configVersion":247,"activityId":"22076769","syncStatus":0,"time":1488056947,"stateVersion":1792,"tzOffset":"3600","mode":3,"hubSwVersion":"4.12.36","deviceSetupState":[],"tzoffset":"3600","isSetupComplete":true,"contentVersion":95,"wifiStatus":1,"discoveryServer":"https:\/\/svcs.myharmony.com\/Discovery\/Discovery.svc","activityStatus":1,"runningActivityList":"","tz":"CET-1CEST,M3.4.0,M10.5.0\/3","activitySetupState":false,"updates":{"106":"4.12.36"},"hubUpdate":false,"sequence":false,"accountId":"8638596"}]]>
                    xmlPullParser.nextText()?.let {
                        event.data = Gson().fromJson(it, Data.StateDigest::class.java)
                    }
                }
                "harmony.engine?startActivityFinished" -> {
                    // <stats xmlns="connect.logitech.com" type="harmony.engine?startActivityFinished"><![CDATA[activityId=22076769:errorCode=200:errorString=OK]]></stats>
                    xmlPullParser.nextText()?.let { text ->
                        for (s in text.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                            if (s.startsWith("activityId=")) {
                                event.data = Data.ActivityStarted(
                                        activityId = s.substring("activityId=".length)
                                )
                                break
                            }
                        }
                    }
                }
                else -> skip(xmlPullParser)
            }
        }
        if (event.data != null) {
            hubEventListener?.onEvent(event)
        }
        return event
    }

    interface HubEventListener {
        fun onEvent(event: HubEvent)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

}