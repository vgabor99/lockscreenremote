package com.vargag99.lockscreenremote.harmonyApi

import org.jivesoftware.smack.packet.PacketExtension

/**
 * Events received from the hub.
 */
class HubEvent : PacketExtension {
    var data: Any? = null

    override fun getElementName(): String {
        return ELEMENTNAME
    }

    override fun getNamespace(): String {
        return NAMESPACE
    }

    override fun toXML(): String? {
        return null
    }

    companion object {
        const val ELEMENTNAME = "event"
        const val NAMESPACE = "connect.logitech.com"
    }

}
