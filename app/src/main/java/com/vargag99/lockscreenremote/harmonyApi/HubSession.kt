package com.vargag99.lockscreenremote.harmonyApi

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.os.Process
import com.vargag99.lockscreenremote.generic.Callback
import com.vargag99.lockscreenremote.generic.assertUiThread
import org.jivesoftware.smack.ConnectionListener
import timber.log.Timber
import java.net.InetAddress

/**
 * A connected session to the hub.
 * All methods should be called on the UI thread; all callbacks are called on the UI thread.
 */
class HubSession(private val host: String,
                 private val port: Int,
                 connectionListener: ConnectionListener?,
                 hubEventListener: HubEventProvider.HubEventListener?) {
    private val connectionListener: ConnectionListener?
    private val hubEventListener: HubEventProvider.HubEventListener?
    var commandApi: CommandApi? = null
        private set // UI side object, exist only while connected.
    private var workerThread: WorkerThread? = null
    private val uiHandler: Handler = Handler(Looper.getMainLooper())

    private val connectCallback = object : Callback<Void> {
        override fun onResult(result: Void?) {
            Timber.d("got commandApi")
            assertUiThread()
            commandApi = CommandApi(this@HubSession)
        }

        override fun onException(e: Exception) {}
    }

    init {
        // Setup the backwards direction (delivery to UI thread).
        this.connectionListener = if (connectionListener == null) null else ConnectionEventDelivery(uiHandler, connectionListener)
        this.hubEventListener = if (hubEventListener == null) null else HubEventDelivery(uiHandler, hubEventListener)
    }

    fun connect(authToken: String?, callback: Callback<Void>?) {
        Timber.d("connect")
        assertUiThread()
        if (workerThread != null) {
            throw IllegalStateException("Already connected")
        }
        workerThread = WorkerThread().apply {
            start()
            connect(authToken, callback)
        }
    }

    fun disconnect() {
        Timber.d("disconnect")
        assertUiThread()
        workerThread?.disconnect()
        // Null UI-side objects immediately. The disconnect is async (graceful).
        workerThread = null
        commandApi = null
    }

    fun sendCommand(actionType: String, command: String, waitForResponse: Boolean, callback: Callback<String>?) {
        assertUiThread()
        workerThread?.sendCommand(actionType, command, waitForResponse, callback)
    }

    /**
     * Worker thread that contains the connection.
     */
    private inner class WorkerThread internal constructor() : HandlerThread("Session worker thread", Process.THREAD_PRIORITY_DEFAULT) {
        private var hubConnection: HubConnection? = null
        private var sessionId: Data.SessionId? = null
        private lateinit var workerHandler: Handler

        override fun start() {
            super.start()
            workerHandler = Handler(this.looper)
        }

        internal fun connect(authToken: String?, callback: Callback<Void>?) {
            workerHandler.post {
                try {
                    Timber.d("connecting")

                    // Resolve the IP from the DNS in case this is a hostname and not an IP
                    var host = host
                    val address = InetAddress.getByName(this@HubSession.host)
                    if (address != null) {
                        host = address.hostAddress
                    }

                    // Guest login to get session token.
                    val sessionId = obtainSessionId(host, port, authToken)

                    // Session login with the session token.
                    val authConnection = HubConnection(host, port, connectionListener, hubEventListener)
                    val username = sessionId.identity
                    authConnection.login("$username@connect.logitech.com/gatorade.", username)
                    this.hubConnection = authConnection
                    this.sessionId = sessionId
                    postResult(connectCallback, null) // Own callback: report success (install command API).
                    postResult(callback, null) // Caller callback.
                    Timber.d("connecting successful")
                } catch (e: Exception) {
                    Timber.e(e, "connecting failed")
                    postError(callback, e)
                }
            }
        }

        internal fun disconnect() {
            workerHandler.postAtFrontOfQueue {
                if (hubConnection != null) {
                    try {
                        // Try graceful close, but ignore the outcome.
                        hubConnection?.disconnect()
                        Timber.d("disconnected")
                    } catch (e: Exception) {
                        Timber.w(e, "disconnect failed")
                        // Ignore.
                    } finally {
                        hubConnection = null
                        quit()
                    }
                }
            }
        }

        @Throws(Exception::class)
        private fun obtainSessionId(host: String, port: Int, authToken: String?): Data.SessionId {
            val guestConnection = HubConnection(host, port, null, null)
            try {
                guestConnection.login("guest@connect.logitech.com/gatorade.", "gatorade.")
                // Get session id.
                return guestConnection.requestSessionId(authToken)
            } finally {
                guestConnection.disconnect()
            }
        }

        internal fun sendCommand(actionType: String, command: String, waitForResponse: Boolean, callback: Callback<String>?) {
            workerHandler.post {
                try {
                    Timber.d("sendCommand")
                    val result = hubConnection?.sendHubCommand(actionType, command, waitForResponse)
                    postResult(callback, result)
                } catch (e: Exception) {
                    Timber.e(e, "sendCommand failed")
                    postError(callback, e)
                }
            }
        }

    }

    private fun <T> postResult(callback: Callback<T>?, result: T?) {
        if (callback != null) {
            uiHandler.post(Callback.Result(callback, result))
        }
    }

    private fun postError(callback: Callback<*>?, exception: Exception) {
        if (callback != null) {
            uiHandler.post(Callback.Error(callback, exception))
        }
    }

}
