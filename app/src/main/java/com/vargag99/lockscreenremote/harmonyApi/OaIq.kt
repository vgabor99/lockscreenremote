package com.vargag99.lockscreenremote.harmonyApi

import org.jivesoftware.smack.packet.IQ

/**
 * An IQ packet that holds the text taken from the "oa" extension that the harmony hub uses
 */
class OaIq(text: String?) : IQ() {
    var data: String? = null


    init {
        data = text
    }

    override fun getChildElementXML(): String? {
        return null
    }

    companion object {
        const val NAMESPACE = "connect.logitech.com"
        const val ELEMENT_NAME = "oa"
    }
}