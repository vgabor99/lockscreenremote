package com.vargag99.lockscreenremote.harmonyApi

class HubSessionFactory : IHubSessionFactory {
    override fun createHubSession(host: String, port: Int): IHubSession = RxHubSession(host, port)

}