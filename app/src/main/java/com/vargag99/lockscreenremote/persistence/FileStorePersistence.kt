package com.vargag99.lockscreenremote.persistence

import com.vargag99.lockscreenremote.app.DiagnosticInfo
import com.vargag99.lockscreenremote.app.Layout
import com.vargag99.lockscreenremote.app.MissingIcons
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import javax.inject.Inject

/**
 * An overly simple persistence for the objects we use.
 * Data is saved to files.
 */
class FileStorePersistence @Inject constructor(private val fileStore: FileStore) : IPersistence {

    @Throws(Exception::class)
    override fun getHubInfoList(): List<HubInfo> {
        return fileStore.loadAll(HUBS, HubInfo::class.java)
    }

    @Throws(Exception::class)
    override fun existsHubInfo(hubId: String): Boolean {
        return fileStore.exists(HUBS, hubId)
    }

    @Throws(Exception::class)
    override fun getHubInfo(hubId: String): HubInfo? {
        return fileStore.load(HUBS, hubId, HubInfo::class.java)
    }

    @Throws(Exception::class)
    override fun updateHubInfo(hubInfo: HubInfo) {
        fileStore.save<Any>(HUBS, hubInfo.uuid, hubInfo)
    }

    @Throws(Exception::class)
    override fun existsConfig(hubId: String): Boolean {
        return fileStore.exists(CONFIGS, hubId)
    }

    @Throws(Exception::class)
    override fun getConfig(hubId: String): Data.Config? {
        return fileStore.load(CONFIGS, hubId, Data.Config::class.java)
    }

    @Throws(Exception::class)
    override fun updateConfig(hubId: String, config: Data.Config) {
        fileStore.save<Any>(CONFIGS, hubId, config)
    }

    @Throws(Exception::class)
    override fun existsLayout(hubId: String, activityId: String): Boolean {
        return fileStore.exists(LAYOUTS, "$hubId-$activityId")
    }

    @Throws(Exception::class)
    override fun getLayout(hubId: String, activityId: String): Layout? {
        return fileStore.load(LAYOUTS, "$hubId-$activityId", Layout::class.java)
    }

    @Throws(Exception::class)
    override fun updateLayout(hubId: String, activityId: String, layout: Layout) {
        fileStore.save<Any>(LAYOUTS, "$hubId-$activityId", layout)
    }

    @Throws(Exception::class)
    override fun existsActivityConfig(hubId: String, activityId: String): Boolean {
        return fileStore.exists(ACTIVITY_CONFIG, "$hubId-$activityId")
    }

    @Throws(Exception::class)
    override fun getActivityConfig(hubId: String, activityId: String): UiData.ActivityConfig? {
        return fileStore.load(ACTIVITY_CONFIG, "$hubId-$activityId", UiData.ActivityConfig::class.java)
    }

    @Throws(Exception::class)
    override fun updateActivityConfig(hubId: String, activityId: String, activityConfig: UiData.ActivityConfig) {
        fileStore.save<Any>(ACTIVITY_CONFIG, "$hubId-$activityId", activityConfig)
    }

    @Throws(Exception::class)
    override fun existsMissingIcons(): Boolean {
        return fileStore.exists(MISC, MISSING_ICONS)
    }

    @Throws(Exception::class)
    override fun getMissingIcons(): MissingIcons? {
        return fileStore.load(MISC, MISSING_ICONS, MissingIcons::class.java)
    }

    @Throws(Exception::class)
    override fun updateMissingIcons(missingIcons: MissingIcons) {
        fileStore.save<Any>(MISC, MISSING_ICONS, missingIcons)
    }

    // Save only, we never read this. The purpose is to be zipped along with all other files when sending a bug report.
    @Throws(Exception::class)
    override fun updateDiagnosticInfo(diagnosticInfo: DiagnosticInfo) {
        fileStore.save<Any>(MISC, DIAGNOSTIC_INFO, diagnosticInfo)
    }

    @Throws(Exception::class)
    override fun zip(targetFn: String) {
        fileStore.zip(targetFn)
    }

    companion object {
        private const val CONFIGS = "configs"
        private const val HUBS = "hubs"
        private const val LAYOUTS = "layouts"
        private const val ACTIVITY_CONFIG = "activity_config"
        private const val MISC = "misc"
        private const val MISSING_ICONS = "missing_icons"
        private const val DIAGNOSTIC_INFO = "diagnostic_info"
    }

}
