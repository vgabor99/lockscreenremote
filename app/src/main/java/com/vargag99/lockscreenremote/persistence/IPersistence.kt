package com.vargag99.lockscreenremote.persistence

import com.vargag99.lockscreenremote.app.DiagnosticInfo
import com.vargag99.lockscreenremote.app.Layout
import com.vargag99.lockscreenremote.app.MissingIcons
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

/**
 * Persistence interface.
 * Provides synchronous blocking functions.
 */
interface IPersistence {
    @Throws(Exception::class)
    fun getHubInfoList(): List<HubInfo>

    @Throws(Exception::class)
    fun existsHubInfo(hubId: String): Boolean

    @Throws(Exception::class)
    fun getHubInfo(hubId: String): HubInfo?

    @Throws(Exception::class)
    fun updateHubInfo(hubInfo: HubInfo)

    @Throws(Exception::class)
    fun existsConfig(hubId: String): Boolean

    @Throws(Exception::class)
    fun getConfig(hubId: String): Data.Config?

    @Throws(Exception::class)
    fun updateConfig(hubId: String, config: Data.Config)

    @Throws(Exception::class)
    fun existsLayout(hubId: String, activityId: String): Boolean

    @Throws(Exception::class)
    fun getLayout(hubId: String, activityId: String): Layout?

    @Throws(Exception::class)
    fun updateLayout(hubId: String, activityId: String, layout: Layout)

    @Throws(Exception::class)
    fun existsActivityConfig(hubId: String, activityId: String): Boolean

    @Throws(Exception::class)
    fun getActivityConfig(hubId: String, activityId: String): UiData.ActivityConfig?

    @Throws(Exception::class)
    fun updateActivityConfig(hubId: String, activityId: String, activityConfig: UiData.ActivityConfig)

    @Throws(Exception::class)
    fun existsMissingIcons(): Boolean

    @Throws(Exception::class)
    fun getMissingIcons(): MissingIcons?

    @Throws(Exception::class)
    fun updateMissingIcons(missingIcons: MissingIcons)

    // Save only, we never read this. The purpose is to be zipped along with all other files when sending a bug report.
    @Throws(Exception::class)
    fun updateDiagnosticInfo(diagnosticInfo: DiagnosticInfo)

    @Throws(Exception::class)
    fun zip(targetFn: String)
}