package com.vargag99.lockscreenremote.persistence

import com.google.gson.Gson
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.generic.Keep
import com.vargag99.lockscreenremote.generic.Zip
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * Storing data in files.
 * TODO this is way too primitive, but for now it will do.
 * TODO for now we use dir-level locking to make loadAll() simple
 */

class FileStore(private val dir: String) {
    private val fileLocks: MutableMap<String, ReentrantReadWriteLock> // Guards access to files.
    private val lock: Any // Guards creating the file locks.

    init {
        fileLocks = WeakHashMap()
        lock = Any()
    }

    @Throws(Exception::class)
    fun <T> save(dir: String, fn: String, data: T) {
        //assertKeep(data);
        getFileLock(dir).write {
            val file = File(this.dir + '/'.toString() + dir + '/'.toString() + fn)
            file.parentFile?.mkdirs()
            FileWriter(file).use { Gson().toJson(data, it) }
        }
    }

    @Throws(Exception::class)
    fun <T> load(dir: String, fn: String, clazz: Class<T>): T? {
        getFileLock(dir).read {
            val file = File(this.dir + '/'.toString() + dir + '/'.toString() + fn)
            return if (file.exists()) {
                Gson().fromJson(FileReader(file), clazz)
            } else null
        }
    }

    @Throws(Exception::class)
    fun exists(dir: String, fn: String): Boolean {
        getFileLock(dir).read {
            val file = File(this.dir + '/'.toString() + dir + '/'.toString() + fn)
            return file.exists()
        }
    }


    @Throws(Exception::class)
    fun <T> loadAll(dir: String, clazz: Class<T>): List<T> {
        getFileLock(dir).read {
            val result = ArrayList<T>()
            val file = File(this.dir + '/'.toString() + dir + '/'.toString())
            file.parentFile?.mkdirs()
            if (file.exists() && file.isDirectory) {
                for (f in (file.listFiles() ?: emptyArray())) {
                    if (f.isFile) {
                        result.add(Gson().fromJson(FileReader(f), clazz))
                    }
                }
            }
            return result
        }
    }


    private fun getFileLock(path: String): ReentrantReadWriteLock {
        synchronized(lock) {
            return fileLocks[path] ?: ReentrantReadWriteLock().apply { fileLocks[path] = this }
        }
    }

    fun zip(targetFn: String) {
        Zip().zipFileAtPath(dir, targetFn) // TODO maybe locking?
    }

    private fun assertKeep(`object`: Any) {
        if (BuildConfig.DEBUG) {
            // This assert checks for @Keep annotation. Used to early detect proguard problems.
            // This does not detect all problems (only the top-level object is checked, but it is
            // possible to proguard a member inside an object hierarchy). Also it does not detect
            // if proguard is configured bad. Still, it catches some basic bugs, so I keep it.
            if (!`object`.javaClass.isAnnotationPresent(Keep::class.java)) {
                throw IllegalArgumentException("Object is not @Keep.")
            }
        }
    }
}
