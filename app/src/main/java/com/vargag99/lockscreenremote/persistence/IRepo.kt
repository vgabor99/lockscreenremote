package com.vargag99.lockscreenremote.persistence

import com.vargag99.lockscreenremote.app.DiagnosticInfo
import com.vargag99.lockscreenremote.app.Layout
import com.vargag99.lockscreenremote.app.MissingIcons
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Repo interface.
 * This is the convenience persistence interface:
 * Provides Observable/Single data getters.
 * Provides "fire and forget" updaters (async completion with ignored-logged error).
 * TODO maybe split to pieces (IConfigRepo, ILayoutRepo, etc. ?)
 */
interface IRepo {

    fun getHubInfoList(): Observable<List<HubInfo>>

    fun existsHubInfo(hubId: String): Single<Boolean>

    fun getHubInfo(hubId: String): Observable<Optional<HubInfo>>

    fun updateHubInfo(hubInfo: HubInfo)

    fun existsConfig(hubId: String): Single<Boolean>

    fun getConfig(hubId: String): Observable<Optional<Data.Config>>

    fun updateConfig(hubId: String, config: Data.Config)

    fun existsLayout(hubId: String, activityId: String): Single<Boolean>

    fun getLayout(hubId: String, activityId: String): Observable<Optional<Layout>>

    fun updateLayout(hubId: String, activityId: String, layout: Layout)

    fun existsActivityConfig(hubId: String, activityId: String): Single<Boolean>

    fun getActivityConfig(hubId: String, activityId: String): Observable<Optional<UiData.ActivityConfig>>

    fun updateActivityConfig(hubId: String, activityId: String, activityConfig: UiData.ActivityConfig)

    fun existsMissingIcons(): Single<Boolean>

    fun getMissingIcons(): Single<Optional<MissingIcons>>

    fun updateMissingIcons(missingIcons: MissingIcons): Completable

    fun updateDiagnosticInfo(diagnosticInfo: DiagnosticInfo): Completable

    fun zip(targetFn: String): Completable
}