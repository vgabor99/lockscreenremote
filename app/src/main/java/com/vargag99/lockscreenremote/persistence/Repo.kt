package com.vargag99.lockscreenremote.persistence

import com.vargag99.lockscreenremote.app.DiagnosticInfo
import com.vargag99.lockscreenremote.app.Layout
import com.vargag99.lockscreenremote.app.MissingIcons
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repo @Inject constructor(private val persistence: IPersistence) : IRepo {
    private val eventsPublisher = PublishSubject.create<Event>()
    private val disposables = CompositeDisposable()

    private sealed class Event {
        object HubInfoListChange : Event()
        data class HubInfoChange(val hubId: String) : Event()
        data class ConfigChange(val hubId: String) : Event()
        data class LayoutChange(val hubId: String, val activityId: String) : Event()
        data class ActivityConfigChange(val hubId: String, val activityId: String) : Event()
    }

    // Observable getter: get data once.
    private fun <T> getSingle(getter: IPersistence.() -> T): Single<T> =
            Single.fromCallable {
                persistence.getter()
            }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    // Observable getter: get data initially, and on published events.
    private fun <T, E> get(getter: IPersistence.() -> T, vararg events: E): Observable<T> =
            eventsPublisher
                    .filter { event -> events.any { it == event } }
                    .map { Unit }
                    .startWith(Unit)
                    .switchMap { Observable.just(persistence.getter()) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    // Fire-and-forget updater: update data and fire events.
    private fun update(update: IPersistence.() -> Unit, vararg events: Event) {
        Completable.fromAction {
            persistence.update()
            events.forEach { eventsPublisher.onNext(it) }
        }
                .subscribeOn(Schedulers.io())
                .subscribeBy(onError = { Timber.e(it) })
                .addTo(disposables)
    }

    override fun getHubInfoList(): Observable<List<HubInfo>> =
            get({ getHubInfoList() }, Event.HubInfoListChange)

    override fun existsHubInfo(hubId: String): Single<Boolean> =
            getSingle { existsHubInfo(hubId) }

    override fun getHubInfo(hubId: String): Observable<Optional<HubInfo>> =
            get({ Optional(getHubInfo(hubId)) }, Event.HubInfoChange(hubId))

    override fun updateHubInfo(hubInfo: HubInfo) =
            update({ updateHubInfo(hubInfo) }, Event.HubInfoListChange, Event.HubInfoChange(hubInfo.uuid))

    override fun existsConfig(hubId: String): Single<Boolean> =
            getSingle { existsConfig(hubId) }

    override fun getConfig(hubId: String): Observable<Optional<Data.Config>> =
            get({ Optional(getConfig(hubId)) }, Event.ConfigChange(hubId))

    override fun updateConfig(hubId: String, config: Data.Config) =
            update({ updateConfig(hubId, config) }, Event.ConfigChange(hubId))

    override fun existsLayout(hubId: String, activityId: String): Single<Boolean> =
            getSingle { existsLayout(hubId, activityId) }

    override fun getLayout(hubId: String, activityId: String): Observable<Optional<Layout>> =
            get({ Optional(getLayout(hubId, activityId)) }, Event.LayoutChange(hubId, activityId))

    override fun updateLayout(hubId: String, activityId: String, layout: Layout) =
            update({ updateLayout(hubId, activityId, layout) }, Event.LayoutChange(hubId, activityId))

    override fun existsActivityConfig(hubId: String, activityId: String): Single<Boolean> =
            getSingle { existsActivityConfig(hubId, activityId) }

    override fun getActivityConfig(hubId: String, activityId: String): Observable<Optional<UiData.ActivityConfig>> =
            get({ Optional(getActivityConfig(hubId, activityId)) }, Event.ActivityConfigChange(hubId, activityId))

    override fun updateActivityConfig(hubId: String, activityId: String, activityConfig: UiData.ActivityConfig) =
            update({ updateActivityConfig(hubId, activityId, activityConfig) }, Event.ActivityConfigChange(hubId, activityId))

    override fun existsMissingIcons(): Single<Boolean> =
            getSingle { existsMissingIcons() }

    override fun getMissingIcons(): Single<Optional<MissingIcons>> =
            getSingle { Optional(getMissingIcons()) }

    override fun updateMissingIcons(missingIcons: MissingIcons) =
            Completable.fromAction { persistence.updateMissingIcons(missingIcons) }
                    .subscribeOn(Schedulers.io())

    override fun updateDiagnosticInfo(diagnosticInfo: DiagnosticInfo) =
            Completable.fromAction { persistence.updateDiagnosticInfo(diagnosticInfo) }
                    .subscribeOn(Schedulers.io())

    override fun zip(targetFn: String) =
            Completable.fromAction { persistence.zip(targetFn) }
                    .subscribeOn(Schedulers.io())

}