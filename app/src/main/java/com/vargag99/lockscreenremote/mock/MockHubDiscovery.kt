package com.vargag99.lockscreenremote.mock

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.harmonyDiscovery.IHubDiscovery
import io.reactivex.Observable
import io.reactivex.rxkotlin.toObservable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MockHubDiscovery @Inject constructor(private val mockHubs: MockHubs) : IHubDiscovery {

    override fun discovery(): Observable<HubInfo> {
        return mockHubs.hubs.toObservable()
                .map { it.hubInfo }
                .concatMap { Observable.just(it).delay(1, TimeUnit.SECONDS) }
    }
}
