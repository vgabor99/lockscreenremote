package com.vargag99.lockscreenremote.mock

import com.vargag99.lockscreenremote.harmonyApi.IHubSession
import com.vargag99.lockscreenremote.harmonyApi.IHubSessionFactory

class MockHubSessionFactory(private val mockHubs: MockHubs) : IHubSessionFactory {
    override fun createHubSession(host: String, port: Int): IHubSession {
        return mockHubs.hubs.firstOrNull { it.hubInfo.ip == host && it.hubInfo.port == port }
                ?: throw Exception("Mock hub not found: $host:$port")
    }

}