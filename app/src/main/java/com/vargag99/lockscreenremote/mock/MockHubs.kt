package com.vargag99.lockscreenremote.mock

import android.content.Context
import com.google.gson.Gson
import com.vargag99.lockscreenremote.generic.Keep
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.harmonyApi.ConnectionEvent
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyApi.HubEvent
import com.vargag99.lockscreenremote.harmonyApi.IHubSession
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * Factory (mock objects).
 */

class MockHubs @Inject constructor(@ApplicationContext context: Context) {
    val hubs = listOf("mock_hub_1.json", "mock_hub_2.json")
            .map { loadMockHub(context, it) }

    inner class MockHubSession(val hubInfo: HubInfo,
                               private val config: Data.Config) : IHubSession {
        private var disposables = CompositeDisposable()
        private val connectionEvents = PublishSubject.create<ConnectionEvent>()
        private val hubEvents = PublishSubject.create<HubEvent>()
        private var state: Data.StateDigest by Delegates.observable(Data.StateDigest(IHarmonyHub.POWER_OFF_ACTIVITY_ID, 0, hubInfo.accountId)) { _, _, new ->
            if (new.activityStatus == Data.AS_ACTIVITY_RUNNING) {
                hubEvents.onNext(HubEvent().apply { data = Data.ActivityStarted(new.activityId) })
            }
            hubEvents.onNext(HubEvent().apply { data = new })
        }
        private val startActivityTrigger = PublishSubject.create<String>()

        private fun setupEventHandling() {
            startActivityTrigger.concatMap { activityId ->
                val (progressState, endState) = when (activityId) {
                    IHarmonyHub.POWER_OFF_ACTIVITY_ID -> Pair(Data.AS_POWERING_OFF, Data.AS_NO_ACTIVITY)
                    else -> Pair(Data.AS_ACTIVITY_STARTING, Data.AS_ACTIVITY_RUNNING)
                }
                val progress = state.copy(activityId = activityId, activityStatus = progressState)
                val final = state.copy(activityId = activityId, activityStatus = endState)
                Observable.just(final)
                        .delay(4, TimeUnit.SECONDS)
                        .startWith(progress)
            }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy { state = it }
                    .addTo(disposables)
            connectionEvents
                    .doOnNext { Timber.d("connectionEvent:$it") }
                    .subscribe()
                    .addTo(disposables)
            hubEvents
                    .doOnNext { Timber.d("hubEvent:$it") }
                    .subscribe()
                    .addTo(disposables)
        }

        override fun connect(authToken: String?): Completable {
            Timber.d("connect")
            disposables = CompositeDisposable()
            setupEventHandling()
            return Completable.timer(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnComplete { Timber.d("connected") }
        }

        override fun disconnect() {
            Timber.d("disconnect")
            disposables.dispose()
        }

        override fun connectionEvents(): Observable<ConnectionEvent> {
            return connectionEvents
        }

        override fun hubEvents(): Observable<HubEvent> {
            return hubEvents
        }

        override fun getConfig(): Single<Optional<Data.Config>> {
            Timber.d("getConfig")
            return Single.just(Optional(config))
                    .delay(500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
        }

        override fun getCurrentActivity(): Single<Optional<String>> {
            Timber.d("getCurrentActivity")
            return Single.timer(500, TimeUnit.MILLISECONDS)
                    .map { Optional(state.activityId) }
                    .observeOn(AndroidSchedulers.mainThread())
        }

        override fun sendButtonPress(time: Long, action: String) {
            Timber.d("sendButtonPress action=$action")
        }

        override fun startActivity(activityId: String) {
            Timber.d("startActivity activityId=$activityId")
            startActivityTrigger.onNext(activityId)
        }

    }

    @Keep
    internal data class MockHubData(
            var hubInfo: HubInfo,
            var config: Data.Config
    )

    @Throws(IOException::class)
    private fun loadMockHub(context: Context, assetFn: String): MockHubSession {
        context.assets.open(assetFn).use {
            val reader = BufferedReader(InputStreamReader(it, "UTF-8"))
            val data = Gson().fromJson(reader, MockHubData::class.java)
            return MockHubSession(data.hubInfo, data.config)
        }
    }
}
