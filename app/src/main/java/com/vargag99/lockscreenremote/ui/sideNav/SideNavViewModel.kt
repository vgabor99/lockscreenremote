package com.vargag99.lockscreenremote.ui.sideNav

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.app.rx
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.persistence.IRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import de.musichin.reactivelivedata.distinctUntilChanged
import de.musichin.reactivelivedata.map
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class SideNavViewModel @Inject constructor(
    repo: IRepo,
    preferences: Preferences,
    @ApplicationContext context: Context, // TODO this is not clean, used to get texts
    private val analytics: IAnalytics
) : RxViewModel() {

    private sealed class Event {
        class NumHubs(val num: Int) : Event()
        object CloseHubList : Event()
        object ToggleHubList : Event()
    }

    data class State(val numHubs: Int, val hubListShowing: Boolean)

    private val events = PublishSubject.create<Event>()
    private val stateVar = MutableLiveData<State>()

    val state: LiveData<State> = stateVar

    val hubListShowing = state.map { it.hubListShowing }.distinctUntilChanged()

    // TODO this is not clean
    val hubSelectorDrawable: LiveData<Int> = Transformations.map(state) { state ->
        when {
            state.numHubs < 2 -> 0
            state.hubListShowing -> R.drawable.ic_arrow_drop_up_white_24dp
            else -> R.drawable.ic_arrow_drop_down_white_24dp
        }
    }

    private val hubNameVar = MutableLiveData<String>()
    val hubName: LiveData<String> = hubNameVar

    fun onHubSelectorClick() {
        events.onNext(Event.ToggleHubList)
    }

    fun closeHubList() {
        events.onNext(Event.CloseHubList)
    }

    private fun reportAnalytics(state: State, event: Event) {
        if (event is Event.ToggleHubList) {
            analytics.event(
                if (state.hubListShowing) IAnalytics.SIDE_NAV_MENU else IAnalytics.SIDE_NAV_HUB_LIST,
                IAnalytics.SIDE_NAV
            )
        }
    }

    init {
        val numHubsObservable = repo.getHubInfoList()
            .map { Event.NumHubs(it.size) }
            .distinctUntilChanged()
        val state = Observable.merge(numHubsObservable, events)
            .scan(State(0, false)) { state, event: Event ->
                reportAnalytics(state, event)
                var (numHubs, hubListShowing) = state
                when (event) {
                    is Event.NumHubs -> numHubs = event.num // Got number of hubs
                    is Event.CloseHubList -> hubListShowing = false // Closing requested
                    is Event.ToggleHubList -> hubListShowing = !hubListShowing // Toggling requested
                }
                if (numHubs < 2) {
                    hubListShowing = false // 0 or 1 hubs, can't show list.
                }
                State(numHubs, hubListShowing)
            }
        state
            .distinctUntilChanged()
            .bindTo(stateVar)
            .addTo(disposables)
        val appContext = context.applicationContext
        preferences.rx().hubId
            .switchMap { hubId ->
                hubId.value?.let { repo.getHubInfo(it) }
                    ?: Observable.just(Optional(null))
            }
            .map { hubInfo ->
                hubInfo.value?.friendlyName
                    ?: appContext.getString(R.string.no_hub_connected)
            }
            .distinctUntilChanged()
            .bindTo(hubNameVar)
            .addTo(disposables)
    }
}