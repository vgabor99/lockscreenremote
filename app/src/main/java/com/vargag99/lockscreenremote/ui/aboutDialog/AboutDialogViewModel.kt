package com.vargag99.lockscreenremote.ui.aboutDialog

import android.content.Context
import android.content.Intent
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.IAppInfo
import com.vargag99.lockscreenremote.app.IEmailCreator
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import java.io.File
import java.util.*
import javax.inject.Inject

@HiltViewModel
internal class AboutDialogViewModel @Inject constructor(
    private val analytics: IAnalytics,
    private val emailCreator: IEmailCreator,
    @ApplicationContext context: Context,
    appInfo: IAppInfo
) : RxViewModel() {

    val appLabel = appInfo.appLabel
    val appVersionString = appInfo.appVersionString
    val copyrightString: String =
        context.getString(R.string.copyright_message, Calendar.getInstance().get(Calendar.YEAR).toString())

    var includeDiagnostics = true

    fun onSendEmailClick() {
        analytics.event(IAnalytics.SEND_EMAIL, IAnalytics.ABOUT)
        val attachment: Single<Optional<File>> =
            if (includeDiagnostics) {
                emailCreator.prepareDiagnosticInfo()
                    .map { Optional(it) }
                    .doOnError { Timber.e(it, "Failed to collect diagnostics info") }
                    .onErrorReturn { Optional(null) }
            } else {
                Single.just(Optional<File>(null))
            }
        attachment
            .map { emailCreator.createEmailIntent(it.value) }
            .toObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .bindTo(sendEmail)
            .addTo(disposables)
    }

    val sendEmail = SingleLiveEvent<Intent>()

}