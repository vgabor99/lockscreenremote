package com.vargag99.lockscreenremote.ui.main

import android.view.View
import androidx.core.view.GravityCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.databinding.adapters.ListenerUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.vargag99.lockscreenremote.R

/**
 * TODO this binding could be more generic (START/END/LEFT_RIGHT) etc.
 * For now, hard-coded START will do.
 */
@BindingAdapter(value = ["openStartAttrChanged"])
fun setListener(drawerLayout: DrawerLayout, listener: InverseBindingListener?) {
    val newListener = listener?.let {
        object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {}

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}

            override fun onDrawerClosed(drawerView: View) {
                it.onChange()
            }

            override fun onDrawerOpened(drawerView: View) {
                it.onChange()
            }
        }
    }
    val oldListener = ListenerUtil.trackListener(drawerLayout, newListener, R.id.drawer_listener)
    oldListener?.let { drawerLayout.removeDrawerListener(it) }
    newListener?.let { drawerLayout.addDrawerListener(it) }
}


@BindingAdapter("openStart")
fun setOpenStart(drawerLayout: DrawerLayout, open: Boolean?) {
    if (open == true) {
        drawerLayout.openDrawer(GravityCompat.START)
    } else {
        drawerLayout.closeDrawer(GravityCompat.START)
    }
}

@InverseBindingAdapter(attribute = "openStart")
fun getOpenStart(drawerLayout: DrawerLayout): Boolean {
    return drawerLayout.isDrawerOpen(GravityCompat.START)
}