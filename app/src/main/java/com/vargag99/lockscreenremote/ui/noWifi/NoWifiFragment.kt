package com.vargag99.lockscreenremote.ui.noWifi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NoWifiFragment : Fragment() {
    @Inject
    internal lateinit var analytics: IAnalytics

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_no_wifi, container, false)
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.NO_WIFI)
    }

    companion object {
        val TAG: String = NoWifiFragment::class.java.simpleName
        fun newInstance() = NoWifiFragment()
    }

}
