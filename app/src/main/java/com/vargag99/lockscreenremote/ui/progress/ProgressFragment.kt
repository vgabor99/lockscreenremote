package com.vargag99.lockscreenremote.ui.progress

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.databinding.FragmentProgressBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgressFragment : Fragment() {
    private var _binding: FragmentProgressBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentProgressBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Kick off the spinning background.
        val animBg =
            AnimatedVectorDrawableCompat.create(binding.progress.context, R.drawable.circle_empty_animated_dot_40dp)
        binding.progress.background = animBg
        animBg!!.start()
        // Initially, delay showing the whole view for a second.
        // If we already have a hubId, we will get the main screen before that.
        if (savedInstanceState == null) {
            binding.progressContainer.alpha = 0f
            ObjectAnimator.ofFloat(binding.progressContainer, "alpha", 1f).apply {
                duration = 500
                startDelay = 1000
            }.start()
        }
    }

    companion object {
        val TAG: String = ProgressFragment::class.java.simpleName
        fun newInstance() = ProgressFragment()
    }

}
