package com.vargag99.lockscreenremote.ui.editLayout.actionPicker

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.ui.UiUtil
import com.vargag99.lockscreenremote.ui.editLayout.EditLayoutArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class ActionPickerDialogViewModel @Inject constructor(
    @ApplicationContext context: Context, // TODO not clean, but we use localized texts
    repo: IRepo,
    private val analytics: IAnalytics
) : RxViewModel(), OnActionPickerItemClickListener {

    override fun onItemClick(item: ActionPickerItem) {
        analytics.event(IAnalytics.PICK_ACTION, IAnalytics.ACTION_PICKER)
        actionPickerItemClicks.value = item
    }

    private val args = PublishSubject.create<EditLayoutArgs>()

    fun setArgs(hubId: String, activityId: String) {
        // I really see no way to inject args into the viewModel
        // Even if I would, ViewModelProviders seems to cache viewModels based on their type,
        // so I would end up having the first viewModel and different arg instances being ignored (??)
        args.onNext(EditLayoutArgs(hubId, activityId))
    }

    private val actionPickerItemsVar = MutableLiveData<List<ActionPickerItem>>()
    val actionPickerItems: LiveData<List<ActionPickerItem>> = actionPickerItemsVar

    val actionPickerItemClicks = SingleLiveEvent<ActionPickerItem>()

    init {
        args.distinctUntilChanged()
            .switchMap { args -> repo.getActivityConfig(args.hubId, args.activityId) }
            .notEmpty()
            .map { activityConfig ->
                val result = mutableListOf<ActionPickerItem>()
                // The (empty) item
                result.add(
                    ActionPickerItem.Action(
                        context.getString(R.string.unset_button),
                        R.drawable.btn_unset,
                        null
                    )
                )
                // Action items in groups.
                for ((_, label, items) in activityConfig.actionGroups) {
                    label.let { result.add(ActionPickerItem.Header(it)) }
                    for (item in items) {
                        result.add(ActionPickerItem.Action(item.label, UiUtil.safeIcon(item), item.action))
                    }
                }
                result as List<ActionPickerItem>

            }
            .bindTo(actionPickerItemsVar)
            .addTo(disposables)
    }
}