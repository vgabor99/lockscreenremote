package com.vargag99.lockscreenremote.ui.sideNav

import android.content.Context
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@HiltViewModel
class SideNavMenuViewModel @Inject constructor(
    private val analytics: IAnalytics,
    @ApplicationContext context: Context // Sorry, what could I do with a menu??
) : RxViewModel(), OnMenuItemClickListener {

    override fun onMenuItemClick(item: MenuItem) {
        when (item.itemId) {
            R.id.nav_share -> IAnalytics.SHARE
            R.id.nav_rate -> IAnalytics.RATE
            R.id.nav_privacy_settings -> IAnalytics.PRIVACY_SETTINGS
            R.id.nav_about -> IAnalytics.ABOUT
            else -> null
        }?.let { analytics.event(it, IAnalytics.SIDE_NAV) }
        menuItemClicksVar.value = item
    }

    private val menuItemsVar = MutableLiveData<List<MenuItem>>()
    val menuItems: LiveData<List<MenuItem>> = menuItemsVar

    private val menuItemClicksVar = SingleLiveEvent<MenuItem>()
    val menuItemClicks: LiveData<MenuItem> = menuItemClicksVar

    init {
        val appContext = context.applicationContext
        // TODO the menu is not dynamic in this branch, but to keep merging simple during the refactor, let's pretend it is.
        Observable.just(Unit)
            .map {
                val menu = UiUtil.createMenu(appContext)
                MenuInflater(appContext).inflate(R.menu.activity_main_drawer, menu)
                UiUtil.setItemVisible(menu, R.id.nav_privacy_settings, BuildConfig.PRIVACY_SETTINGS)
                menu
            }
            .map { menu -> (0 until menu.size()).map { menu.getItem(it) }.filter { it.isVisible } }
            .bindTo(menuItemsVar)
            .addTo(disposables)
    }
}