package com.vargag99.lockscreenremote.ui.privacySettings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.app.rx
import com.vargag99.lockscreenremote.generic.Alias
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

// We have no analytics here, because we might not have permission for it.
// This is the thing that ask for that permission.
@HiltViewModel
internal class PrivacySettingsViewModel @Inject constructor(preferences: Preferences) : RxViewModel() {

    private val acceptEnabledVar = MutableLiveData<Boolean>()

    init {
        Observables.combineLatest(
            preferences.rx().analyticsEnabled,
            preferences.rx().crashlyticsEnabled
        )
            .map { (analyticsEnabled, crashlyticsEnabled) -> analyticsEnabled && crashlyticsEnabled }
            .bindTo(acceptEnabledVar)
            .addTo(disposables)
    }

    var enableAnalytics: Boolean by Alias(preferences::analyticsEnabled)

    var enableCrashlytics: Boolean by Alias(preferences::crashlyticsEnabled)

    val acceptEnabled: LiveData<Boolean> = acceptEnabledVar

    fun onAcceptClick() {
        accepted.value = Unit
    }

    fun onExitClick() {
        enableAnalytics = false
        enableCrashlytics = false
        rejected.value = Unit
    }

    val accepted = SingleLiveEvent<Unit>()
    val rejected = SingleLiveEvent<Unit>()

}