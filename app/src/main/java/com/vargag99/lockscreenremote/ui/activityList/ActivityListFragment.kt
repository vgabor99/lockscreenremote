package com.vargag99.lockscreenremote.ui.activityList

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.BR
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentActivityListBinding
import com.vargag99.lockscreenremote.generic.extraNotNull
import dagger.hilt.android.AndroidEntryPoint
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

/**
 * Activity list fragment.
 */
@AndroidEntryPoint
class ActivityListFragment : Fragment() {
    private val viewModel: ActivityListViewModel by viewModels()

    @Inject
    internal lateinit var analytics: IAnalytics

    internal var listener: Listener? = null

    val hubId by extraNotNull<String>(HUB_ID)

    interface Listener {

        fun onEditLayoutClicked(hubId: String, activityId: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? Listener
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentActivityListBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.itemBinding = ItemBinding.of<ActivityListItem>(BR.item, R.layout.activity_row)
            .bindExtra(BR.onClickListener, viewModel)
        viewModel.setHubId(hubId) // TODO inject this
        viewModel.editLayout.observe(
            viewLifecycleOwner,
            Observer { listener?.onEditLayoutClicked(hubId, it.activityId) })
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.ACTIVITY_LIST)
    }

    companion object {
        val TAG: String = ActivityListFragment::class.java.simpleName
        private const val HUB_ID = "HUB_ID"

        fun newInstance(hubId: String) = ActivityListFragment().apply {
            arguments = Bundle().apply {
                putString(HUB_ID, hubId)
            }
        }
    }

}
