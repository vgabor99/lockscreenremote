package com.vargag99.lockscreenremote.ui.sideNav

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

data class SelectableHubInfo(val hubInfo: HubInfo, val selected: Boolean) {
    interface OnItemClickListener {
        fun onItemClick(item: SelectableHubInfo)
    }
}
