package com.vargag99.lockscreenremote.ui.privacySettings

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.databinding.ActivityPrivacySettingsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Privacy Settings.
 */
@AndroidEntryPoint
class PrivacySettingsActivity : AppCompatActivity() {
    private val viewModel: PrivacySettingsViewModel by viewModels()

    @Inject
    internal lateinit var navigator: PrivacySettingsNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityPrivacySettingsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_privacy_settings)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        viewModel.accepted.observe(this, Observer {
            val intent = intent
            if (intent != null && intent.getBooleanExtra(ARG_LAUNCH_ON_ACCEPT, false)) {
                navigator.runApp()
            }
            finish()
        })
        viewModel.rejected.observe(this, Observer {
            navigator.exit()
            finish()
        })
        setFinishOnTouchOutside(false)
    }

    // No analytics here
    // We might not have the permission

    companion object {
        const val ARG_LAUNCH_ON_ACCEPT = "ARG_LAUNCH_ON_ACCEPT"
    }
}
