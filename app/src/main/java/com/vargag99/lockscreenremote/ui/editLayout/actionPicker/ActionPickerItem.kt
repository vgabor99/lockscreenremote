package com.vargag99.lockscreenremote.ui.editLayout.actionPicker

/**
 * Item that appears in the Action picker dialog.
 */
sealed class ActionPickerItem {
    data class Header(val label: String) : ActionPickerItem()
    data class Action(val label: String, val icon: Int, val action: String?) : ActionPickerItem()
}

