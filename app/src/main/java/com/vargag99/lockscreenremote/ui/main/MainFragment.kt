package com.vargag99.lockscreenremote.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.R
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {
    private val viewModel: MainViewModel by viewModels()

    @Inject
    lateinit var navigator: MainFragmentNavigator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is MainViewState.Progress -> navigator.showProgress()
                is MainViewState.NoHubFound -> navigator.showNoHubFound()
                is MainViewState.NoWifi -> navigator.showNoWifi()
                is MainViewState.UnsupportedHub -> navigator.showUnsupportedHub(it.hubInfo.currentFwVersion)
                is MainViewState.XmppDisabled -> navigator.showXmppDisabled()
                is MainViewState.ActivityList -> navigator.showActivityList(it.hubInfo.uuid)

            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    companion object {
        val TAG: String = MainFragment::class.java.simpleName
        fun newInstance() = MainFragment()
    }

}