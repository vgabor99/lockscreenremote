package com.vargag99.lockscreenremote.ui.main

import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.RxViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DrawerViewModel @Inject constructor(analytics: IAnalytics) : RxViewModel() {
    val drawerOpen = MutableLiveData<Boolean>().apply { value = false }

    fun onBackPressed(): Boolean {
        if (drawerOpen.value == true) {
            drawerOpen.value = false
            return true
        }
        return false
    }

    init {
        drawerOpen.observeForever {
            if (it) {
                analytics.event(IAnalytics.SIDE_NAV_OPEN, IAnalytics.SIDE_NAV)
            }
        }
    }
}