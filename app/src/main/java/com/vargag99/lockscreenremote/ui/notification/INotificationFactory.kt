package com.vargag99.lockscreenremote.ui.notification

import android.app.Notification
import android.support.v4.media.session.MediaSessionCompat

interface INotificationFactory {
    fun areNotificationsEnabled(): Boolean
    fun createNotification(
        viewState: NotificationViewState,
        mediaSession: MediaSessionCompat
    ): Notification
}