package com.vargag99.lockscreenremote.ui.firstRun

import android.content.Intent
import androidx.core.content.ContextCompat
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.ui.notification.NotificationService
import javax.inject.Inject

class FirstRunNavigator @Inject constructor(private val activity: FirstRunActivity) {
    fun showNotification() {
        activity.overridePendingTransition(R.anim.enter_from_top, R.anim.exit_to_bottom)
        ContextCompat.startForegroundService(activity, Intent(NotificationService.ACTION_HEADSUP, null, activity, NotificationService::class.java))
    }

}