package com.vargag99.lockscreenremote.ui.editLayout.actionPicker

interface OnActionPickerItemClickListener {
    fun onItemClick(item: ActionPickerItem)
}

