package com.vargag99.lockscreenremote.ui.sideNav

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.app.rx
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@HiltViewModel
class HubSelectorViewModel @Inject constructor(
    private val preferences: Preferences,
    private val analytics: IAnalytics,
    repo: IRepo
) : RxViewModel(), SelectableHubInfo.OnItemClickListener {

    override fun onItemClick(item: SelectableHubInfo) {
        analytics.event(IAnalytics.HUB_SELECTED, IAnalytics.SIDE_NAV)
        preferences.hubId = item.hubInfo.uuid
        hubSelectionVar.value = item.hubInfo
    }

    private val hubSelectionVar = SingleLiveEvent<HubInfo>()
    val hubSelection: LiveData<HubInfo> = hubSelectionVar

    private val hubsVar = MutableLiveData<List<SelectableHubInfo>>()
    val hubs: LiveData<List<SelectableHubInfo>> = hubsVar

    init {
        val selectedId = preferences.rx().hubId
        val hubs = repo.getHubInfoList()
            .map { it.sortedWith(UiUtil.hubInfoComparator) }
        Observables.combineLatest(hubs, selectedId)
            .map { (hubs, selectedId) ->
                hubs.map { SelectableHubInfo(it, it.uuid == selectedId.value) }
            }
            .bindTo(hubsVar)
            .addTo(disposables)
    }
}