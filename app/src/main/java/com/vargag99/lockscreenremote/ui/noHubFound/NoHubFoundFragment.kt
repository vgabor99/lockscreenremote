package com.vargag99.lockscreenremote.ui.noHubFound

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NoHubFoundFragment : Fragment() {

    @Inject
    internal lateinit var analytics: IAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_no_hub_found, container, false)
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.NO_HUB_FOUND)
    }

    companion object {
        val TAG: String = NoHubFoundFragment::class.java.simpleName
        fun newInstance() = NoHubFoundFragment()
    }
}
