package com.vargag99.lockscreenremote.ui.editLayout.actionPicker

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.BR
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentActionListBinding
import com.vargag99.lockscreenremote.generic.extraNotNull
import com.vargag99.lockscreenremote.ui.editLayout.EditLayoutViewModel
import dagger.hilt.android.AndroidEntryPoint
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

/**
 * Display the list of available actions in a dialog.
 */
@AndroidEntryPoint
class ActionPickerDialogFragment : AppCompatDialogFragment() {
    private val viewModel: ActionPickerDialogViewModel by viewModels()
    private val editLayoutViewModel: EditLayoutViewModel by activityViewModels() // Activity sharing. The picker dialog is not a child.

    @Inject
    internal lateinit var analytics: IAnalytics
    val hubId by extraNotNull<String>(HUB_ID)
    val activityId by extraNotNull<String>(ACTIVITY_ID)
    val buttonIndex by extraNotNull<Int>(BUTTON_INDEX)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = FragmentActionListBinding.inflate(LayoutInflater.from(this.context))
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.itemBinding = ItemBinding.of<ActionPickerItem> { itemBinding, _, item ->
            itemBinding.set(
                BR.item,
                if (item is ActionPickerItem.Action) R.layout.icon_label_row_dense else R.layout.label_row_dense
            )
        }
            .bindExtra(BR.onClickListener, viewModel)
        viewModel.setArgs(hubId, activityId) // TODO inject this
        viewModel.actionPickerItemClicks.observe(this, Observer { item ->
            if (item is ActionPickerItem.Action) {
                editLayoutViewModel.setButtonAction(buttonIndex, item.action)
            }
            dismiss()
        })
        return AlertDialog.Builder(requireActivity()).apply {
            setTitle(getString(R.string.select_action))
            setView(view)
            setView(binding.root)
        }.create()

    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.ACTION_PICKER)
    }

    companion object {
        val TAG: String = ActionPickerDialogFragment::class.java.simpleName
        private const val HUB_ID = "HUB_ID"
        private const val ACTIVITY_ID = "ACTIVITY_ID"
        private const val BUTTON_INDEX = "BUTTON_INDEX"

        fun newInstance(hubId: String, activityId: String, buttonIndex: Int) = ActionPickerDialogFragment().apply {
            arguments = Bundle().apply {
                putString(HUB_ID, hubId)
                putString(ACTIVITY_ID, activityId)
                putInt(BUTTON_INDEX, buttonIndex)
            }
        }
    }
}
