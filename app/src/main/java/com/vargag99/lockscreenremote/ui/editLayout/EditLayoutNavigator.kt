package com.vargag99.lockscreenremote.ui.editLayout

import com.vargag99.lockscreenremote.ui.editLayout.actionPicker.ActionPickerDialogFragment
import timber.log.Timber
import javax.inject.Inject

class EditLayoutNavigator @Inject constructor(private val fragment: EditLayoutFragment) {

    fun showActionPicker(hubId: String, activityId: String, buttonIndex: Int) {
        Timber.d("showActionPicker buttonIndex=$buttonIndex")
        ActionPickerDialogFragment.newInstance(hubId, activityId, buttonIndex)
            .show(fragment.parentFragmentManager, ActionPickerDialogFragment.TAG)
    }
}