package com.vargag99.lockscreenremote.ui.binding

import android.text.method.MovementMethod
import android.widget.TextView
import androidx.databinding.BindingAdapter


@BindingAdapter("movementMethod")
fun setMovementMethod(textView: TextView, movementMethod: MovementMethod) {
    textView.movementMethod = movementMethod
}
