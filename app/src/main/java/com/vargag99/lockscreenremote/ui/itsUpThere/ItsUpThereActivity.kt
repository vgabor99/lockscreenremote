package com.vargag99.lockscreenremote.ui.itsUpThere

import android.os.Bundle
import android.view.Gravity
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.ActivityItsUpThereBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Onboarding activity, inform the user that the remote is running.
 */
@AndroidEntryPoint
class ItsUpThereActivity : AppCompatActivity() {
    internal val viewModel: ItsUpThereViewModel by viewModels()

    @Inject
    internal lateinit var analytics: IAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layoutParams = window.attributes
        layoutParams.gravity = Gravity.TOP
        val binding: ActivityItsUpThereBinding = DataBindingUtil.setContentView(this, R.layout.activity_its_up_there)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        viewModel.requestFinish.observe(this, Observer {
            finish()
        })
        setFinishOnTouchOutside(false)
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(this, IAnalytics.ITS_UP_THERE)
    }

    override fun onPause() {
        super.onPause()
        // We don't want this to stick around.
        // If the user locks the screen, or some other app comes in, better go away.
        finish()
    }

}
