package com.vargag99.lockscreenremote.ui.editLayout

import android.view.View

/**
 * Event coming from the edit layout view.
 */
interface OnEditLayoutViewListener {
    fun onButtonClick(button: EditLayoutButton)
    fun onButtonLongClick(view: View, button: EditLayoutButton)
    fun onForbiddenClick(view: View)
}
