package com.vargag99.lockscreenremote.ui.xmppDisabled

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentXmppDisabledBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class XmppDisabledFragment : Fragment() {
    private lateinit var binding: FragmentXmppDisabledBinding

    @Inject
    internal lateinit var analytics: IAnalytics

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_xmpp_disabled, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.XMPP_DISABLED)
    }

    companion object {
        val TAG: String = XmppDisabledFragment::class.java.simpleName
        fun newInstance() = XmppDisabledFragment()
    }
}
