package com.vargag99.lockscreenremote.ui.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.CATEGORY_TRANSPORT
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.media.app.NotificationCompat.MediaStyle
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.generic.FLAG_IMMUTABLE
import com.vargag99.lockscreenremote.generic.newIntent
import com.vargag99.lockscreenremote.ui.UiUtil
import com.vargag99.lockscreenremote.ui.main.MainActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Notification factory.
 * N.B. The notification appears in another process where support library is not available.
 * Therefore, we must make sure NOT to use anything from the support library. No widgets, no style
 * attributes, nothing. Widgets will crash, attributes will not be there so won't work.
 */
class NotificationFactory @Inject
internal constructor(
    @ApplicationContext private val context: Context,
    private val notificationManager: NotificationManager,
    private val notificationManagerCompat: NotificationManagerCompat
) : INotificationFactory {
    private val backgroundResId: Int

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createChannel()
        backgroundResId = UiUtil.resolveResourceAttribute(
            context,
            android.R.attr.selectableItemBackgroundBorderless
        )
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val name = context.getString(R.string.notification_channel_name)
        val description = context.getString(R.string.notification_channel_description)
        val channel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW)
        channel.description = description
        channel.setBypassDnd(true)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        channel.setShowBadge(false)
        notificationManager.createNotificationChannel(channel)
    }

    override fun areNotificationsEnabled(): Boolean {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.TIRAMISU) return true // Media session exemption
        if (!notificationManagerCompat.areNotificationsEnabled()) return false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = notificationManager.getNotificationChannel(CHANNEL_ID)
            if (channel.importance == NotificationManager.IMPORTANCE_NONE) return false
        }
        return true
    }

    override fun createNotification(
        viewState: NotificationViewState,
        mediaSession: MediaSessionCompat
    ): Notification {
        // Build the notification.
        // Depending on OS version / device / manufacturer, the notification uses data from
        // either the notification builder, or from the media session. Update both in a matching way.
        // Updating the media session is a side effect to creating the notification. :(
        val buttons = listOf(exitButtonData()) + viewState.buttons.subList(1, 5).mapNotNull { it?.toButtonData() }
        updateMediaSession(mediaSession, viewState, buttons)

        return NotificationCompat.Builder(context.applicationContext, CHANNEL_ID).run {
            val mediaStyle = MediaStyle()
            mediaStyle.setMediaSession(mediaSession.sessionToken)

            buttons.forEach { addActionFrom(it) }
            updateButtonsInCompactView(mediaStyle, buttons)

            color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
            setSmallIcon(R.drawable.ic_harmony_widget_white_24dp)
            setStyle(mediaStyle)
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            setContentIntent(getPendingMainActivityIntent())
            setDeleteIntent(getPendingIntent(NotificationService.ACTION_EXIT, null))
            setCategory(CATEGORY_TRANSPORT)
            setContentTitle(viewState.title)
            setContentText(viewState.info)
            setShowWhen(false)
            build()
        }
    }

    private fun updateMediaSession(
        mediaSession: MediaSessionCompat,
        viewState: NotificationViewState,
        buttons: List<ButtonData>
    ) {
        mediaSession.setMetadata(
            MediaMetadataCompat.Builder().run {
                putString(MediaMetadataCompat.METADATA_KEY_TITLE, viewState.title)
                putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, viewState.info)
                build()
            }
        )
        mediaSession.setPlaybackState(
            PlaybackStateCompat.Builder().run {
                setState(PlaybackStateCompat.STATE_PLAYING, 0, 1f)
                for (button in buttons) addCustomActionFrom(button)
                build()
            }
        )
    }

    private fun updateButtonsInCompactView(
        mediaStyle: MediaStyle,
        buttons: List<ButtonData>

    ) {
        when (buttons.size) {
            0 -> Unit
            1 -> mediaStyle.setShowActionsInCompactView(0)
            2 -> mediaStyle.setShowActionsInCompactView(0, 1)
            3 -> mediaStyle.setShowActionsInCompactView(0, 1, 2)
            4 -> mediaStyle.setShowActionsInCompactView(1, 2, 3)
            5 -> mediaStyle.setShowActionsInCompactView(2, 3, 4)
        }
    }

    private fun exitButtonData() = ButtonData(
        action = NotificationService.ACTION_EXIT,
        label = context.getString(R.string.exit),
        icon = R.drawable.btn_close,
        command = null
    )

    private fun UiData.ActionItem.toButtonData() = ButtonData(
        action = NotificationService.ACTION_CLICK,
        label = this.label,
        icon = UiUtil.safeIcon(this),
        command = this.action
    )

    private fun PlaybackStateCompat.Builder.addCustomActionFrom(data: ButtonData) =
        addCustomAction(
            PlaybackStateCompat.CustomAction.Builder(data.action, data.label, data.icon)
                .setExtras(Bundle().apply { putString(NotificationService.CUSTOM_ACTION_EXTRA_COMMAND, data.command) })
                .build()
        )

    private fun NotificationCompat.Builder.addActionFrom(data: ButtonData) =
        addAction(data.icon, data.label, getPendingIntent(data.action, data.command))

    private fun getPendingIntent(action: String, command: String?): PendingIntent {
        val uri = if (command == null) null else Uri.parse(command)
        val intent = Intent(action, uri, context, NotificationService::class.java)
        val requestCode = System.currentTimeMillis().toInt()
        return PendingIntent.getService(context, requestCode, intent, FLAG_IMMUTABLE)
    }

    private fun getPendingMainActivityIntent(): PendingIntent {
        val intent = newIntent<MainActivity>(context).apply {
            action = NotificationService.ACTION_STARTUI
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        val requestCode = System.currentTimeMillis().toInt()
        return PendingIntent.getActivity(context, requestCode, intent, FLAG_IMMUTABLE)
    }

    companion object {
        private const val CHANNEL_ID = "ChannelId"
    }

}
