package com.vargag99.lockscreenremote.ui.disabledNotifications

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.ActivityDisabledNotificationsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Show an error message and direct the user to system settings, if notifications are disabled.
 */
@AndroidEntryPoint
class DisabledNotificationsActivity : AppCompatActivity() {
    internal val viewModel: DisabledNotificationsViewModel by viewModels()

    @Inject
    internal lateinit var analytics: IAnalytics

    @Inject
    internal lateinit var navigator: DisabledNotificationsNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityDisabledNotificationsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_disabled_notifications)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        viewModel.requestFinish.observe(this, Observer {
            finish()
        })
        viewModel.showSettings.observe(this, Observer {
            navigator.openSettings()
            finish()
        })
        setFinishOnTouchOutside(false)
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(this, IAnalytics.DISABLED_NOTIFICATIONS)
    }

}
