package com.vargag99.lockscreenremote.ui.activityList

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.vargag99.lockscreenremote.R

@BindingAdapter("stateBackground")
fun setStateBackground(imageView: ImageView, state: ActivityListItem.State?) {
    when (state) {
        null -> Unit
        ActivityListItem.State.PROGRESS -> {
            imageView.background = AnimatedVectorDrawableCompat.create(imageView.context, R.drawable.circle_teal_animated_dot_40dp)?.apply {
                start()
            }
        }
        ActivityListItem.State.CURRENT -> imageView.setBackgroundResource(R.drawable.circle_teal_40dp)
        ActivityListItem.State.NORMAL -> imageView.setBackgroundResource(R.drawable.circle_gray_40dp)
    }
}