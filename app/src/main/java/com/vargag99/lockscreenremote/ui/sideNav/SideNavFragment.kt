package com.vargag99.lockscreenremote.ui.sideNav

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.BR
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.databinding.FragmentSideNavBinding
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.AndroidEntryPoint
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 * SideNav fragment
 */
@AndroidEntryPoint
class SideNavFragment : Fragment() {
    private val sideNavViewModel: SideNavViewModel by activityViewModels()
    private val hubSelectorViewModel: HubSelectorViewModel by activityViewModels()
    private val menuViewModel: SideNavMenuViewModel by activityViewModels()

    private var hubListAnimation: Animator? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentSideNavBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        // SideNav
        binding.sideNavViewModel = sideNavViewModel
        // Hub selector
        binding.hubSelectorViewModel = hubSelectorViewModel
        binding.hubSelectorItemBinding = ItemBinding.of<SelectableHubInfo>(BR.item, R.layout.hubinfo_row)
            .bindExtra(BR.onClickListener, hubSelectorViewModel)
        // Menu
        binding.menuViewModel = menuViewModel
        binding.menuItemBinding = ItemBinding.of<MenuItem>(BR.item, R.layout.icon_label_row_normal)
            .bindExtra(BR.onClickListener, menuViewModel)
        // Observe viewModel
        // TODO can I use data binding to make coordinated animation for multiple views? For now, do it here.
        sideNavViewModel.hubListShowing.observe(viewLifecycleOwner, Observer {
            showHubList(binding, it)
        })
        return binding.root
    }

    private fun showHubList(binding: FragmentSideNavBinding, show: Boolean) {
        val context = context ?: return
        hubListAnimation?.cancel()
        hubListAnimation = null
        hubListAnimation = UiUtil.crossFade(
            if (show) binding.hubRecyclerView else binding.menuRecyclerView,
            if (show) binding.menuRecyclerView else binding.hubRecyclerView,
            (UiUtil.dpToPixel(context, 24) * if (show) 1 else -1).toFloat()
        ).apply { start() }
    }

    companion object {
        fun newInstance() = SideNavFragment()
    }

}
