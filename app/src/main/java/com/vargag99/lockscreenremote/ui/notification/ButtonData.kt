package com.vargag99.lockscreenremote.ui.notification

data class ButtonData(
    val action: String, // Action identity.
    val label: String, // User-friendly label.
    val icon: Int, // User-facing icon.
    val command: String? // Command payload.
)
