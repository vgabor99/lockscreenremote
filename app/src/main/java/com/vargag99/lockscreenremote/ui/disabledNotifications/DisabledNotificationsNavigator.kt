package com.vargag99.lockscreenremote.ui.disabledNotifications

import android.content.Intent
import android.os.Build
import javax.inject.Inject

class DisabledNotificationsNavigator @Inject constructor(private val activity: DisabledNotificationsActivity) {
    fun openSettings() {
        Intent().apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                action = "android.settings.APP_NOTIFICATION_SETTINGS"
                putExtra("android.provider.extra.APP_PACKAGE", activity.packageName)
            } else {
                action = "android.settings.APP_NOTIFICATION_SETTINGS"
                putExtra("app_package", activity.packageName)
                putExtra("app_uid", activity.applicationInfo.uid)
            }
        }.let { activity.startActivity(it) }
    }
}