package com.vargag99.lockscreenremote.ui.aboutDialog

import android.content.ActivityNotFoundException
import android.content.Intent
import com.vargag99.lockscreenremote.R
import timber.log.Timber
import javax.inject.Inject

class AboutDialogNavigator @Inject constructor(private val dialog: AboutDialogFragment) {
    fun sendEmail(intent: Intent) {
        try {
            dialog.startActivity(Intent.createChooser(intent, dialog.getString(R.string.email_chooser_title)))
            dialog.dismiss()
        } catch (e: ActivityNotFoundException) {
            Timber.e(e, "Failed to send email")
        }
    }


}