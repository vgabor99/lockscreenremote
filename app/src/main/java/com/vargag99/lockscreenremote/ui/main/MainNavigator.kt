package com.vargag99.lockscreenremote.ui.main

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.generic.launchActivity
import com.vargag99.lockscreenremote.ui.UiUtil
import com.vargag99.lockscreenremote.ui.aboutDialog.AboutDialogFragment
import com.vargag99.lockscreenremote.ui.editLayout.EditLayoutFragment
import com.vargag99.lockscreenremote.ui.privacySettings.PrivacySettingsActivity
import timber.log.Timber
import javax.inject.Inject

class MainNavigator @Inject constructor(private val activity: MainActivity) {

    fun showMain() {
        val fm = activity.supportFragmentManager
        val fragment = fm.findFragmentById(R.id.fragment_container)
        if (fragment == null) {
            fm.beginTransaction()
                .replace(R.id.fragment_container, MainFragment.newInstance())
                .commit()
        }
    }

    fun showEditLayout(hubId: String, activityId: String) {
        val fm = activity.supportFragmentManager
        val fragment = fm.findFragmentById(R.id.fragment_container)
        if (fragment !is EditLayoutFragment ||
            fragment.hubId != hubId ||
            fragment.activityId != activityId
        ) {
            fm.beginTransaction()
                .setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
                )
                .replace(R.id.fragment_container, EditLayoutFragment.newInstance(hubId, activityId))
                .addToBackStack(EditLayoutFragment.TAG)
                .commit()
        }
    }


    fun showPrivacySettings() {
        activity.launchActivity<PrivacySettingsActivity>()
    }

    fun showAbout() {
        AboutDialogFragment.newInstance().show(activity.supportFragmentManager, null)
    }

    fun share() {
        val appLabel = UiUtil.getAppLabel(activity)
        val uri = playStoreHttpUri
        val message = activity.getString(R.string.share_message_appname_url, appLabel, uri)
        val sendIntent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, message)
            putExtra(Intent.EXTRA_SUBJECT, appLabel)
        }
        activity.startActivity(
            Intent.createChooser(
                sendIntent,
                activity.getText(R.string.share_chooser_title)
            )
        )
    }

    private val playStoreHttpUri: String
        get() = "http://play.google.com/store/apps/details?id=${activity.packageName}"

    fun rateInPlayMarket() {
        val uri = Uri.parse("market://details?id=${activity.packageName}")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri).apply {
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            )
        }
        try {
            activity.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            // Play Store app not found, try a browser.
            try {
                activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(playStoreHttpUri)))
            } catch (e1: Exception) {
                Timber.e(e1, "rateInPlayMarket failed")
            }
        }
    }

}