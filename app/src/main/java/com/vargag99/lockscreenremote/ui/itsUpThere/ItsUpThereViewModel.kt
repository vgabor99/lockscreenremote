package com.vargag99.lockscreenremote.ui.itsUpThere

import androidx.lifecycle.ViewModel
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ItsUpThereViewModel @Inject constructor(
    private val analytics: IAnalytics,
    private val preferences: Preferences
) : ViewModel() {

    var dontShowAgain = false

    fun onOkClick() {
        if (dontShowAgain) {
            preferences.showItsUpThere = false
        }
        analytics.event(if (dontShowAgain) IAnalytics.OK_DONT_SHOW_AGAIN else IAnalytics.OK, IAnalytics.ITS_UP_THERE)
        requestFinish.call()
    }

    val requestFinish = SingleLiveEvent<Unit>()

}