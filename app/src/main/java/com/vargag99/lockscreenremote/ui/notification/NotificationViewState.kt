package com.vargag99.lockscreenremote.ui.notification

import com.vargag99.lockscreenremote.app.UiData

data class NotificationViewState(
        val icon: Int,
        val title: String,
        val info: String,
        val progress: Boolean,
        val buttons: List<UiData.ActionItem?>
)
