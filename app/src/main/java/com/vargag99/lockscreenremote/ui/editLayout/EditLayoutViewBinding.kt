package com.vargag99.lockscreenremote.ui.editLayout

import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.ui.UiUtil

// The layout in the notification and here in the editor is reused (with layout include).
// It must support RemoteViews, which uses allows a limited set (not including data binding, obviously).
// So we have a custom binding here, from root level, to keep the included parts clean for RemoteViews.

// The root view has a single EditLayoutViewHolder instance, serving as a single entry point into
// everything that happens in this view (caching views, binding data, delegating observer calls).
// The binding adapters delegate all functionality to the EditLayoutViewHolder.

private fun getHolder(view: ViewGroup): EditLayoutViewHolder {
    return view.getTag(R.id.view_holder) as? EditLayoutViewHolder
            ?: EditLayoutViewHolder(view).also { view.setTag(R.id.view_holder, it) }
}

@BindingAdapter("editLayoutViewState")
fun setEditLayoutViewState(view: ViewGroup, viewState: EditLayoutViewState?) {

    viewState?.let { getHolder(view).bindViewState(it) }
}

@BindingAdapter("editLayoutListener")
fun setEditLayoutOnButtonClickListener(view: ViewGroup, listener: OnEditLayoutViewListener?) {

    getHolder(view).listener = listener
}

internal class EditLayoutViewHolder(view: ViewGroup) {
    private val card: View // Root of the whole notification.
    private val title: TextView
    private val info: TextView
    private val row1: View
    private val row2: View
    private val buttons: Array<View?>

    var listener: OnEditLayoutViewListener? = null

    fun bindViewState(state: EditLayoutViewState) {
        // Title and text
        title.text = state.title
        info.text = state.info
        // Buttons (note, button 0 is skipped because it is the fixed "X" button)
        for (i in 1..4) {
            val view = buttons[i]
            val button = state.buttons[i]
            (view as? ImageButton)?.let {
                it.button = button // Store the data
                it.setImageResource(button.icon) // Set icon
            }
        }
    }

    // We attach the backing EditLayoutViewState.EditLayoutButton to each ImageView with a tag
    private var View.button: EditLayoutButton?
        get() = getTag(R.id.button_item) as? EditLayoutButton
        set(value) = setTag(R.id.button_item, value)

    init {
        card = view.findViewById(R.id.card)
        title = view.findViewById<View>(R.id.title) as TextView
        info = view.findViewById<View>(R.id.info) as TextView
        row1 = view.findViewById(R.id.row1)
        row2 = view.findViewById(R.id.row2)

        buttons = arrayOfNulls(5)

        buttons[0] = view.findViewById(R.id.button21)
        buttons[1] = view.findViewById(R.id.button22)
        buttons[2] = view.findViewById(R.id.button23)
        buttons[3] = view.findViewById(R.id.button24)
        buttons[4] = view.findViewById(R.id.button25)

        // Setup click handling for buttons.
        // Also setup clickable background - we cannot do it in the XML because RemoteViews crashes on it.
        val backgroundResId = UiUtil.resolveResourceAttribute(view.context, R.attr.selectableItemBackgroundBorderless)
        buttons.forEach { buttonView ->
            if (buttonView != null) {
                buttonView.setOnClickListener { buttonView.button?.let { listener?.onButtonClick(it) } }
                buttonView.setOnLongClickListener {
                    buttonView.button?.let { listener?.onButtonLongClick(buttonView, it) }
                    true
                }
                buttonView.setBackgroundResource(backgroundResId)
            }
        }

        // Set click handlers
        val forbiddenClickListener = View.OnClickListener { listener?.onForbiddenClick(it) }
        row1.setOnClickListener(forbiddenClickListener)
        buttons[0]?.setOnClickListener(forbiddenClickListener)

        // Set clickable backgrounds for various parts.
        buttons[0]?.setBackgroundResource(backgroundResId)
        card.setBackgroundResource(backgroundResId)
    }
}
