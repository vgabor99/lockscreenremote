package com.vargag99.lockscreenremote.ui.unsupportedHub

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentUnsupportedHubBinding
import com.vargag99.lockscreenremote.generic.extraNotNull
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class UnsupportedHubFragment : Fragment() {
    private val viewModel: UnsupportedHubViewModel by viewModels()

    @Inject
    internal lateinit var analytics: IAnalytics

    val hubFwVersion by extraNotNull<String>(HUB_FW_VERSION)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentUnsupportedHubBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.hubFwVersion = hubFwVersion
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.UNSUPPORTED_HUB)
    }

    companion object {
        val TAG: String = UnsupportedHubFragment::class.java.simpleName
        private const val HUB_FW_VERSION = "HUB_FW_VERSION"
        fun newInstance(hubFwVersion: String) = UnsupportedHubFragment().apply {
            arguments = Bundle().apply {
                putString(HUB_FW_VERSION, hubFwVersion)
            }
        }
    }
}
