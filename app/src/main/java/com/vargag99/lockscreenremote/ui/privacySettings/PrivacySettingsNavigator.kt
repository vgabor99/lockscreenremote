package com.vargag99.lockscreenremote.ui.privacySettings

import android.content.Intent
import com.vargag99.lockscreenremote.generic.launchActivity
import com.vargag99.lockscreenremote.ui.launcher.LauncherActivity
import com.vargag99.lockscreenremote.ui.notification.NotificationService
import javax.inject.Inject

class PrivacySettingsNavigator @Inject constructor(private val activity: PrivacySettingsActivity) {
    fun exit() {
        activity.stopService(Intent(activity, NotificationService::class.java))
    }

    fun runApp() {
        activity.launchActivity<LauncherActivity>()
    }

}