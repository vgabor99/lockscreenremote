package com.vargag99.lockscreenremote.ui.launcher

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.generic.launchActivity
import com.vargag99.lockscreenremote.ui.disabledNotifications.DisabledNotificationsActivity
import com.vargag99.lockscreenremote.ui.firstRun.FirstRunActivity
import com.vargag99.lockscreenremote.ui.notification.NotificationService
import com.vargag99.lockscreenremote.ui.privacySettings.PrivacySettingsActivity
import javax.inject.Inject

class LauncherNavigator @Inject constructor(private val activity: LauncherActivity) {

    fun showPrivacySettings() {
        activity.launchActivity<PrivacySettingsActivity>(
            options = Bundle().apply { putBoolean(PrivacySettingsActivity.ARG_LAUNCH_ON_ACCEPT, true) }
        )
    }

    fun showDisabledNotifications() {
        activity.launchActivity<DisabledNotificationsActivity>()
    }

    fun showNotification() {
        ContextCompat.startForegroundService(activity, Intent(NotificationService.ACTION_HEADSUP, null, activity, NotificationService::class.java))
    }

    fun showFirstRun() {
        activity.launchActivity<FirstRunActivity>()
        activity.overridePendingTransition(R.anim.enter_from_top, R.anim.exit_to_bottom)
    }

}