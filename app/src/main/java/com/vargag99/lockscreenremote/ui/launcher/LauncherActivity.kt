package com.vargag99.lockscreenremote.ui.launcher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.ui.notification.INotificationFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * This invisible Activity has the sole purpose of providing a launcher icon for starting
 * the app (NotificationService or the onboarding).
 */
@AndroidEntryPoint
class LauncherActivity : AppCompatActivity() {

    @Inject
    internal lateinit var preferences: Preferences

    @Inject
    internal lateinit var notificationFactory: INotificationFactory

    @Inject
    internal lateinit var analytics: IAnalytics

    @Inject
    internal lateinit var navigator: LauncherNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analytics.setCurrentScreen(this, IAnalytics.LAUNCHER)
        // We can't have ViewModel in this activity.
        // The launcher must call finish() from its onCreate(), to avoid flicker. We can't afford asynchronity.
        when {
            BuildConfig.PRIVACY_SETTINGS && !(preferences.analyticsEnabled && preferences.crashlyticsEnabled) -> {
                navigator.showPrivacySettings()
            }
            !notificationFactory.areNotificationsEnabled() -> {
                navigator.showDisabledNotifications()
            }
            !preferences.firstRunDone -> {
                navigator.showFirstRun()
            }
            else -> {
                navigator.showNotification()
            }
        }
        finish()
    }

}
