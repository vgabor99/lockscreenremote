package com.vargag99.lockscreenremote.ui.editLayout

/**
 * A button in the layout editor.
 */
data class EditLayoutButton(
        val index: Int,
        val label: String,
        val icon: Int)