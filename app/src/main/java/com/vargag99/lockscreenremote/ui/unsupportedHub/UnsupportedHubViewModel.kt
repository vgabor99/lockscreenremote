package com.vargag99.lockscreenremote.ui.unsupportedHub

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class UnsupportedHubViewModel @Inject constructor() : ViewModel() {
    var hubFwVersion: String = ""
}