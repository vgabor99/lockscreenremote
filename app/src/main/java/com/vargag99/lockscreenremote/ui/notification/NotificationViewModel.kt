package com.vargag99.lockscreenremote.ui.notification

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.app.Actions
import com.vargag99.lockscreenremote.app.Icon
import com.vargag99.lockscreenremote.app.Layout
import com.vargag99.lockscreenremote.app.Layouts
import com.vargag99.lockscreenremote.app.UiData
import com.vargag99.lockscreenremote.generic.Optional
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.hubManager.HubAction
import com.vargag99.lockscreenremote.hubManager.HubManagerState
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub
import com.vargag99.lockscreenremote.hubManager.IHubManager
import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class NotificationViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val repo: IRepo,
    hubManager: IHubManager
) {

    private val disposables = CompositeDisposable()
    private var managerState: HubManagerState? = null
    private var activityConfig: UiData.ActivityConfig? = null
    private var layout: Layout? = null

    private val notificationStateVar = MutableLiveData<NotificationViewState>()
    val notificationState: LiveData<NotificationViewState> = notificationStateVar
    val actions = SingleLiveEvent<HubAction>()

    init {
        // Store the manager state, contributes to the UI state.
        hubManager.getState()
            .subscribeBy {
                this.managerState = it
                stateChanged() // Update UI for all manager changes.
            }
            .addTo(disposables)
        val hubObservable = hubManager.getState()
            .map { Optional(it.harmonyHub) }
            .notEmpty()
            .distinctUntilChanged()
        // Fetch UI data for the current activity.
        hubObservable
            .switchMap { hubIdAndActivityId(it) }
            .switchMap { (hubId, activityId) -> uiData(hubId, activityId) }
            .subscribe { (activityConfig, layout) ->
                this.activityConfig = activityConfig
                this.layout = layout
                stateChanged() // Update UI for activity changes and when UI data is loaded.
            }
            .addTo(disposables)
        // Update UI for hub state changes.
        hubObservable
            .switchMap { it.stateSignal() }
            .subscribe { stateChanged() }
            .addTo(disposables)
    }

    private fun stateChanged() {
        val managerState = this.managerState ?: return
        val hubInfo = managerState.hubInfo
        val harmonyHub = managerState.harmonyHub
        val activityConfig = this.activityConfig

        // Title, info and progress.
        var icon = R.drawable.ic_harmony_widget_white_24dp
        var title = context.getString(R.string.connecting)
        var info = hubInfo?.friendlyName ?: context.getString(R.string.no_hub_connected)
        var progress = true

        if (managerState.error is IHubManager.HubManagerError.NoWifi) {
            // No wifi: "No wifi / Not Connected"
            title = context.getString(R.string.no_wifi)
            info = context.getString(R.string.no_hub_connected)
            progress = false
        } else if (harmonyHub != null) {
            val hubError = harmonyHub.getState().hubError
            if (hubError == IHarmonyHub.HubError.NO_XMPP_API) {
                title = context.getString(R.string.unsupported_hub)
                progress = false
            } else if (hubError == IHarmonyHub.HubError.XMPP_CONNECTION_REFUSED) {
                title = context.getString(R.string.xmpp_disabled)
                progress = false
            } else {
                if (harmonyHub.getState().activityState.activity != null) {
                    // Get this from the hub, not from the UI data, to show as early as possible.
                    icon = Icon.forActivity(harmonyHub.getState().activityState.activity!!).mResId
                    title = UiUtil.getActivityTitle(context, harmonyHub.getState().activityState.activity!!)
                }
                when (harmonyHub.getState().activityState.activityState) {
                    IHarmonyHub.ActivityState.UNKNOWN -> Unit
                    IHarmonyHub.ActivityState.NO_ACTIVITY -> progress = false
                    IHarmonyHub.ActivityState.STARTING -> {
                        info = context.getString(R.string.starting)
                        progress = true
                    }
                    IHarmonyHub.ActivityState.POWERING_OFF -> {
                        icon = R.drawable.act_power_off
                        info = context.getString(R.string.powering_off)
                        progress = true
                    }
                    IHarmonyHub.ActivityState.RUNNING -> progress = false
                }
            }
        }
        if (progress) {
            icon = R.drawable.ic_harmony_widget_white_24dp
        }
        // Buttons
        val error = managerState.error != null || harmonyHub?.getState()?.hubError != IHarmonyHub.HubError.NONE
        val layout = if (progress || error) {
            // Don't show buttons during progress or error.
            Layouts.createEmpty()
        } else {
            // Otherwise, show the loaded layout.
            this.layout ?: Layouts.createEmpty()
        }
        val buttons = layout.mActions.take(5).map { action ->
            when {
                activityConfig == null -> null
                else -> Actions.findActionItem(activityConfig, action)
            }
        }

        notificationStateVar.value = NotificationViewState(
            icon = icon,
            title = title,
            info = info,
            progress = progress,
            buttons = buttons
        )
    }

    fun onButtonClick(action: String?) {
        val activityConfig = this.activityConfig ?: return
        Actions.decodeAction(activityConfig, action)?.let { actions.value = it }
    }

    private fun hubIdAndActivityId(hub: IHarmonyHub): Observable<Pair<String, String>> {
        val hubIdObservable = Observable.just(hub.info.uuid)
        val activityIdObservable = hub.stateSignal()
            .map { state -> Optional(state.activityState.activity) }
            .notEmpty()
            .map { activity -> activity.id }
            .distinctUntilChanged()
        return Observables.combineLatest(hubIdObservable, activityIdObservable) { hubId, activityId ->
            Pair(hubId, activityId)
        }
    }

    private fun uiData(hubId: String, activityId: String): Observable<Pair<UiData.ActivityConfig?, Layout?>> {
        val activityConfigObservable = repo.getActivityConfig(hubId, activityId)
        val layoutObservable = repo.getLayout(hubId, activityId)
        return Observables.combineLatest(activityConfigObservable, layoutObservable) { activityConfig, layout ->
            Pair(activityConfig.value, layout.value)
        }
            .startWith(Observable.just(Pair(null, null))) // Reset to nulls while loading
    }

    fun clear() {
        disposables.dispose()
    }
}