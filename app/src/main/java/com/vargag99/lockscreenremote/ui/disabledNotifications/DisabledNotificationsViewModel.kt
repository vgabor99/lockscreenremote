package com.vargag99.lockscreenremote.ui.disabledNotifications

import androidx.lifecycle.ViewModel
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class DisabledNotificationsViewModel @Inject constructor(private val analytics: IAnalytics) : ViewModel() {

    fun onCancelClick() {
        analytics.event(IAnalytics.CANCEL, IAnalytics.DISABLED_NOTIFICATIONS)
        requestFinish.call()
    }

    fun onSettingsClick() {
        analytics.event(IAnalytics.SETTINGS, IAnalytics.DISABLED_NOTIFICATIONS)
        showSettings.call()
    }

    val requestFinish = SingleLiveEvent<Unit>()

    val showSettings = SingleLiveEvent<Unit>()
}