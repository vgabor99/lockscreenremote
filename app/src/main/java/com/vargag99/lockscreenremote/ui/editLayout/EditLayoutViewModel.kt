package com.vargag99.lockscreenremote.ui.editLayout

import android.content.Context
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Actions
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
internal class EditLayoutViewModel @Inject constructor(
    @ApplicationContext context: Context, // TODO not clean
    repo: IRepo,
    private val analytics: IAnalytics
) : RxViewModel(), OnEditLayoutViewListener {

    override fun onButtonClick(button: EditLayoutButton) {
        showActionPicker.value = button
    }

    override fun onButtonLongClick(view: View, button: EditLayoutButton) {
        showButtonHelp.value = Pair(view, button)
    }

    override fun onForbiddenClick(view: View) {
        showForbiddenClick.value = view
    }

    val showActionPicker = SingleLiveEvent<EditLayoutButton>()

    val showButtonHelp = SingleLiveEvent<Pair<View, EditLayoutButton>>()

    val showForbiddenClick = SingleLiveEvent<View>()

    private val viewStateVar = MutableLiveData<EditLayoutViewState>()
    val viewState: LiveData<EditLayoutViewState> = viewStateVar

    private val args = PublishSubject.create<EditLayoutArgs>()

    fun setArgs(hubId: String, activityId: String) {
        // I really see no way to inject args into the viewModel
        // Even if I would, ViewModelProviders seems to cache viewModels based on their type,
        // so I would end up having the first viewModel and different arg instances being ignored (??)
        args.onNext(EditLayoutArgs(hubId, activityId))
    }

    private data class ButtonChange(val index: Int, val action: String?)

    private val buttonChanges = PublishSubject.create<ButtonChange>()
    fun setButtonAction(index: Int, action: String?) {
        buttonChanges.onNext(ButtonChange(index, action))
    }

    init {
        val appContext = context.applicationContext
        val args = args.distinctUntilChanged()
        val activityConfigObservable = args
            .switchMap { repo.getActivityConfig(it.hubId, it.activityId) }
            .notEmpty()
        val layoutObservable = args
            .switchMap { repo.getLayout(it.hubId, it.activityId) }
            .notEmpty()
        Observables.combineLatest(activityConfigObservable, layoutObservable) { activityConfig, layout ->
            val title = activityConfig.activity.label
            val info = activityConfig.hub.friendlyName
            val buttons = (0..4).map { i ->
                val action = layout.mActions[i]
                val item = Actions.findActionItem(activityConfig, action)
                item?.let { EditLayoutButton(i, it.label, UiUtil.safeIcon(it)) }
                    ?: EditLayoutButton(i, appContext.getString(R.string.unset_button), R.drawable.btn_unset)
            }
            EditLayoutViewState(title, info, buttons)
        }
            .bindTo(viewStateVar)
            .addTo(disposables)
        buttonChanges
            .withLatestFrom(layoutObservable, args)
            .subscribe { (change, layout, args) ->
                val newLayout = layout.copy().apply { mActions[change.index] = change.action }
                repo.updateLayout(args.hubId, args.activityId, newLayout)
            }
            .addTo(disposables)

    }
}