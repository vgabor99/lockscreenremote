package com.vargag99.lockscreenremote.ui.notification

import android.app.Service
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Bundle
import android.support.v4.media.session.MediaSessionCompat
import androidx.annotation.Nullable
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.AppLifecycleOwner
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.generic.launchActivity
import com.vargag99.lockscreenremote.hubManager.IHubManager
import com.vargag99.lockscreenremote.ui.itsUpThere.ItsUpThereActivity
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

/**
 * This service manages the notification UI.
 */
@AndroidEntryPoint
class NotificationService : LifecycleService() {
    @Inject
    internal lateinit var hubManager: IHubManager

    @Inject
    internal lateinit var viewModel: NotificationViewModel

    @Inject
    internal lateinit var notificationFactory: INotificationFactory

    @Inject
    internal lateinit var preferences: Preferences

    @Inject
    internal lateinit var analytics: IAnalytics
    private lateinit var mediaSession: MediaSessionCompat
    private val disposables = CompositeDisposable()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand $intent")
        super.onStartCommand(intent, flags, startId)
        if (intent != null) {
            val serviceAction = intent.action
            if (serviceAction != null) {
                handleNotificationAction(
                    serviceAction = serviceAction,
                    hubAction = intent.data?.toString()
                )
            }
        }
        return Service.START_STICKY
    }

    private fun onCustomAction(action: String?, extras: Bundle?) {
        Timber.d("onCustomAction $action, $extras")
        if (action != null) {
            handleNotificationAction(
                serviceAction = action,
                hubAction = extras?.getString(CUSTOM_ACTION_EXTRA_COMMAND)
            )
        }
    }

    private fun handleNotificationAction(serviceAction: String, hubAction: String?) {
        when (serviceAction) {
            ACTION_HEADSUP -> {
                if (preferences.showItsUpThere) {
                    launchItsUpThere()
                }
            }
            ACTION_EXIT -> {
                analytics.event(ACTION_EXIT, IAnalytics.NOTIFICATION)
                analytics.event(IAnalytics.SWIPE_OUT_EXIT, IAnalytics.EXIT)
                stopSelf()
            }
            ACTION_CLICK -> {
                analytics.event(ACTION_CLICK, IAnalytics.NOTIFICATION)
                click(hubAction)
            }
            else -> {
            }
        }
    }

    private fun launchItsUpThere() {
        // Launch the "It's up there!" dialog.
        launchActivity<ItsUpThereActivity> { flags = FLAG_ACTIVITY_NEW_TASK }
    }

    private fun click(@Nullable hubAction: String?) {
        Timber.d("click hubAction=%s", hubAction)
        if (hubAction != null) {
            viewModel.onButtonClick(hubAction)
        }
    }

    override fun onCreate() {
        Timber.i("onCreate")
        super.onCreate()
        AppLifecycleOwner.onServiceCreated() // Update lifecycle before injection, otherwise we get dummies.
        versionCheck()
        hubManager.requestExitSignal()
            .subscribeBy { stopSelf() }
            .addTo(disposables)
        viewModel.notificationState
            .observe(this, Observer {
                showNotification(it)
            })
        viewModel.actions
            .observe(this, Observer { hubManager.executeAction(it) })
        mediaSession = MediaSessionCompat(this.applicationContext, getString(R.string.app_name))
        mediaSession.setCallback(object : MediaSessionCompat.Callback() {
            override fun onCustomAction(action: String?, extras: Bundle?) {
                this@NotificationService.onCustomAction(action, extras)
            }
        })
        mediaSession.isActive = true
    }

    override fun onDestroy() {
        Timber.i("onDestroy")
        super.onDestroy()
        viewModel.clear()
        removeNotification()
        mediaSession.release()
        disposables.dispose()
        AppLifecycleOwner.onServiceDestroyed()
    }

    private fun showNotification(state: NotificationViewState) {
        val notification = notificationFactory.createNotification(state, mediaSession)
        startForeground(NOTIFICATION_ID, notification)
    }

    private fun removeNotification() {
        stopForeground(false)
    }

    private fun versionCheck() {
        // Future development placeholder.
        // Store the current version as setting; after update we may have to upgrade data.
        if (preferences.versionCode < BuildConfig.VERSION_CODE) {
            // Upgrade. For now, nothing.
            Timber.i("versionCheck upgraded to:%s", BuildConfig.VERSION_CODE)
            preferences.versionCode = BuildConfig.VERSION_CODE
        }
    }

    companion object {
        private const val NOTIFICATION_ID = 555
        const val ACTION_CLICK = "ACTION_CLICK"
        const val ACTION_EXIT = "ACTION_EXIT"
        const val ACTION_HEADSUP = "ACTION_HEADSUP"

        // Kept for clarity: this is an action on the notification.
        // But we can no longer handle this action in the service, because of notification trampoline
        // restrictions. The pending intent for this action must be the activity.
        const val ACTION_STARTUI = "ACTION_STARTUI"

        // Extras in the PlaybackStateCompat
        const val CUSTOM_ACTION_EXTRA_COMMAND = "COMMAND"
    }
}
