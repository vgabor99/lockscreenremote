package com.vargag99.lockscreenremote.ui.activityList

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Icon
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.SingleLiveEvent
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.hubManager.HubAction
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub
import com.vargag99.lockscreenremote.hubManager.IHubManager
import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class ActivityListViewModel @Inject constructor(
    @ApplicationContext context: Context, // TODO not clean
    private val hubManager: IHubManager,
    repo: IRepo,
    private val analytics: IAnalytics
) : RxViewModel(), ActivityListItem.OnItemClickListener {

    override fun onItemClick(item: ActivityListItem) {
        analytics.event(IAnalytics.SWITCH_ACTIVITY, IAnalytics.ACTIVITY_LIST)
        hubManager.executeAction(HubAction.StartActivity(item.activityId))
    }

    override fun onEditItemClick(item: ActivityListItem) {
        analytics.event(IAnalytics.EDIT_LAYOUT, IAnalytics.ACTIVITY_LIST)
        editLayout.value = item
    }

    val editLayout = SingleLiveEvent<ActivityListItem>()

    private val activityListVar = MutableLiveData<List<ActivityListItem>>().apply { value = listOf() }
    val activityList: LiveData<List<ActivityListItem>> = activityListVar

    private val hubIdVar = PublishSubject.create<String>()
    fun setHubId(hubId: String) {
        hubIdVar.onNext(hubId)
    }

    private fun activityState(
        activity: Data.Activity,
        currentActivity: IHarmonyHub.ActivitySstate
    ): ActivityListItem.State {
        // During poweroff, we still get the previous activity. But I want to set the PowerOff
        // activity (visibly) in progress, not the current activity which is being turned off.
        val currentId = if (currentActivity.activityState === IHarmonyHub.ActivityState.POWERING_OFF)
            IHarmonyHub.POWER_OFF_ACTIVITY_ID
        else
            currentActivity.activity?.id
        if (currentId != activity.id) return ActivityListItem.State.NORMAL // Not the current activity
        // The current activity may be in progress.
        val inProgress = when (currentActivity.activityState) {
            IHarmonyHub.ActivityState.POWERING_OFF, IHarmonyHub.ActivityState.STARTING -> true
            else -> false
        }
        return if (inProgress) ActivityListItem.State.PROGRESS else ActivityListItem.State.CURRENT
    }

    init {
        val currentActivity = hubManager
            .getState()
            .switchMap { it.harmonyHub?.stateSignal() ?: Observable.never() }
            .map { it.activityState }
            .distinctUntilChanged()
        val activitiesObservable = hubIdVar
            .switchMap { repo.getConfig(it) }
            .notEmpty()
            .map { it.activity.sortedWith(UiUtil.activityComparator) }
        Observables.combineLatest(activitiesObservable, currentActivity) { activities, current ->
            activities.map {
                ActivityListItem(
                    activityId = it.id,
                    label = UiUtil.getActivityTitle(context, it),
                    icon = Icon.forActivity(it).mResId,
                    state = activityState(it, current)
                )
            }
        }
            .bindTo(activityListVar)
            .addTo(disposables)
    }
}