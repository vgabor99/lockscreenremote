package com.vargag99.lockscreenremote.ui.sideNav

import android.view.MenuItem

interface OnMenuItemClickListener {
    fun onMenuItemClick(item: MenuItem)
}