package com.vargag99.lockscreenremote.ui.aboutDialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentAboutBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * About dialog.
 */
@AndroidEntryPoint
class AboutDialogFragment : AppCompatDialogFragment() {
    private val viewModel: AboutDialogViewModel by viewModels()

    @Inject
    internal lateinit var analytics: IAnalytics

    @Inject
    internal lateinit var navigator: AboutDialogNavigator

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding = FragmentAboutBinding.inflate(LayoutInflater.from(this.context))
        binding.viewmodel = viewModel
        viewModel.sendEmail.observe(this, Observer {
            navigator.sendEmail(it)
        })
        return AlertDialog.Builder(requireActivity()).apply {
            setView(binding.root)
        }.create()
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.ABOUT)
    }

    companion object {
        val TAG: String = AboutDialogFragment::class.java.simpleName
        fun newInstance() = AboutDialogFragment()

    }
}
