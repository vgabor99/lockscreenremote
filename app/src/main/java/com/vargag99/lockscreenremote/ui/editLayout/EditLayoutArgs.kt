package com.vargag99.lockscreenremote.ui.editLayout

data class EditLayoutArgs(val hubId: String, val activityId: String)