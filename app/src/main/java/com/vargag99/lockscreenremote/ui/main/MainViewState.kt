package com.vargag99.lockscreenremote.ui.main

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

sealed class MainViewState {
    object Progress : MainViewState()
    object NoWifi : MainViewState()
    object NoHubFound : MainViewState()
    class UnsupportedHub(val hubInfo: HubInfo) : MainViewState()
    class XmppDisabled(val hubInfo: HubInfo) : MainViewState()
    class ActivityList(val hubInfo: HubInfo) : MainViewState()
}