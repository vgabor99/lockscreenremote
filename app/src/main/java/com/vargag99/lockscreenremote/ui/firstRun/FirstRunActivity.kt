package com.vargag99.lockscreenremote.ui.firstRun

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.TimeInterpolator
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.databinding.ActivityFirstRunBinding
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.AndroidEntryPoint
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import javax.inject.Inject

@AndroidEntryPoint
class FirstRunActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFirstRunBinding

    @Inject
    internal lateinit var analytics: IAnalytics

    @Inject
    internal lateinit var preferences: Preferences

    @Inject
    internal lateinit var navigator: FirstRunNavigator
    private var lastStep = STEP_NONE // This is not persisted. Shows what this UI instance did last.
    private var nextStep = STEP_SLIDE_IN_CARD // This is persisted. Shows the next step in UI flow.

    private val stepSlideInCard = object : Step {

        override fun step() {
            animate(binding.card, "translationY", 0, 500, 500, OvershootInterpolator()) { next() }
        }

        override fun skip() {
            binding.card.translationY = 0f
        }
    }

    private val stepShowLeftSide = object : Step {

        override fun step() {
            analytics.event(IAnalytics.LEFT_SIDE, IAnalytics.FIRST_RUN)
            MaterialTapTargetPrompt.Builder(this@FirstRunActivity)
                .setTarget(binding.notification.text)
                .setPrimaryText(R.string.first_run_left_side_primary)
                .setSecondaryText(R.string.first_run_left_side_secondary)
                .setTextGravity(Gravity.LEFT)
                .setPromptStateChangeListener { _, state ->
                    when (state) {
                        MaterialTapTargetPrompt.STATE_FOCAL_PRESSED -> analytics.event(
                            IAnalytics.LEFT_SIDE_TAP,
                            IAnalytics.FIRST_RUN
                        )
                        MaterialTapTargetPrompt.STATE_FINISHED -> next()
                    }
                }
                .show()
        }

        override fun skip() {}
    }

    private val stepShowButtons = object : Step {

        override fun step() {
            analytics.event(IAnalytics.BUTTONS, IAnalytics.FIRST_RUN)
            MaterialTapTargetPrompt.Builder(this@FirstRunActivity)
                .setTarget(binding.notification.buttons)
                .setPrimaryText(R.string.first_run_buttons_primary)
                .setSecondaryText(R.string.first_run_buttons_secondary)
                .setTextGravity(Gravity.RIGHT)
                .setPromptStateChangeListener { _, state ->
                    when (state) {
                        MaterialTapTargetPrompt.STATE_FOCAL_PRESSED -> analytics.event(
                            IAnalytics.BUTTONS_TAP,
                            IAnalytics.FIRST_RUN
                        )
                        MaterialTapTargetPrompt.STATE_FINISHED -> next()
                    }
                }
                .show()
        }

        override fun skip() {}
    }

    private val stepSlideInDownArrow = object : Step {

        override fun step() {
            animate(binding.arrowDown, "translationY", 0, 0, 500, OvershootInterpolator()) { next() }
        }

        override fun skip() {
            binding.arrowDown.translationY = 0f
        }
    }

    private val stepShowPullDown = object : Step {

        override fun step() {
            analytics.event(IAnalytics.PULL, IAnalytics.FIRST_RUN)
            MaterialTapTargetPrompt.Builder(this@FirstRunActivity)
                .setTarget(binding.arrowDown)
                .setPrimaryText(R.string.first_run_pull_primary)
                .setSecondaryText(R.string.first_run_pull_secondary)
                .setPromptStateChangeListener { _, state ->
                    when (state) {
                        MaterialTapTargetPrompt.STATE_FOCAL_PRESSED -> analytics.event(
                            IAnalytics.PULL_TAP,
                            IAnalytics.FIRST_RUN
                        )
                        MaterialTapTargetPrompt.STATE_FINISHED -> {
                            animate(binding.arrowDown, "alpha", 0, 0, 300, LinearInterpolator())
                            // Slide in extra buttons. The buttons have an extra 8dp gray top, to provide overlap.
                            val overlap = resources.getDimensionPixelSize(R.dimen.extra_buttons_overlap)
                            animate(
                                binding.extraButtons.root,
                                "translationY",
                                (binding.extraButtons.root.height - overlap).toLong(),
                                0,
                                300,
                                AccelerateInterpolator()
                            ) { next() }
                        }
                    }
                }
                .show()
        }

        override fun skip() {
            binding.arrowDown.alpha = 0f
            val overlap = resources.getDimensionPixelSize(R.dimen.extra_buttons_overlap)
            binding.extraButtons.root.translationY = (binding.extraButtons.root.height - overlap).toFloat()
        }
    }

    private val stepShowExitButton = object : Step {

        override fun step() {
            analytics.event(IAnalytics.EXIT_BUTTON, IAnalytics.FIRST_RUN)
            MaterialTapTargetPrompt.Builder(this@FirstRunActivity)
                .setTarget(binding.extraButtons.button21)
                .setPrimaryText(R.string.first_run_show_exit_primary)
                .setSecondaryText(R.string.first_run_show_exit_secondary)
                .setPromptStateChangeListener { _, state ->
                    when (state) {
                        MaterialTapTargetPrompt.STATE_FOCAL_PRESSED -> analytics.event(
                            IAnalytics.EXIT_BUTTON_TAP,
                            IAnalytics.FIRST_RUN
                        )
                        MaterialTapTargetPrompt.STATE_FINISHED -> animate(
                            binding.card,
                            "translationX",
                            binding.card.width.toLong(),
                            0,
                            300,
                            FastOutLinearInInterpolator(),
                            { next() })
                    }
                }
                .show()
        }

        override fun skip() {
            binding.card.translationX = binding.card.width.toFloat()
        }
    }

    private val stepDone = object : Step {

        override fun step() {
            done()
        }

        override fun skip() {
            step() // This cannot be skipped.
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirstRunBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            nextStep = savedInstanceState.getInt(NEXT_STEP)
        }
        binding.card.translationY = UiUtil.dpToPixel(this, -200).toFloat() // Move out of the setCurrentScreen.
        binding.arrowDown.translationY = UiUtil.dpToPixel(this, -300).toFloat() // Move out of the setCurrentScreen.
        binding.skip.setOnClickListener {
            analytics.event(IAnalytics.SKIP, IAnalytics.FIRST_RUN)
            nextStep = STEP_DONE
            done() // Not going through catchUp, just finish. No UI flicker needed.
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(NEXT_STEP, nextStep)
    }

    public override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(this, IAnalytics.FIRST_RUN)
        // The view is not laid out yet in onResume(), so we cannot catch up because
        // some steps need laid-out UI to determine translation offsets. So we post this on one of
        // the root-ish views. Messages to Views are delivered after the view is measured.
        binding.card.post { catchUp() }
    }

    private fun next() {
        if (nextStep < STEP_DONE) {
            nextStep++
            catchUp()
        }
    }

    private fun catchUp() {
        // Catch up from lastStep to nextStep.
        // Skip all intermediate steps until nextStep, then execute nextStep.
        while (lastStep < nextStep) {
            lastStep++
            if (lastStep == nextStep) {
                // This is the target step: execute and return.
                // The target step will call next() again when done.
                getStep(lastStep).step()
                return
            } else {
                // This is an intermedeiate step: skip and keep going.
                getStep(lastStep).skip()
            }
        }
    }

    private fun getStep(step: Int): Step {
        when (step) {
            STEP_SLIDE_IN_CARD -> return stepSlideInCard
            STEP_SHOW_LEFT_SIDE -> return stepShowLeftSide
            STEP_SHOW_BUTTONS -> return stepShowButtons
            STEP_SLIDE_IN_DOWN_ARROW -> return stepSlideInDownArrow
            STEP_SHOW_PULL_DOWN -> return stepShowPullDown
            STEP_SHOW_EXIT_BUTTON -> return stepShowExitButton
            STEP_DONE -> return stepDone
        }
        throw IllegalArgumentException("Invalid step $step")
    }

    private interface Step {
        /**
         * Execute an (asynchronous) step.
         */
        fun step()

        fun skip()
    }

    private fun done() {
        preferences.firstRunDone = true
        finish()
        navigator.showNotification()
    }

    private fun animate(
        view: View,
        property: String,
        value: Long,
        startDelay: Long,
        duration: Long,
        interpolator: TimeInterpolator,
        onEnd: (() -> Unit)? = null
    ): Animator {
        return ObjectAnimator.ofFloat(view, property, value.toFloat()).also {
            it.startDelay = startDelay
            it.duration = duration
            it.interpolator = interpolator
            if (onEnd != null) {
                it.addListener(object : AnimationEndListener() {
                    override fun onAnimationEnd(animation: Animator) {
                        onEnd()
                    }
                })
            }
            it.start()
        }
    }

    /**
     * Convenience class for shorter code - we want to override onAnimationEnd only.
     */
    abstract inner class AnimationEndListener : Animator.AnimatorListener {

        override fun onAnimationStart(animation: Animator) {}

        override fun onAnimationCancel(animation: Animator) {}

        override fun onAnimationRepeat(animation: Animator) {}
    }

    companion object {
        val TAG: String = FirstRunActivity::class.java.simpleName
        private const val NEXT_STEP = "NEXT_STEP"
        private const val STEP_NONE = 0
        private const val STEP_SLIDE_IN_CARD = 1
        private const val STEP_SHOW_LEFT_SIDE = 2
        private const val STEP_SHOW_BUTTONS = 3
        private const val STEP_SLIDE_IN_DOWN_ARROW = 4
        private const val STEP_SHOW_PULL_DOWN = 5
        private const val STEP_SHOW_EXIT_BUTTON = 6
        private const val STEP_DONE = 7
    }

}
