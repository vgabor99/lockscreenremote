package com.vargag99.lockscreenremote.ui.editLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.databinding.FragmentEditLayoutBinding
import com.vargag99.lockscreenremote.generic.extraNotNull
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

/**
 * Edit layout fragment.
 */
@AndroidEntryPoint
class EditLayoutFragment : Fragment() {
    private val viewModel: EditLayoutViewModel by activityViewModels() // Activity sharing. The picker dialog is not a child.

    @Inject
    internal lateinit var analytics: IAnalytics

    @Inject
    internal lateinit var navigator: EditLayoutNavigator

    private var toast: Toast? = null

    val hubId by extraNotNull<String>(HUB_ID)

    val activityId by extraNotNull<String>(ACTIVITY_ID)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentEditLayoutBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.setArgs(hubId, activityId) // TODO inject this
        binding.viewModel = viewModel
        binding.listener = viewModel
        viewModel.showActionPicker.observe(
            viewLifecycleOwner,
            Observer { navigator.showActionPicker(hubId, activityId, it.index) })
        viewModel.showButtonHelp.observe(viewLifecycleOwner, Observer { showButtonHelp(it.first, it.second) })
        viewModel.showForbiddenClick.observe(viewLifecycleOwner, Observer { showForbiddenClick(it) })
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        analytics.setCurrentScreen(activity, IAnalytics.EDIT_LAYOUT)
    }

    private fun showButtonHelp(view: View, button: EditLayoutButton) {
        Timber.d("showButtonHelp index=${button.index}")
        showToastAboveView(button.label, view)
    }

    private fun showForbiddenClick(view: View) {
        showToastAboveView(getString(R.string.this_area_cannot_be_customized), view)
        view.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.shake))
    }

    private fun showToastAboveView(message: String, view: View) {
        toast?.cancel()
        toast = null
        toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT).apply {
            UiUtil.positionToastAboveView(this, view)
            show()
        }
    }

    companion object {
        @JvmField
        val TAG: String = EditLayoutFragment::class.java.simpleName
        private const val HUB_ID = "HUB_ID"
        private const val ACTIVITY_ID = "ACTIVITY_ID"
        fun newInstance(hubId: String, activityId: String) = EditLayoutFragment().apply {
            arguments = Bundle().apply {
                putString(HUB_ID, hubId)
                putString(ACTIVITY_ID, activityId)
            }
        }
    }
}
