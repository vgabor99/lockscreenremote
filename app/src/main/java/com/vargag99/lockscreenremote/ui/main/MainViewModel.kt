package com.vargag99.lockscreenremote.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.generic.RxViewModel
import com.vargag99.lockscreenremote.generic.bindTo
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub
import com.vargag99.lockscreenremote.hubManager.IHubManager
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    hubManager: IHubManager,
    @ApplicationContext context: Context
) : RxViewModel() {

    private val viewStateVar = MutableLiveData<MainViewState>()
    val viewState: LiveData<MainViewState> = viewStateVar

    private val toolbarTitleVar = MutableLiveData<String>()
    val toolbarTitle: LiveData<String> = toolbarTitleVar

    init {
        hubManager.getState()
            .map { state ->
                when {
                    state.error is IHubManager.HubManagerError.NoWifi -> MainViewState.NoWifi
                    state.error is IHubManager.HubManagerError.NoHubFound -> MainViewState.NoHubFound
                    state.hubInfo == null -> MainViewState.Progress
                    state.harmonyHub?.getState()?.hubError == IHarmonyHub.HubError.NO_XMPP_API -> MainViewState.UnsupportedHub(
                        state.hubInfo
                    )
                    state.harmonyHub?.getState()?.hubError == IHarmonyHub.HubError.XMPP_CONNECTION_REFUSED -> MainViewState.XmppDisabled(
                        state.hubInfo
                    )
                    else -> MainViewState.ActivityList(state.hubInfo)
                }
            }
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .bindTo(viewStateVar)
            .addTo(disposables)
        hubManager.getState()
            .map { state ->
                // Get this from the actually connected hub, not the hubInfo.
                // When we cannot connect, it is clearer to see "not connected" than
                // the hub name and an empty activity list.
                state.harmonyHub?.info?.friendlyName
                    ?: context.getString(R.string.no_hub_connected)
            }
            .distinctUntilChanged()
            .bindTo(toolbarTitleVar)
            .addTo(disposables)
    }
}