package com.vargag99.lockscreenremote.ui.main

import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.databinding.ActivityMainBinding
import com.vargag99.lockscreenremote.generic.newIntent
import com.vargag99.lockscreenremote.ui.activityList.ActivityListFragment
import com.vargag99.lockscreenremote.ui.notification.NotificationService
import com.vargag99.lockscreenremote.ui.sideNav.HubSelectorViewModel
import com.vargag99.lockscreenremote.ui.sideNav.SideNavMenuViewModel
import com.vargag99.lockscreenremote.ui.sideNav.SideNavViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ActivityListFragment.Listener {
    @Inject
    internal lateinit var analytics: IAnalytics
    private val viewModel: MainViewModel by viewModels()
    private val drawerViewModel: DrawerViewModel by viewModels()
    private val sideNavViewModel: SideNavViewModel by viewModels()
    private val hubSelectorViewModel: HubSelectorViewModel by viewModels()
    private val menuViewModel: SideNavMenuViewModel by viewModels()

    @Inject
    internal lateinit var navigator: MainNavigator

    private lateinit var toolbar: Toolbar

    @Inject
    internal lateinit var preferences: Preferences
    private lateinit var navigationIcon: DrawerArrowDrawable
    private val navigationIconAnimator = FragmentManager.OnBackStackChangedListener {
        val drawer = supportFragmentManager.backStackEntryCount == 0
        ObjectAnimator.ofFloat(navigationIcon, "progress", if (drawer) 0.0f else 1.0f).start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.drawerViewModel = drawerViewModel
        // Observe viewModels
        hubSelectorViewModel.hubSelection.observe(this, Observer {
            // When a new hub is selected, close the sideNav
            drawerViewModel.drawerOpen.value = false
        })
        menuViewModel.menuItemClicks.observe(this, Observer {
            // When a menu item is clicked, close the sideNav
            drawerViewModel.drawerOpen.value = false
            onNavigationItemSelected(it)
        })
        toolbar = findViewById(R.id.toolbar)
        navigationIcon = DrawerArrowDrawable(this).apply {
            color = Color.WHITE
        }
        toolbar.navigationIcon = navigationIcon
        toolbar.setNavigationOnClickListener {
            if (supportFragmentManager.backStackEntryCount == 0) {
                drawerViewModel.drawerOpen.value = true
            } else {
                onBackPressed()
            }
        }
        drawerViewModel.drawerOpen.observe(this, Observer {
            // When the drawer is closed, we close the hub list (sideNav always starts in menu view)
            if (!it) {
                sideNavViewModel.closeHubList()
            }
        })
        if (savedInstanceState != null) {
            navigationIcon.progress = savedInstanceState.getFloat(NAVIGATION_ICON_PROGRESS)
        } else {
            reportNotificationAction(intent)
            navigator.showMain()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putFloat(NAVIGATION_ICON_PROGRESS, navigationIcon.progress)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        reportNotificationAction(intent)
    }

    private fun reportNotificationAction(intent: Intent?) {
        if (intent?.action == NotificationService.ACTION_STARTUI) {
            analytics.event(NotificationService.ACTION_STARTUI, IAnalytics.NOTIFICATION)
        }
    }

    override fun onStart() {
        super.onStart()
        supportFragmentManager.addOnBackStackChangedListener(navigationIconAnimator)
        ContextCompat.startForegroundService(this, newIntent<NotificationService>(this))
    }

    override fun onStop() {
        super.onStop()
        supportFragmentManager.removeOnBackStackChangedListener(navigationIconAnimator)
    }

    override fun onResume() {
        super.onResume()
        if (BuildConfig.PRIVACY_SETTINGS && !(preferences.analyticsEnabled && preferences.crashlyticsEnabled)) {
            finish()
        }
    }

    override fun onBackPressed() {
        if (!drawerViewModel.onBackPressed()) {
            super.onBackPressed()
        }
    }

    private fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_share -> navigator.share()
            R.id.nav_rate -> navigator.rateInPlayMarket()
            R.id.nav_privacy_settings -> navigator.showPrivacySettings()
            R.id.nav_about -> navigator.showAbout()
        }
        return true
    }

    override fun onEditLayoutClicked(hubId: String, activityId: String) {
        navigator.showEditLayout(hubId, activityId)
    }

    companion object {
        private const val NAVIGATION_ICON_PROGRESS = "NAVIGATION_ICON_PROGRESS"
    }

}
