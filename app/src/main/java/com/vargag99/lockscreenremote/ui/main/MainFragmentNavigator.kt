package com.vargag99.lockscreenremote.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.ui.activityList.ActivityListFragment
import com.vargag99.lockscreenremote.ui.noHubFound.NoHubFoundFragment
import com.vargag99.lockscreenremote.ui.noWifi.NoWifiFragment
import com.vargag99.lockscreenremote.ui.progress.ProgressFragment
import com.vargag99.lockscreenremote.ui.unsupportedHub.UnsupportedHubFragment
import com.vargag99.lockscreenremote.ui.xmppDisabled.XmppDisabledFragment
import javax.inject.Inject

class MainFragmentNavigator @Inject constructor(private val fragment: MainFragment) {

    private fun replaceFragment(check: (fragment: Fragment?) -> Boolean, create: () -> Fragment) {
        val fm = fragment.childFragmentManager
        val fragment = fm.findFragmentById(R.id.main_container)
        if (!check(fragment)) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fm.beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.main_container, create())
                    .commit()
        }
    }

    fun showNoHubFound() {
        replaceFragment(
                check = { it is NoHubFoundFragment },
                create = { NoHubFoundFragment.newInstance() }
        )
    }

    fun showProgress() {
        replaceFragment(
                check = { it is ProgressFragment },
                create = { ProgressFragment.newInstance() }
        )
    }

    fun showActivityList(hubId: String) {
        replaceFragment(
                check = { it is ActivityListFragment && it.hubId == hubId },
                create = { ActivityListFragment.newInstance(hubId) }
        )
    }

    fun showNoWifi() {
        replaceFragment(
                check = { it is NoWifiFragment },
                create = { NoWifiFragment.newInstance() }
        )
    }

    fun showUnsupportedHub(hubFwVersion: String) {
        replaceFragment(
                check = { it is UnsupportedHubFragment && it.hubFwVersion == hubFwVersion },
                create = { UnsupportedHubFragment.newInstance(hubFwVersion) }
        )
    }

    fun showXmppDisabled() {
        replaceFragment(
                check = { it is XmppDisabledFragment },
                create = { XmppDisabledFragment.newInstance() }
        )
    }

}