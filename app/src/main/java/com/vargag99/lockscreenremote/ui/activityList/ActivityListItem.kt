package com.vargag99.lockscreenremote.ui.activityList

data class ActivityListItem(
        val activityId: String,
        val label: String,
        val icon: Int,
        val state: State) {

    enum class State { NORMAL, CURRENT, PROGRESS }

    interface OnItemClickListener {
        fun onItemClick(item: ActivityListItem)
        fun onEditItemClick(item: ActivityListItem)
    }
}