package com.vargag99.lockscreenremote.ui.binding

import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

@BindingAdapter("drawableRight")
fun setDrawableRight(textView: TextView, resId: Int?) {
    val drawable: Drawable? = when (resId) {
        null, 0 -> null
        else -> ContextCompat.getDrawable(textView.context, resId)
    }
    textView.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
}
