package com.vargag99.lockscreenremote.ui;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.vargag99.lockscreenremote.R;
import com.vargag99.lockscreenremote.app.UiData;
import com.vargag99.lockscreenremote.harmonyApi.Data;
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo;
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub;

import java.util.Comparator;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * UI utilities.
 */

public class UiUtil {
    private static final Pattern CAMEL_SPLIT_PATTERN = Pattern.compile("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])");
    public static final Comparator<Data.Activity> activityComparator = new Comparator<Data.Activity>() {
        @Override
        public int compare(Data.Activity left, Data.Activity right) {
            // The PowerOff has 0 activity order, we have to adjust it (in our case, to smallest).
            int l = left.id.equals(IHarmonyHub.POWER_OFF_ACTIVITY_ID) ? -1 : left.activityOrder;
            int r = right.id.equals(IHarmonyHub.POWER_OFF_ACTIVITY_ID) ? -1 : right.activityOrder;
            return l - r;
        }
    };

    public static final Comparator<HubInfo> hubInfoComparator = new Comparator<HubInfo>() {
        @Override
        public int compare(HubInfo left, HubInfo right) {
            return left.friendlyName.compareToIgnoreCase(right.friendlyName);
        }
    };

    @NonNull
    public static String getActivityTitle(@NonNull Context context, @NonNull Data.Activity activity) {
        switch (activity.type) {
            case "PowerOff":
                return context.getString(R.string.power_off);
            default:
                return activity.label;
        }
    }

    @NonNull
    public static String camelCaseSplit(@NonNull String s) {
        return TextUtils.join(" ", CAMEL_SPLIT_PATTERN.split(s));
    }

    public static Animator crossFade(@NonNull final View fadeInView, @NonNull final View fadeOutView, float dist) {
        Animator fadeIn = ObjectAnimator.ofFloat(fadeInView, "alpha", 0.f, 1.f);
        fadeIn.setDuration(200);
        Animator slideIn = ObjectAnimator.ofFloat(fadeInView, "translationY", -dist, 0);
        fadeIn.setDuration(200);
        Animator fadeOut = ObjectAnimator.ofFloat(fadeOutView, "alpha", 1.f, 0.f);
        fadeOut.setDuration(200);
        Animator slideOut = ObjectAnimator.ofFloat(fadeOutView, "translationY", 0, dist);

        fadeIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                fadeInView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        fadeOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                fadeOutView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        AnimatorSet crossFade = new AnimatorSet();
        crossFade.playTogether(fadeIn, slideIn, fadeOut, slideOut);
        crossFade.setInterpolator(new OvershootInterpolator());
        return crossFade;
    }

    public static int dpToPixel(@NonNull Context context, int dp) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public static int resolveResourceAttribute(@NonNull Context context, int attribute) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attribute, typedValue, true);
        return typedValue.resourceId;
    }

    public static void positionToastAboveView(@NonNull Toast toast, @NonNull View view) {
        int xOffset = 0;
        int yOffset = 0;
        Rect rect = new Rect();
        View parent = (View) view.getParent();
        int parentHeight = parent.getHeight();
        if (view.getGlobalVisibleRect(rect)) {
            View root = view.getRootView();
            int halfWidth = root.getRight() / 2;
            int halfHeight = root.getBottom() / 2;
            int parentCenterX = ((rect.right - rect.left) / 2) + rect.left;
            int parentCenterY = ((rect.bottom - rect.top) / 2) + rect.top;
            if (parentCenterY <= halfHeight) {
                yOffset = -(halfHeight - parentCenterY) - parentHeight;
            } else {
                yOffset = (parentCenterY - halfHeight) - parentHeight;
            }
            if (parentCenterX < halfWidth) {
                xOffset = -(halfWidth - parentCenterX);
            }
            if (parentCenterX >= halfWidth) {
                xOffset = parentCenterX - halfWidth;
            }
        }
        toast.setGravity(Gravity.CENTER, xOffset, yOffset);
    }

    public static void setItemVisible(@NonNull Menu menu, int itemId, boolean visible) {
        MenuItem item = menu.findItem(itemId);
        if (item != null) {
            item.setVisible(visible);
        }
    }

    @NonNull
    public static String getAppLabel(@NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
            return packageManager.getApplicationLabel(applicationInfo).toString();
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e, "getApplicationInfo failed");
            return "Lock Screen Remote";
        }
    }

    @NonNull
    public static String getAppVersionString(@NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e, "getApplicationInfo failed");
            return "?";
        }
    }

    public static int safeIcon(@Nullable UiData.Activity activity) {
        // This check tries to survive the case when the icon is not set or not deserialized.
        // Ideally, this should never happen.
        // Life is not ideal.
        //noinspection ConstantConditions
        if (activity == null || activity.icon == null || activity.icon.mResId == 0) {
            FirebaseCrashlytics.getInstance().recordException(new Exception("Icon missing"));
            return R.drawable.btn_unknown;
        }
        return activity.icon.mResId;
    }

    public static int safeIcon(@Nullable UiData.ActionItem item) {
        // This check tries to survive the case when the icon is not set or not deserialized.
        // Ideally, this should never happen.
        // Life is not ideal.
        //noinspection ConstantConditions
        if (item == null || item.icon == null || item.icon.mResId == 0) {
            FirebaseCrashlytics.getInstance().recordException(new Exception("Icon missing"));
            return R.drawable.btn_unknown;
        }
        return item.icon.mResId;
    }

    public static Menu createMenu(Context context) {
        // Trick to get an empty menu.
        //noinspection ConstantConditions
        return new PopupMenu(context, null).getMenu();
    }

    private UiUtil() {
        // Static.
    }

}
