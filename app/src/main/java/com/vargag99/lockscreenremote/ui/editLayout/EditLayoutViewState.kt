package com.vargag99.lockscreenremote.ui.editLayout

/**
 * View state of the EditLayout setCurrentScreen
 */
class EditLayoutViewState(
        val title: String,
        val info: String,
        val buttons: List<EditLayoutButton>)

