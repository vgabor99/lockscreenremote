package com.vargag99.lockscreenremote.analytics

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Analytics.
 */

class Analytics(context: Context) : IAnalytics {
    private val analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(context)
    private val timings: MutableMap<String, Long> = mutableMapOf()

    override fun setCurrentScreen(activity: Activity?, screen: String) {
        if (activity != null) {
            val params = Bundle().apply {
                putString(FirebaseAnalytics.Param.SCREEN_NAME, screen)
            }
            analytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, params)
        }
    }

    override fun event(event: String, context: String) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.LEVEL_NAME, context)
        }
        analytics.logEvent(event, params)
    }

    override fun stats(event: String, value: Long) {
        val params = Bundle().apply {
            putLong(FirebaseAnalytics.Param.VALUE, value)
        }
        analytics.logEvent(event, params)
    }

    override fun startTiming(variable: String) {
        timings[variable] = System.currentTimeMillis()
    }

    override fun endTiming(variable: String) {
        val start = timings.remove(variable)
        if (start != null) {
            val duration = System.currentTimeMillis() - start
            stats(variable, duration)
        }
    }

    override fun missingIcon(type: String, value: String) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.VALUE, value)
        }
        analytics.logEvent(type, params)
    }

    override fun unsupportedHub(fwVersion: String) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.VALUE, fwVersion)
        }
        analytics.logEvent(IAnalytics.UNSUPPORTED_HUB, params)
    }

}
