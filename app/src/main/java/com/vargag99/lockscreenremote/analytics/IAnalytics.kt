package com.vargag99.lockscreenremote.analytics

import android.app.Activity

/**
 * Analytics interface.
 */

interface IAnalytics {

    fun setCurrentScreen(activity: Activity?, screen: String)

    fun event(event: String, context: String)

    fun stats(event: String, value: Long)

    fun startTiming(variable: String)

    fun endTiming(variable: String)

    fun missingIcon(type: String, value: String)

    fun unsupportedHub(fwVersion: String)

    companion object {
        // Notification clicks
        const val NOTIFICATION = "notification"

        // Launcher
        const val LAUNCHER = "launcher"

        // First run
        const val FIRST_RUN = "first_run"
        const val LEFT_SIDE = "left_side"
        const val LEFT_SIDE_TAP = "left_side_tap"
        const val BUTTONS = "buttons"
        const val BUTTONS_TAP = "buttons_tap"
        const val PULL = "pull"
        const val PULL_TAP = "pull_tap"
        const val EXIT_BUTTON = "exit_button"
        const val EXIT_BUTTON_TAP = "exit_button_tap"
        const val SKIP = "skip"

        // Privacy settings
        const val PRIVACY_SETTINGS = "privacy_settings"

        // About
        const val ABOUT = "about"
        const val SEND_EMAIL = "send_email"

        // Disabled notifications
        const val DISABLED_NOTIFICATIONS = "disabled_notifications"
        const val SETTINGS = "settings"
        const val CANCEL = "cancel"

        // It's up there
        const val ITS_UP_THERE = "its_up_there"
        const val OK_DONT_SHOW_AGAIN = "ok_dont_show_again"
        const val OK = "ok"

        // No hub found
        const val NO_HUB_FOUND = "no_hub_found"

        // XMPP disabled
        const val XMPP_DISABLED = "xmpp_disabled"

        // No Wifi
        const val NO_WIFI = "no_wifi"

        // Activity list
        const val ACTIVITY_LIST = "activity_list"
        const val SWITCH_ACTIVITY = "switch_activity"

        // Edit layout
        const val EDIT_LAYOUT = "edit_layout"
        const val PRO_AREA_CLICK = "pro_area_click"

        // Action picker
        const val ACTION_PICKER = "action_picker"
        const val PICK_ACTION = "pick_action"

        // Side nav
        const val SIDE_NAV = "side_nav"
        const val SIDE_NAV_OPEN = "side_nav_open"
        const val SIDE_NAV_HUB_LIST = "side_nav_hub_list"
        const val SIDE_NAV_MENU = "side_nav_menu"
        const val HUB_SELECTED = "hub_selected"
        const val SHARE = "share"
        const val RATE = "rate"

        // Missing icons.
        const val MISSING_ACTIVITY_ICON = "missing_activity_icon"
        const val MISSING_FUNCTION_ICON = "missing_function_icon"

        // Timings
        const val HUB_DISCOVERY_TIME = "hub_discovery_time" // Time to first discovery.
        const val HUB_CONNECT_TIME = "hub_connect_time" // Connect time.
        const val STARTUP_READY_TIME = "startup_ready_time" // Total time from service start to usable hub.
        const val SCREEN_ON_READY_TIME = "screen_on_ready_time" // Total time from setCurrentScreen on to usable hub.
        const val SERVICE_RUNNING_TIME = "service_running_time" // Total time from start exit.

        // Stats
        const val NUM_HUBS = "num_hubs"
        const val NUM_ACTIVITIES = "num_activities"

        // Exit reasons
        const val EXIT = "exit"
        const val POWER_OFF_EXIT = "power_off_exit"
        const val SWIPE_OUT_EXIT = "swipe_out_exit"
        const val INACTIVITY_EXIT = "inactivity_exit"
        const val NO_HUB_EXIT = "no_hub_exit"

        // Unsupported hub
        const val UNSUPPORTED_HUB = "unsupported_hub"
    }

}
