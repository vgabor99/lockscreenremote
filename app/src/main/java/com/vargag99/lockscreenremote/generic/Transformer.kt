package com.vargag99.lockscreenremote.generic

/**
 * Transformer that transforms result between callbacks. Exceptions are handled too.
 *
 * @param <From> The resulting type (type that this object accepts).
 * @param <To>   The source type (type that this object forwards to).
</To></From> */
abstract class Transformer<From, To>(private val callback: Callback<To>?) : Callback<From> {

    abstract fun transform(result: From?): To?

    override fun onResult(result: From?) {
        if (callback != null) {
            try {
                callback.onResult(transform(result))
            } catch (e: Exception) {
                onException(e)
            }

        }
    }

    override fun onException(e: Exception) {
        callback?.onException(e)
    }

}
