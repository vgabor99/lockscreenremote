package com.vargag99.lockscreenremote.generic

import androidx.annotation.Nullable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

/**
 * RxJava 2 does not allow null events, so we have to wrap them.
 */
data class Optional<T>(@Nullable val value: T?) {
    fun isEmpty() = value == null
}

fun <T> Observable<Optional<T>>.notEmpty(): Observable<T> = filter { !it.isEmpty() }.map { it.value!! }
fun <T> Single<Optional<T>>.notEmpty(): Maybe<T> = filter { !it.isEmpty() }.map { it.value!! }