package com.vargag99.lockscreenremote.generic

/**
 * Task result, either success or failure.
 * Get the exception first; if the exception is null then it is a success.
 *
 * @param <T> Task result type.
</T> */

class TaskResult<T> private constructor(val result: T?, val exception: Exception?) {
    companion object {

        fun <T> result(result: T?): TaskResult<T> {
            return TaskResult(result, null)
        }

        fun <T> exception(e: Exception): TaskResult<T> {
            return TaskResult(null, e)
        }

        fun <T> deliver(result: TaskResult<T>?, callback: Callback<T>?) {
            if (result != null && callback != null) {
                if (result.exception != null) {
                    callback.onException(result.exception)
                } else {
                    callback.onResult(result.result)
                }
            }
        }
    }
}
