package com.vargag99.lockscreenremote.generic

/**
 * Protect from obfuscation. Same as @android.support.annotation.Keep, but with RUNTIME retention.
 * FileStore asserts that saved object will not be obfuscated.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CONSTRUCTOR, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.FIELD)
annotation class Keep
