package com.vargag99.lockscreenremote.generic

import io.reactivex.Completable
import io.reactivex.Single

/**
 * Transform Callback to Single
 */
fun <T> single(block: (callback: Callback<T>) -> Unit): Single<Optional<T>> {
    return Single.create { emitter ->
        val callback = object : Callback<T> {
            override fun onResult(result: T?) {
                emitter.onSuccess(Optional(result))
            }

            override fun onException(e: Exception) {
                emitter.onError(e)
            }
        }
        block(callback)
    }
}

/**
 * Transform Callback<Void> to Completable
 */
fun completable(block: (callback: Callback<Void>) -> Unit): Completable {
    return Completable.create { emitter ->
        val callback = object : Callback<Void> {
            override fun onResult(result: Void?) {
                emitter.onComplete()
            }

            override fun onException(e: Exception) {
                emitter.onError(e)
            }
        }
        block(callback)
    }
}
