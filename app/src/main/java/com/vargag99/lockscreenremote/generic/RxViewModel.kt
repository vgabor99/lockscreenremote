package com.vargag99.lockscreenremote.generic

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel base class with rx dispose support.
 */
open class RxViewModel : ViewModel() {
    internal val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.dispose()
    }
}