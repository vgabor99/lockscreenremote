package com.vargag99.lockscreenremote.generic

import android.os.Looper
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import java.io.StringReader
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Utilities.
 */

fun assertUiThread() {
    if (Looper.myLooper() != Looper.getMainLooper()) {
        throw IllegalStateException("Must be called on UI thread")
    }
}

fun String.toProperties(lineSeparator: String, keyValueSeparator: String): Properties {
    val replaced = replace(lineSeparator.toRegex(), "\n").replace(keyValueSeparator.toRegex(), "=")
    return Properties().apply { load(StringReader(replaced)) }
}

fun <T> Observable<T>.bindTo(mutableLiveData: MutableLiveData<T>): Disposable {
    return subscribe { mutableLiveData.value = it }
}

fun <T : Any> Observable<T>.delayEach(interval: Long, timeUnit: TimeUnit): Observable<T> =
    Observables.zip(this, Observable.interval(interval, timeUnit)) { item, _ -> item }
