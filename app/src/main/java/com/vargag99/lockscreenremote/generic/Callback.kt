package com.vargag99.lockscreenremote.generic

/**
 * Callback with exception support.
 */

interface Callback<T> {
    fun onResult(result: T?)

    fun onException(e: Exception)

    /**
     * Runnable that delivers a Callback result.
     *
     * @param <T>
     */
    class Result<T>(private val callback: Callback<T>,
                    private val result: T?) : Runnable {

        override fun run() {
            callback.onResult(result)
        }
    }

    /**
     * Runnable that delivers a Callback error.
     */
    class Error(private val callback: Callback<*>,
                private val exception: Exception) : Runnable {

        override fun run() {
            callback.onException(exception)
        }
    }
}
