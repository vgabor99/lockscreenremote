package com.vargag99.lockscreenremote.generic

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

class CrashlyticsTree : Timber.Tree() {

    override fun isLoggable(priority: Int): Boolean {
        return priority >= Log.INFO
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        FirebaseCrashlytics.getInstance().log("$tag ($priority) $message")
        t?.let { FirebaseCrashlytics.getInstance().recordException(it) }
    }
}
