package com.vargag99.lockscreenremote.generic

import android.app.PendingIntent
import android.os.Build

val FLAG_MUTABLE = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) PendingIntent.FLAG_MUTABLE else 0
val FLAG_IMMUTABLE = PendingIntent.FLAG_IMMUTABLE