package com.vargag99.lockscreenremote.di

import com.vargag99.lockscreenremote.ui.notification.INotificationFactory
import com.vargag99.lockscreenremote.ui.notification.NotificationFactory
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NotificationFactoryModule {

    @Binds
    @Reusable
    abstract fun bindNotificationFactory(notificationFactory: NotificationFactory): INotificationFactory
}