package com.vargag99.lockscreenremote.di

import com.vargag99.lockscreenremote.app.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    internal abstract fun bindUiDataBuilder(uiDataBuilder: UiDataBuilder): IUiDataBuilder

    @Binds
    internal abstract fun bindHubConfigProcessor(hubConfigProcessor: HubDataProcessor): IHubDataProcessor

    @Binds
    internal abstract fun bindMissingIconChecker(missingIconChecker: MissingIconChecker): IMissingIconChecker
}