package com.vargag99.lockscreenremote.di.persistence

import com.vargag99.lockscreenremote.persistence.IPersistence
import com.vargag99.lockscreenremote.persistence.FileStorePersistence
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class PersistenceModule {

    @Binds
    @Reusable
    abstract fun bindsPersistence(persistence: FileStorePersistence): IPersistence
}