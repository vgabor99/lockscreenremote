package com.vargag99.lockscreenremote.di.hubManager

import com.vargag99.lockscreenremote.hubManager.IScreenOnNotifier
import com.vargag99.lockscreenremote.hubManager.IWifiNotifier
import com.vargag99.lockscreenremote.hubManager.ScreenOnNotifier
import com.vargag99.lockscreenremote.hubManager.WifiNotifier
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class HubManagerUtilsModule {
    @Binds
    @Reusable
    internal abstract fun bindWifiNotifier(wifiNotifier: WifiNotifier): IWifiNotifier

    @Binds
    @Reusable
    internal abstract fun bindScreenOnNotifier(screenOnNotifier: ScreenOnNotifier): IScreenOnNotifier
}