package com.vargag99.lockscreenremote.di.persistence

import com.vargag99.lockscreenremote.persistence.IRepo
import com.vargag99.lockscreenremote.persistence.Repo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {

    @Binds
    @Singleton
    abstract fun bindsRepo(repo: Repo): IRepo
}