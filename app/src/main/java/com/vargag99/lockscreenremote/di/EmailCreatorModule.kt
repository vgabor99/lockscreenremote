package com.vargag99.lockscreenremote.di

import com.vargag99.lockscreenremote.app.EmailCreator
import com.vargag99.lockscreenremote.app.IEmailCreator
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Suppress("unused")
@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class EmailCreatorModule {
    @Binds
    abstract fun bindEmailCreator(emailCreator: EmailCreator): IEmailCreator
}