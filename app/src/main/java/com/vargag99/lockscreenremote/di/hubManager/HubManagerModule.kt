package com.vargag99.lockscreenremote.di.hubManager

import com.vargag99.lockscreenremote.app.LifecycleAwareHubManager
import com.vargag99.lockscreenremote.hubManager.HubManager
import com.vargag99.lockscreenremote.hubManager.IHubManager
import com.vargag99.lockscreenremote.hubManager.IManagedHubManager
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * The LifecycleAwareHubManager is a IHubManager proxy that switches between real IHubManagers and a dummy.
 * The source managers are IManagedHubManager-s (to allow graceful close)
 */
@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class HubManagerModule {
    @Binds
    internal abstract fun bindActualHubManager(hubManager: HubManager): IManagedHubManager

    @Binds
    @Singleton
    internal abstract fun bindLifecycleAwareProxy(hubManager: LifecycleAwareHubManager): IHubManager

}
