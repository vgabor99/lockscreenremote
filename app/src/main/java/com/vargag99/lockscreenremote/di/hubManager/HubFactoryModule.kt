package com.vargag99.lockscreenremote.di.hubManager

import com.vargag99.lockscreenremote.hubManager.HubFactory
import com.vargag99.lockscreenremote.hubManager.IHubFactory
import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class HubFactoryModule {
    @Binds
    @Reusable
    internal abstract fun bindHubFactory(hubFactory: HubFactory): IHubFactory
}