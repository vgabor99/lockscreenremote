package com.vargag99.lockscreenremote.di.ui

import androidx.fragment.app.Fragment
import com.vargag99.lockscreenremote.ui.aboutDialog.AboutDialogFragment
import com.vargag99.lockscreenremote.ui.activityList.ActivityListFragment
import com.vargag99.lockscreenremote.ui.editLayout.EditLayoutFragment
import com.vargag99.lockscreenremote.ui.editLayout.actionPicker.ActionPickerDialogFragment
import com.vargag99.lockscreenremote.ui.main.MainFragment
import com.vargag99.lockscreenremote.ui.noHubFound.NoHubFoundFragment
import com.vargag99.lockscreenremote.ui.noWifi.NoWifiFragment
import com.vargag99.lockscreenremote.ui.sideNav.SideNavFragment
import com.vargag99.lockscreenremote.ui.unsupportedHub.UnsupportedHubFragment
import com.vargag99.lockscreenremote.ui.xmppDisabled.XmppDisabledFragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Suppress("unused")
@Module
@InstallIn(FragmentComponent::class)
class FragmentBuildersModule {
    @Provides
    fun bindMainFragment(fragment: Fragment): MainFragment =
        fragment as MainFragment

    @Provides
    fun bindActivityListFragment(fragment: Fragment): ActivityListFragment =
        fragment as ActivityListFragment

    @Provides
    fun bindNoHubFoundFragment(fragment: Fragment): NoHubFoundFragment =
        fragment as NoHubFoundFragment

    @Provides
    fun bindAboutDialogFragment(fragment: Fragment): AboutDialogFragment =
        fragment as AboutDialogFragment

    @Provides
    fun bindSideNavFragment(fragment: Fragment): SideNavFragment =
        fragment as SideNavFragment

    @Provides
    fun bindEditLayoutFragment(fragment: Fragment): EditLayoutFragment =
        fragment as EditLayoutFragment

    @Provides
    fun bindActionPickerDialogFragment(fragment: Fragment): ActionPickerDialogFragment =
        fragment as ActionPickerDialogFragment

    @Provides
    fun bindUnsupportedHubFragment(fragment: Fragment): UnsupportedHubFragment =
        fragment as UnsupportedHubFragment

    @Provides
    fun bindXmppDisabledFragment(fragment: Fragment): XmppDisabledFragment =
        fragment as XmppDisabledFragment

    @Provides
    fun bindNoWifiFragment(fragment: Fragment): NoWifiFragment =
        fragment as NoWifiFragment

}