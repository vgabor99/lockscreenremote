package com.vargag99.lockscreenremote.di.persistence

import android.content.Context
import com.vargag99.lockscreenremote.persistence.FileStore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
class FileStoreModule {
    @Provides
    @Singleton
    internal fun provideFileStore(@ApplicationContext context: Context): FileStore {
        return FileStore(context.filesDir.absolutePath + '/'.toString() + DATA_DIR)
    }

    companion object {
        var DATA_DIR = "data"
    }
}