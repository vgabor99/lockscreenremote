package com.vargag99.lockscreenremote.di.ui

import android.app.Activity
import com.vargag99.lockscreenremote.ui.disabledNotifications.DisabledNotificationsActivity
import com.vargag99.lockscreenremote.ui.firstRun.FirstRunActivity
import com.vargag99.lockscreenremote.ui.itsUpThere.ItsUpThereActivity
import com.vargag99.lockscreenremote.ui.launcher.LauncherActivity
import com.vargag99.lockscreenremote.ui.main.MainActivity
import com.vargag99.lockscreenremote.ui.privacySettings.PrivacySettingsActivity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Suppress("unused")
@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {
    @Provides
    fun provideMainActivity(activity: Activity): MainActivity =
        activity as MainActivity

    @Provides
    fun provideLauncherActivity(activity: Activity): LauncherActivity =
        activity as LauncherActivity

    @Provides
    fun provideDisabledNotificationsActivity(activity: Activity): DisabledNotificationsActivity =
        activity as DisabledNotificationsActivity

    @Provides
    fun provideItsUpThereActivity(activity: Activity): ItsUpThereActivity =
        activity as ItsUpThereActivity

    @Provides
    fun providePrivacySettingsActivity(activity: Activity): PrivacySettingsActivity =
        activity as PrivacySettingsActivity

    @Provides
    fun provideFirstRunActivity(activity: Activity): FirstRunActivity =
        activity as FirstRunActivity

}