package com.vargag99.lockscreenremote.di

import android.content.Context
import com.vargag99.lockscreenremote.app.Preferences
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class PreferencesModule {

    @Provides
    @Reusable
    fun providePreferences(@ApplicationContext context: Context): Preferences = Preferences(context)
}