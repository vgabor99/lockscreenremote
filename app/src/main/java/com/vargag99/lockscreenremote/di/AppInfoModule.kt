package com.vargag99.lockscreenremote.di

import com.vargag99.lockscreenremote.app.AppInfo
import com.vargag99.lockscreenremote.app.IAppInfo
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
abstract class AppInfoModule {
    @Binds
    abstract fun bindAppInfo(appInfo: AppInfo): IAppInfo
}