package com.vargag99.lockscreenremote.di

import android.content.Context
import com.vargag99.lockscreenremote.analytics.Analytics
import com.vargag99.lockscreenremote.analytics.IAnalytics
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
class AnalyticsModule {

    @Provides
    @Reusable
    internal fun provideAnalytics(@ApplicationContext context: Context): IAnalytics = Analytics(context)
}