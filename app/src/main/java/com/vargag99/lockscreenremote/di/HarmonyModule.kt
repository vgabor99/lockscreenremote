package com.vargag99.lockscreenremote.di

import android.net.wifi.WifiManager
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.harmonyApi.HubSessionFactory
import com.vargag99.lockscreenremote.harmonyApi.IHubSessionFactory
import com.vargag99.lockscreenremote.harmonyDiscovery.HubDiscovery
import com.vargag99.lockscreenremote.harmonyDiscovery.IHubDiscovery
import com.vargag99.lockscreenremote.mock.MockHubDiscovery
import com.vargag99.lockscreenremote.mock.MockHubSessionFactory
import com.vargag99.lockscreenremote.mock.MockHubs
import dagger.Lazy
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Suppress("unused")
@Module
@InstallIn(SingletonComponent::class)
class HarmonyModule {

    @Provides
    @Singleton
    internal fun provideHubDiscovery(
        wifiManager: WifiManager,
        analytics: IAnalytics,
        mockHubs: Lazy<MockHubs>
    ): IHubDiscovery {
        return if (BuildConfig.MOCK_FACTORY) MockHubDiscovery(mockHubs.get()) else HubDiscovery(wifiManager, analytics)
    }

    @Provides
    @Singleton
    internal fun provideHubSessionFactory(mockHubs: Lazy<MockHubs>): IHubSessionFactory {
        return if (BuildConfig.MOCK_FACTORY) MockHubSessionFactory(mockHubs.get()) else HubSessionFactory()

    }
}