package com.vargag99.lockscreenremote.harmonyDiscovery

import com.google.gson.Gson
import timber.log.Timber

object HubCompatibilityCheck {

    fun isSupportedHub(hubInfo: HubInfo): Boolean {
        try {
            val protocolVersion = Gson().fromJson(hubInfo.protocolVersion, HubInfo.ProtocolVersion::class.java)
            return protocolVersion.XMPP != null // XMPP supported, fine.
        } catch (e: Exception) {
            Timber.w(e, "isSupportedHub failed")
        }
        return false
    }
}
