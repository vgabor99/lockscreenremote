package com.vargag99.lockscreenremote.harmonyDiscovery

import io.reactivex.Observable

/**
 * Discover Logitech Harmony Hubs on the network.
 */

interface IHubDiscovery {

    fun discovery(): Observable<HubInfo>
}
