package com.vargag99.lockscreenremote.harmonyDiscovery

import android.net.wifi.WifiManager
import com.google.common.net.InetAddresses
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.toProperties
import io.reactivex.Emitter
import io.reactivex.Observable
import timber.log.Timber
import java.io.IOException
import java.io.InputStreamReader
import java.net.*
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Discover Logitech Harmony Hubs on the network by broadcasting a discovery string to port 5224 on
 * the broadcast address. Hubs respond by making a TCP connection back to the IP address that our
 * packet was sent from and on the port we advertised as part of the original discovery request.
 */
class HubDiscovery
@Inject constructor(private val wifiManager: WifiManager, private val analytics: IAnalytics) : IHubDiscovery {

    override fun discovery(): Observable<HubInfo> {
        return Observable.create { emitter ->
            val discovery = Discovery()
            emitter.setCancellable { discovery.stop() }
            discovery.start(emitter)
        }
    }

    private inner class Discovery {

        private var broadcastFuture: ScheduledFuture<*>? = null
        private var serverSocket: ServerSocket? = null
        private var serverPort = 0
        private var server: HarmonyServer? = null
        private var running = false

        private val broadcastAddress: InetAddress
            @Throws(IOException::class)
            get() {
                if (!wifiManager.isWifiEnabled) {
                    throw IOException("Wifi is disabled")
                }
                val dhcp = wifiManager.dhcpInfo ?: throw UnknownHostException()
                val broadcast = dhcp.ipAddress and dhcp.netmask or dhcp.netmask.inv()
                val quads = ByteArray(4)
                for (k in 0..3) {
                    quads[k] = (broadcast shr k * 8 and 0xFF).toByte()
                }
                return InetAddress.getByAddress(quads)
            }

        private val wifiIpAddress: InetAddress
            @Throws(IOException::class)
            get() {
                if (!wifiManager.isWifiEnabled) {
                    throw IOException("Wifi is disabled")
                }
                return InetAddresses.fromInteger(wifiManager.connectionInfo.ipAddress)
            }

        /**
         * Starts discovery for Harmony Hubs
         */
        @Synchronized
        fun start(emitter: Emitter<HubInfo>) {
            try {
                val serverSocket = ServerSocket(serverPort).apply {
                    // Reuse previous port (except first time).
                    reuseAddress = true
                    soTimeout = 10000
                }
                val serverPort = serverSocket.localPort
                Timber.d("Creating Harmony server on port $serverPort")
                val server = HarmonyServer(serverSocket, emitter).apply {
                    start()
                }
                this.serverSocket = serverSocket
                this.serverPort = serverPort
                this.server = server

                broadcastFuture = scheduler.scheduleAtFixedRate({ sendDiscoveryMessage(String.format(Locale.US, DISCO_STRING, serverSocket.localPort)) }, 0, 2, TimeUnit.SECONDS)

                running = true
            } catch (e: IOException) {
                Timber.e(e, "Failed to start Harmony discovery server ")
            }

        }

        /**
         * Stops discovery of Harmony Hubs
         */
        @Synchronized
        fun stop() {
            broadcastFuture?.cancel(true)
            server?.setRunning(false)
            try {
                serverSocket?.close()
            } catch (e: Exception) {
                Timber.e(e, "Failed to stop harmony discovery socket")
            }
            running = false

        }

        /**
         * Send broadcast message
         *
         * @param discoverString String to be used for the discovery
         */
        private fun sendDiscoveryMessage(discoverString: String) {
            try {
                DatagramSocket().apply {
                    broadcast = true
                }.use { bcSend ->
                    val sendData = discoverString.toByteArray()
                    val bcAddress = broadcastAddress
                    val wifiIpAddress = wifiIpAddress
                    val sendPacket = DatagramPacket(sendData, sendData.size, bcAddress, DISCO_PORT)
                    bcSend.send(sendPacket)
                    Timber.d("Request packet sent host=%s", bcAddress.hostAddress)
                }
            } catch (e: Exception) {
                Timber.e(e, "Exception during sendDiscoveryMessage")
            }
        }
    }

    /**
     * Server which accepts TCP connections from Harmony Hubs during the discovery process
     *
     * @author Dan Cunningham - Initial contribution
     */
    private inner class HarmonyServer(private val serverSocket1: ServerSocket, private val emitter: Emitter<HubInfo>) : Thread() {
        private var running1: Boolean = true // Stop signal
        private val responses = mutableSetOf<String>()

        override fun run() {
            while (running1) {
                try {
                    serverSocket1.accept().use { socket ->
                        InputStreamReader(socket.getInputStream()).buffered().forEachLine { input ->
                            if (!running1) {
                                return@forEachLine
                            }
                            Timber.d("READ $input")
                            val result = parseResult(input)
                            if (!responses.contains(result.hostName)) {
                                responses.add(result.hostName)
                                emitter.onNext(result)
                            }
                        }
                    }
                } catch (e: IOException) {
                    if (running1) {
                        Timber.d(e, "Error connecting to hub")
                    }
                }
            }
            analytics.stats(IAnalytics.NUM_HUBS, responses.size.toLong())
            emitter.onComplete()
        }

        fun setRunning(running: Boolean) {
            running1 = running
        }

        @Throws(IOException::class)
        private fun parseResult(input: String): HubInfo {
            // email:gabor.varga.99@gmail.com;mode:3;accountId:8638596;ip:192.168.0.126;port:5222;uuid:ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1;hubId:106;current_fw_version:4.12.36;productId:Pimento;setupSessionType:0;setupSessionClient:MS-131c9010-b732-418c-8f59-e71c7bcb743c;setupSessionIsStale:true;setupSessionSetupType:;setupStatus:0;host_name:Nappali;friendlyName:Nappali;discoveryServerUri:https://svcs.myharmony.com/Discovery/Discovery.svc;openApiVersion:2;minimumOpenApiClientVersionRequired:1;recommendedOpenApiClientVersion:1;protocolVersion:{XMPP="1.0", HTTP="1.0", RF="1.0", WEBSOCKET="1.0"};hubProfiles:{Harmony="2.0"};remoteId:9289336;oohEnabled:true
            return HubInfo(input.toProperties(";", ":"))
        }
    }

    companion object {
        // notice the port appended to the end of the string
        private const val DISCO_STRING = "_logitech-reverse-bonjour._tcp.local.\n%d"
        private const val DISCO_PORT = 5224

        private val scheduler = Executors.newScheduledThreadPool(1)
    }

}
