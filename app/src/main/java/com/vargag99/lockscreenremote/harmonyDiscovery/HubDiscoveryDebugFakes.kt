package com.vargag99.lockscreenremote.harmonyDiscovery

import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.generic.delayEach
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 * Debug functionality: for each discovered real hub, "discover" some fakes too.
 * (Allows developing multi-hub support, without actually purchasing another hub.)
 */
fun Observable<HubInfo>.withDebugFakes(): Observable<HubInfo> {
    if (BuildConfig.DEBUG && BuildConfig.FAKE_HUBS != null) {
        return this
                .flatMap { hubInfo ->
                    // Make some fakes.
                    // Reuse existing data but fake friendlyName to fake and also prefix the uuid.
                    val fakes = BuildConfig.FAKE_HUBS
                            .split(",".toRegex())
                            .dropLastWhile { it.isEmpty() }
                            .toTypedArray()
                            .map { hubInfo.copy(uuid = "${it}-${hubInfo.uuid}", friendlyName = it) }
                    Observable.fromIterable(fakes)
                            .delayEach(1L, TimeUnit.SECONDS)
                            .startWith(hubInfo)
                }
    }
    return this
}
