package com.vargag99.lockscreenremote.harmonyDiscovery

import com.vargag99.lockscreenremote.generic.Keep
import java.util.*

/**
 * Harmony hub discovery result.
 *
 *
 * email:gabor.varga.99@gmail.com;
 * mode:3;
 * accountId:8638596;
 * ip:192.168.0.126;
 * port:5222;
 * uuid:ea38b745-82bb-4cfd-99ec-0c0a8c3a31c1;
 * hubId:106;
 * current_fw_version:4.12.36;
 * productId:Pimento;
 * setupSessionType:0;
 * setupSessionClient:MS-131c9010-b732-418c-8f59-e71c7bcb743c;
 * setupSessionIsStale:true;
 * setupSessionSetupType:;
 * setupStatus:0;
 * host_name:Nappali;
 * friendlyName:Nappali;
 * discoveryServerUri:https: *svcs.myharmony.com/Discovery/Discovery.svc;
 * openApiVersion:2;
 * minimumOpenApiClientVersionRequired:1;
 * recommendedOpenApiClientVersion:1;
 * protocolVersion:{XMPP="1.0", HTTP="1.0", RF="1.0", WEBSOCKET="1.0"};
 * hubProfiles:{Harmony="2.0"};
 * remoteId:9289336;
 * oohEnabled:true
 *
 *
 */
@Keep
data class HubInfo(
        @JvmField val mode: String,
        @JvmField val accountId: String,
        @JvmField val ip: String,
        @JvmField val port: Int,
        @JvmField val uuid: String,
        @JvmField val hubId: String,
        @JvmField val currentFwVersion: String,
        @JvmField val productId: String,
        @JvmField val hostName: String,
        @JvmField val friendlyName: String,
        @JvmField val protocolVersion: String,
        @JvmField val hubProfiles: String
) {

    constructor(props: Properties) : this(
            mode = props.getProperty("mode"),
            accountId = props.getProperty("accountId"),
            ip = props.getProperty("ip"),
            port = Integer.valueOf(props.getProperty("port")),
            uuid = props.getProperty("uuid"),
            hubId = props.getProperty("hubId"),
            currentFwVersion = props.getProperty("current_fw_version"),
            productId = props.getProperty("productId"),
            hostName = props.getProperty("host_name").replace("[^A-Za-z0-9\\-_]".toRegex(), ""),
            friendlyName = props.getProperty("friendlyName"),
            protocolVersion = props.getProperty("protocolVersion"),
            hubProfiles = props.getProperty("hubProfiles")
    )

    @Keep
    data class ProtocolVersion(
            @JvmField var XMPP: String? = null,
            @JvmField var HTTP: String? = null,
            @JvmField var RF: String? = null,
            @JvmField var WEBSOCKET: String? = null
    )
}