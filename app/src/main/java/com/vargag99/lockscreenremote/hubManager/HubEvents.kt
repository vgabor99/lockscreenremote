package com.vargag99.lockscreenremote.hubManager

import io.reactivex.Observable

private enum class DetectTurnOff { INIT, SEEN_ACTIVITY, TURNED_OFF }

/**
 * Emit an stats when the hub is turned off (after it was on).
 */
fun IManagedHarmonyHub.turnedOff(): Observable<Unit> =
        stateSignal()
                .map { it.activityState }
                .scan(DetectTurnOff.INIT) { state, activityState ->
                    when {
                        state == DetectTurnOff.INIT -> {
                            val id = activityState.activity?.id
                            if (id != null && id != IHarmonyHub.POWER_OFF_ACTIVITY_ID) DetectTurnOff.SEEN_ACTIVITY
                            else state
                        }
                        state == DetectTurnOff.SEEN_ACTIVITY && activityState.activityState == IHarmonyHub.ActivityState.NO_ACTIVITY -> DetectTurnOff.TURNED_OFF
                        else -> state
                    }
                }
                .filter { it == DetectTurnOff.TURNED_OFF }
                .map { Unit }

/**
 * Emit an event when the hub has activity (other than being off).
 */
fun IManagedHarmonyHub.hasActivityOtherThanOff(): Observable<Unit> =
        stateSignal()
                .filter {
                    when (it.activityState.activityState) {
                        IHarmonyHub.ActivityState.STARTING,
                        IHarmonyHub.ActivityState.RUNNING,
                        IHarmonyHub.ActivityState.POWERING_OFF -> true
                        else -> false
                    }
                }
                .map { Unit }

/**
 * Emit an event when we the hub activity is known.
 * (That is the point when the hub starts to be useful from user standpoint).
 */
fun IManagedHarmonyHub.hasAnyActivity(): Observable<Unit> =
        stateSignal()
                .filter { it.activityState.activityState != IHarmonyHub.ActivityState.UNKNOWN && it.activityState.activity != null }
                .map { Unit }
