package com.vargag99.lockscreenremote.hubManager

import com.google.common.base.Objects
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Observable

/**
 * Harmony hub interface.
 */

interface IHarmonyHub {

    val info: HubInfo

    enum class ActivityState {
        UNKNOWN, // We get this when we don't know the state yet (connecting).
        NO_ACTIVITY, // Off (i.e. RUNNING state for activity PowerOff)
        STARTING, // Starting activity (other than PowerOff)
        RUNNING, // Running activity (other than PowerOff)
        POWERING_OFF // Powering off (i.e. STARTING state for PowerOff)
    }

    enum class HubState {
        NULL,
        CONNECTING,
        CONNECTED,
        ERROR
    }

    enum class HubError {
        NONE,
        NO_XMPP_API,
        XMPP_CONNECTION_REFUSED,
        UNKNOWN,
    }

    data class ActivitySstate(
            val activity: Data.Activity?,
            val activityState: IHarmonyHub.ActivityState
    ) {
        fun sameState(other: ActivitySstate): Boolean {
            // This is NOT equals - we only compare the activity ID and the state.
            return other.activityState == this.activityState && Objects.equal(other.activity?.id, this.activity?.id)
        }
    }

    data class State(
            val hubState: HubState,
            val hubError: HubError,
            val activityState: ActivitySstate
    )

    fun getState(): State

    fun stateSignal(): Observable<State>

    fun sendButtonPress(action: String)

    fun startActivity(activityId: String)

    fun executeAction(action: HubAction) {
        when (action) {
            is HubAction.StartActivity -> startActivity(action.activityId)
            is HubAction.ButtonPress -> sendButtonPress(action.action)
        }
    }

    companion object {

        const val POWER_OFF_ACTIVITY_ID = "-1"
    }

}
