package com.vargag99.lockscreenremote.hubManager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.annotation.NonNull
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Watch and notify setCurrentScreen coming on.
 */
class ScreenOnNotifier @Inject constructor(
    @param:NonNull @ApplicationContext private val context: Context
    ) : IScreenOnNotifier {
    override fun screenOn(): Observable<Unit> {
        return Observable.create { emitter ->
            val broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    emitter.onNext(Unit)
                }
            }
            context.registerReceiver(broadcastReceiver, IntentFilter(Intent.ACTION_SCREEN_ON))
            emitter.setCancellable { context.unregisterReceiver(broadcastReceiver) }
        }
    }

}
