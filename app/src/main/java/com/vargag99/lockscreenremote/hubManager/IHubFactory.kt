package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

/**
 * Hub factory.
 */

interface IHubFactory {

    fun createHub(hubInfo: HubInfo): IManagedHarmonyHub
}
