package com.vargag99.lockscreenremote.hubManager

/**
 * Internal API, allows closing the manager.
 */
interface IManagedHubManager : IHubManager {
    fun close()
}