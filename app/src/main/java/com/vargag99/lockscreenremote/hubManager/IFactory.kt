package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.harmonyDiscovery.IHubDiscovery

/**
 * Factory for our objects.
 */

interface IFactory {

    fun createWifiNotifier(): IWifiNotifier

    fun createHubDiscovery(): IHubDiscovery

    fun createHub(hubInfo: HubInfo): IManagedHarmonyHub
}
