package com.vargag99.lockscreenremote.hubManager

import io.reactivex.Observable

/**
 * Screen on notifier.
 */
interface IScreenOnNotifier {
    fun screenOn(): Observable<Unit>
}