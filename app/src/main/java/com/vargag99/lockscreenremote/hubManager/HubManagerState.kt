package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

data class HubManagerState(
        val error: IHubManager.HubManagerError?,
        val hubInfo: HubInfo?,
        val harmonyHub: IHarmonyHub?
)