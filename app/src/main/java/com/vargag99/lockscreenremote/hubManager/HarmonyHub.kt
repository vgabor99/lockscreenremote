package com.vargag99.lockscreenremote.hubManager

import androidx.annotation.NonNull
import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.harmonyApi.ConnectionEvent
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyApi.IHubSession
import com.vargag99.lockscreenremote.harmonyApi.IHubSessionFactory
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * Harmony hub client.
 * Maintains the connection, retrieves hub data and allows sending commands.
 */
class HarmonyHub(override val info: HubInfo,
                 private val analytics: IAnalytics,
                 hubSessionFactory: IHubSessionFactory) : IManagedHarmonyHub {
    private val hubSession: IHubSession = hubSessionFactory.createHubSession(info.ip, info.port)
    private var authToken: String? = null
    private var hubState: IHarmonyHub.HubState = IHarmonyHub.HubState.NULL
    private var hubError: IHarmonyHub.HubError = IHarmonyHub.HubError.NONE
    private var configVar: Data.Config? by Delegates.observable<Data.Config?>(null) { _, _, new ->
        new?.let { configSubject.onNext(it) }
    }
    private val configSubject: PublishSubject<Data.Config> = PublishSubject.create()
    private var activityState: IHarmonyHub.ActivitySstate = IHarmonyHub.ActivitySstate(null, IHarmonyHub.ActivityState.UNKNOWN)
    private var disposables = CompositeDisposable() // Disposables to be discarded with one connect run.
    private var reconnect: Disposable? = null

    private fun setupEventHandling() {
        hubSession.connectionEvents()
                .subscribeBy { event ->
                    when (event) {
                        is ConnectionEvent.ConnectionClosedOnError -> handleError(event.exception)
                        else -> Unit
                    }
                }
                .addTo(disposables)
        hubSession.hubEvents()
                .subscribeBy { event ->
                    val data = event.data
                    when (data) {
                        is Data.StateDigest -> {
                            Timber.d("StateDigest: $data")
                            when (data.activityStatus) {
                                // 0--No activity, 1--Activity starting, 2--Activity running, 3--Powering off
                                Data.AS_NO_ACTIVITY -> IHarmonyHub.ActivityState.NO_ACTIVITY
                                Data.AS_ACTIVITY_STARTING -> IHarmonyHub.ActivityState.STARTING
                                Data.AS_ACTIVITY_RUNNING -> null // Better get this from ActivityStarted stats, comes earlier and we get only one.
                                Data.AS_POWERING_OFF -> IHarmonyHub.ActivityState.POWERING_OFF
                                else -> null
                            }?.let { newState ->
                                setActivity(data.activityId, newState)
                                stateChanged()
                            }
                        }
                        is Data.ActivityStarted -> {
                            Timber.d("ActivityStarted: $data")
                            setActivity(data.activityId, IHarmonyHub.ActivityState.RUNNING)
                            stateChanged()
                        }
                    }
                }
                .addTo(disposables)
    }

    override fun connect(authToken: String?) {
        Timber.i("connect")
        analytics.startTiming(IAnalytics.HUB_CONNECT_TIME)
        this.authToken = authToken
        doDisconnect()
        disposables = CompositeDisposable() // Disposables for this connect run.
        hubState = IHarmonyHub.HubState.CONNECTING
        setupEventHandling()
        connectSession(authToken)
                .andThen(fetchConfig()) // Get config first, this completes being connected.
                .andThen(keepalive())// Kick off keepalive: this starts with getting the activity.
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = {
                    doDisconnect()
                    handleError(it)
                })
                .addTo(disposables)
        stateChanged()
    }

    override fun disconnect() {
        Timber.i("disconnect")
        doDisconnect()
        stateChanged()
    }

    private fun doDisconnect() {
        Timber.i("doDisconnect")
        reconnect?.dispose()
        disposables.dispose()
        hubState = IHarmonyHub.HubState.NULL
        setActivity(null, IHarmonyHub.ActivityState.UNKNOWN)
        hubSession.disconnect()
    }

    private fun reconnect(delaySeconds: Long) {
        Timber.d("reconnect delaySeconds=$delaySeconds")
        reconnect?.dispose()
        reconnect = Observable.timer(delaySeconds, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { connect(this.authToken) }
    }

    private fun handleError(t: Throwable) {
        hubError = t.toHubError()
        reconnect(ERROR_RECONNECT_WAIT_SECONDS)
        stateChanged()
    }

    override fun checkConnection() {
        Timber.d("checkConnection")
        // This is called when the screen comes on, make sure we are still connected.
        // Do an immediate keepalive.
        keepaliveTrigger.onNext(Unit)
    }

    override fun getConfig(): Observable<Data.Config> {
        return configSubject
    }

    private val keepaliveTrigger: PublishSubject<Unit> = PublishSubject.create() // Trigger immediate keepalive

    private fun keepalive(): Observable<Unit> {
        // Keepalive logic: do keepalive on trigger immediately, then follow-up in pre-set intervals.
        return keepaliveTrigger // Triggered
                .startWith(Unit) // Initial
                .switchMap { Observable.interval(0, KEEPALIVE_SECONDS, TimeUnit.SECONDS) }
                .doOnNext { Timber.d("keepalive") }
                .switchMap { getCurrentActivity().toObservable<Unit>() }
    }

    private fun getCurrentActivity(): Completable {
        return hubSession.getCurrentActivity()
                .notEmpty()
                .doOnSuccess { result ->
                    if (IHarmonyHub.ActivityState.UNKNOWN === activityState.activityState) {
                        // The keepalive ony reports the currently running activity.
                        // We only set it as current if we have no better information.
                        // Don't let the keepalive "revert" during an ongoing activity switch.
                        if (result == IHarmonyHub.POWER_OFF_ACTIVITY_ID) {
                            // Why do we have different states for the PowerOff activity?
                            setActivity(result, IHarmonyHub.ActivityState.NO_ACTIVITY)
                        } else {
                            setActivity(result, IHarmonyHub.ActivityState.RUNNING)
                        }
                        stateChanged()
                    }
                }
                .doOnError { Timber.e(it, "getCurrentActivity failed") }
                .ignoreElement() // Stored into state already.
    }

    private val stateVar = BehaviorSubject.createDefault(getState())

    override fun getState(): IHarmonyHub.State {
        return IHarmonyHub.State(
                hubState = hubState,
                hubError = hubError,
                activityState = activityState
        )
    }

    private fun stateChanged() {
        stateVar.onNext(getState())
    }

    override fun stateSignal(): Observable<IHarmonyHub.State> = stateVar

    override fun sendButtonPress(action: String) {
        val now = System.currentTimeMillis()
        hubSession.sendButtonPress(now, action)
    }

    override fun startActivity(activityId: String) {
        hubSession.startActivity(activityId)
    }

    private fun setActivity(activityId: String?, @NonNull activityState: IHarmonyHub.ActivityState) {
        val currentActivity = findActivity(activityId)
        val newState = IHarmonyHub.ActivitySstate(currentActivity, activityState)
        if (!newState.sameState(this.activityState)) {
            this.activityState = newState
        }
    }

    private fun findActivity(activityId: String?): Data.Activity? {
        return configVar?.activity?.find { activityId == it.id }
    }

    private fun connectSession(authToken: String?): Completable {
        return hubSession.connect(authToken)
                .doOnComplete {
                    Timber.d("Login OK")
                    // Connected but not yet usable, don't set CONNECTED yet.
                    // fetchConfig() will do it.
                }
                .doOnError {
                    Timber.e(it, "Login failed")
                }
    }

    private fun fetchConfig(): Completable {
        return hubSession.getConfig()
                .notEmpty()
                .doOnError {
                    Timber.d(it, "GetConfig failed")
                }
                .doOnSuccess { result ->
                    Timber.d("GetConfig OK")
                    analytics.endTiming(IAnalytics.HUB_CONNECT_TIME)
                    configVar = result
                    hubState = IHarmonyHub.HubState.CONNECTED
                    hubError = IHarmonyHub.HubError.NONE
                }
                .ignoreElement()
    }

    companion object {
        private const val KEEPALIVE_SECONDS = 10L
        private const val ERROR_RECONNECT_WAIT_SECONDS = 2L

    }
}
