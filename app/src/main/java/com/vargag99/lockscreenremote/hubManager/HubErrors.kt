package com.vargag99.lockscreenremote.hubManager

import org.jivesoftware.smack.XMPPException

/**
 * Classify hub errors.
 */
fun Throwable?.toHubError(): IHarmonyHub.HubError =
        when {
            this == null -> IHarmonyHub.HubError.NONE
            this is XMPPException && xmppError?.code == 502 && toString().contains("ECONNREFUSED") -> IHarmonyHub.HubError.XMPP_CONNECTION_REFUSED
            else -> IHarmonyHub.HubError.UNKNOWN
        }