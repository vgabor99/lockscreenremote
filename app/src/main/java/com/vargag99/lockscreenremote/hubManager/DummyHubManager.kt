package com.vargag99.lockscreenremote.hubManager

import io.reactivex.Observable

class DummyHubManager : IManagedHubManager {

    override fun getState(): Observable<HubManagerState> = Observable.never()

    override fun requestExitSignal(): Observable<Unit> = Observable.never()

    override fun executeAction(action: HubAction) = Unit

    override fun close() = Unit
}