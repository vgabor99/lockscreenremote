package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.app.IHubDataProcessor
import com.vargag99.lockscreenremote.app.Preferences
import com.vargag99.lockscreenremote.app.rx
import com.vargag99.lockscreenremote.generic.notEmpty
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.harmonyDiscovery.IHubDiscovery
import com.vargag99.lockscreenremote.harmonyDiscovery.withDebugFakes
import com.vargag99.lockscreenremote.persistence.IRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Application business logic.
 * Maintains the connection to the hub.
 * It also provides the ui state (that is, content of the remote the application shows).
 */
class HubManager @Inject constructor(
        wifiNotifier: IWifiNotifier,
        screenOnNotifier: IScreenOnNotifier,
        private val discovery: IHubDiscovery,
        private val hubFactory: IHubFactory,
        private val hubDataProcessor: IHubDataProcessor,
        private val preferences: Preferences,
        private val repo: IRepo,
        private val analytics: IAnalytics
) : IManagedHubManager {
    private val initDisposables = CompositeDisposable()
    private var startDisposables = CompositeDisposable()
    private var wifiSignal: Disposable? = null
    private var error: IHubManager.HubManagerError? = null
    private var screenOnSignal: Disposable? = null
    private var discoverySignal: Disposable? = null
    private var inactivityExit: Disposable? = null
    private val selectedHubInfo: BehaviorSubject<HubInfo> = BehaviorSubject.create()
    private var hubConnector: HubConnector? = null
    private var managerState = BehaviorSubject.create<HubManagerState>()
    private val requestExitSubject: PublishSubject<Unit> = PublishSubject.create()

    private fun hubDiscoveryFinished() {
        Timber.d("hubDiscoveryFinished")
        if (hubConnector == null) {
            // No hub found after full 20 seconds discovery. Give up and exit.
            analytics.event(IAnalytics.NO_HUB_EXIT, IAnalytics.EXIT)
            error = IHubManager.HubManagerError.NoHubFound
            stateChanged()
            requestExit()
        }
    }

    private fun hubDiscovered(result: HubInfo) {
        Timber.i("hubDiscovered hostName=%s friendlyName=%s", result.hostName, result.friendlyName)
        analytics.endTiming(IAnalytics.HUB_DISCOVERY_TIME)
        repo.updateHubInfo(result)
        if (preferences.hubId == null) {
            // Nothing set in preferences, store it.
            preferences.hubId = result.uuid
        }
    }

    private fun connectHub(hubInfo: HubInfo) {
        with(hubConnector) {
            if (this == null || hub.info.uuid != hubInfo.uuid) {
                doDisconnectHub()
                doConnectHub(hubInfo)
                stateChanged()
            }
        }
    }

    override fun getState(): Observable<HubManagerState> {
        return managerState
    }

    override fun requestExitSignal(): Observable<Unit> {
        return requestExitSubject
    }

    private fun stateChanged() {
        Timber.d("stateChanged")
        managerState.onNext(HubManagerState(
                error,
                selectedHubInfo.value,
                hubConnector?.hub)
        )
    }

    private fun requestExit() {
        Timber.d("requestExit")
        requestExitSubject.onNext(Unit)
    }

    override fun executeAction(action: HubAction) {
        Timber.d("executeAction action=$action")
        hubConnector?.hub?.executeAction(action)
    }

    init {
        Timber.i("init")
        // Start watching wifi, when we get it, kick off.
        wifiSignal = wifiNotifier.connectedWifi()
                .distinctUntilChanged()
                .subscribe { connectedWifi ->
                    Timber.d("connectedWifi=${connectedWifi}")
                    if (connectedWifi) {
                        error = null
                        start()
                    } else {
                        error = IHubManager.HubManagerError.NoWifi
                        stop()
                    }
                    stateChanged()
                }
                .addTo(initDisposables)
        // Screen on event triggers connection check.
        screenOnSignal = screenOnNotifier.screenOn()
                .subscribe {
                    Timber.d("onScreenOn")
                    hubConnector?.let {
                        analytics.startTiming(IAnalytics.SCREEN_ON_READY_TIME)
                        it.hub.checkConnection()
                    }
                }
                .addTo(initDisposables)
        // Schedule inactivity exit. If we don't see any activity running in 5 minutes, exit.
        // First activity change that is not OFF cancels the inactivity exit.
        inactivityExit = Observable.timer(INACTIVITY_EXIT_SECONDS, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe {
                    Timber.d("Inactivity exit")
                    analytics.event(IAnalytics.INACTIVITY_EXIT, IAnalytics.EXIT)
                    requestExit()
                }
                .addTo(initDisposables)
        preferences.rx().hubId
                .notEmpty()
                .switchMap { hubId -> repo.getHubInfo(hubId) }
                .notEmpty()
                .distinctUntilChanged() // Avoid unnecessary reconnect
                .subscribe(selectedHubInfo)
        analytics.startTiming(IAnalytics.SERVICE_RUNNING_TIME)
    }

    override fun close() {
        Timber.i("close")
        initDisposables.dispose()
        stop()
        analytics.endTiming(IAnalytics.SERVICE_RUNNING_TIME)
    }

    private fun start() {
        Timber.i("start")
        // This is where we start operation, when we have wifi.
        startDisposables = CompositeDisposable()
        analytics.startTiming(IAnalytics.HUB_DISCOVERY_TIME)
        analytics.startTiming(IAnalytics.STARTUP_READY_TIME)
        discoverySignal = discovery.discovery()
                .withDebugFakes()
                .take(DISCOVERY_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { hubDiscovered(it) },
                        onComplete = { hubDiscoveryFinished() },
                        onError = { Timber.e(it, "Discovery failed") }
                )
                .addTo(startDisposables)
        selectedHubInfo.subscribe { connectHub(it) }
                .addTo(startDisposables)
    }

    private fun stop() {
        Timber.i("stop")
        // This is where we stop (shutdown, or wifi is lost).
        startDisposables.dispose()
        doDisconnectHub()
    }

    private fun doConnectHub(hubInfo: HubInfo) {
        if (hubConnector != null) {
            throw IllegalStateException("Already connected")
        }
        val hub = hubFactory.createHub(hubInfo)
        hubConnector = HubConnector(hub)
        stateChanged()
    }

    private fun doDisconnectHub() {
        hubConnector?.dispose()
        hubConnector = null
    }

    /**
     * Hub connector, connects the hub and reacts to its signals.
     */
    private inner class HubConnector(val hub: IManagedHarmonyHub) {
        private val hubDisposables = CompositeDisposable()

        init {
            hub.stateSignal()
                    .subscribeBy {
                        Timber.d("hubStateSignal state=$it")
                        // Update notification. TODO do we need this here?
                        stateChanged()
                    }
                    .addTo(hubDisposables)
            hub.turnedOff()
                    .take(1)
                    .subscribeBy {
                        Timber.d("hub turned off")
                        // The user turned off the hub just now. Exit.
                        analytics.event(IAnalytics.POWER_OFF_EXIT, IAnalytics.EXIT)
                        requestExit()
                    }
                    .addTo(hubDisposables)
            hub.hasActivityOtherThanOff()
                    .take(1)
                    .subscribeBy {
                        Timber.d("hub has activity")
                        // OK we have seen an activity, don't exit (until the hub is turned off).
                        inactivityExit?.dispose()
                    }
                    .addTo(hubDisposables)
            hub.hasAnyActivity()
                    .take(1)
                    .subscribeBy {
                        Timber.d("hub is usable")
                        // This is the point when we have a usable remote. Stop the timing clock if ticking.
                        analytics.endTiming(IAnalytics.SCREEN_ON_READY_TIME)
                        analytics.endTiming(IAnalytics.STARTUP_READY_TIME)
                    }
                    .addTo(hubDisposables)
            hub.getConfig()
                    .subscribeBy { config ->
                        hubDataProcessor.processHubConfig(hub.info, config)
                    }
                    .addTo(hubDisposables)
            hub.connect(null)
        }

        fun dispose() {
            hubDisposables.dispose()
            hub.disconnect()
        }

    }

    companion object {
        private const val DISCOVERY_TIMEOUT_SECONDS = 20L // We try 20 seconds then give up.
        private const val INACTIVITY_EXIT_SECONDS = 5 * 60L // If we see no activity in 5 minutes, exit.
    }

}
