package com.vargag99.lockscreenremote.hubManager

sealed class HubAction {
    data class StartActivity(val activityId: String) : HubAction()
    data class ButtonPress(val action: String) : HubAction()
}