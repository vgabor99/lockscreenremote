package com.vargag99.lockscreenremote.hubManager

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject


/**
 * Wifi notifier.
 */

class WifiNotifier @Inject constructor(
    private val connectivityManager: ConnectivityManager
) : IWifiNotifier {

    override fun connectedWifi(): Observable<Boolean> {
        val changes: Observable<Boolean> = Observable.create { emitter ->
            val networkCallback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    emitter.onNext(true)
                }

                override fun onUnavailable() {
                    super.onUnavailable()
                    emitter.onNext(false)
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    emitter.onNext(false)
                }
            }
            val networkRequest = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build()
            connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
            emitter.setCancellable { connectivityManager.unregisterNetworkCallback(networkCallback) }
        }
        return changes
            .startWith(isConnected())
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun isConnected(): Boolean {
        return connectivityManager.activeNetworkInfo?.run {
            type == ConnectivityManager.TYPE_WIFI && isConnected
        } ?: false
    }
}
