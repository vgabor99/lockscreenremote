package com.vargag99.lockscreenremote.hubManager

import io.reactivex.Observable

/**
 * Hub Manager interface.
 * The HubManager contains most of the business logic in the app.
 * - It keeps the user-selected hub connected
 * - Watches for events that require (re)-connecting (e.g. wifi, screen on/off)
 * - Processes and persists hub-related data (config, UiData)
 * - Provides access to the connected hub (for sending commands)
 */

interface IHubManager {

    sealed class HubManagerError {
        object NoWifi : HubManagerError() // Can't connect to wifi
        object NoHubFound : HubManagerError() // No hubs found on this network
    }

    /**
     * Get state.
     *
     * @return UI state
     */
    fun getState(): Observable<HubManagerState>

    /**
     * Request exit signal.
     *
     * @return Request exit signal.
     */
    fun requestExitSignal(): Observable<Unit>

    /**
     * Handle action (hub action command).
     *
     * @param action Action.
     */
    fun executeAction(action: HubAction)

}
