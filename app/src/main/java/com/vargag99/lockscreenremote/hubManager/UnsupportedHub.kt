package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Observable

/**
 * Unsupported hub - has HubInfo but otherwise it is dead.
 */
class UnsupportedHub(override val info: HubInfo, private val hubError: IHarmonyHub.HubError) : IManagedHarmonyHub {

    override fun getState() = IHarmonyHub.State(
            IHarmonyHub.HubState.ERROR,
            hubError,
            IHarmonyHub.ActivitySstate(null, IHarmonyHub.ActivityState.UNKNOWN)
    )

    override fun stateSignal() = Observable.just(getState())

    override fun connect(authToken: String?) {

    }

    override fun disconnect() {

    }

    override fun checkConnection() {

    }

    override fun getConfig(): Observable<Data.Config> {
        return Observable.never()
    }

    override fun sendButtonPress(action: String) {

    }

    override fun startActivity(activityId: String) {

    }
}
