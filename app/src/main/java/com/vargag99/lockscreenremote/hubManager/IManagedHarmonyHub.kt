package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.harmonyApi.Data
import io.reactivex.Observable

/**
 * Internal API, used by the service to manage the hub.
 */

interface IManagedHarmonyHub : IHarmonyHub {

    fun connect(authToken: String?)

    fun disconnect()

    fun checkConnection()

    fun getConfig(): Observable<Data.Config>
}
