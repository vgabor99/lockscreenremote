package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.generic.Optional
import io.reactivex.Observable

/**
 * Watch and notify connected wifi network.
 */

interface IWifiNotifier {

    fun connectedWifi(): Observable<Boolean>
}
