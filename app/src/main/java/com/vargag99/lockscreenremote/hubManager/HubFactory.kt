package com.vargag99.lockscreenremote.hubManager

import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.harmonyApi.IHubSessionFactory
import com.vargag99.lockscreenremote.harmonyDiscovery.HubCompatibilityCheck
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import timber.log.Timber
import javax.inject.Inject

class HubFactory @Inject constructor(
        private val analytics: IAnalytics,
        private val hubSessionFactory: IHubSessionFactory) : IHubFactory {

    override fun createHub(hubInfo: HubInfo): IManagedHarmonyHub {
        return if (HubCompatibilityCheck.isSupportedHub(hubInfo)) {
            HarmonyHub(hubInfo, analytics, hubSessionFactory)
        } else {
            Timber.i("Unsupported hub: %s", hubInfo.currentFwVersion)
            analytics.unsupportedHub(hubInfo.currentFwVersion)
            UnsupportedHub(hubInfo, IHarmonyHub.HubError.NO_XMPP_API)
        }
    }

}
