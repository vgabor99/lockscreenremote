package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.hubManager.IHarmonyHub
import com.vargag99.lockscreenremote.ui.UiUtil

/**
 * Create default layouts for activities.
 */

object Layouts {

    fun createDefault(config: Data.Config, activity: Data.Activity): Layout {
        return createEmpty().apply {
            when (activity.type) {
                "PowerOff" -> {
                    // List all non-poweroff activities, add the first three.
                    config.activity
                            .filter { IHarmonyHub.POWER_OFF_ACTIVITY_ID != it.id }
                            .sortedWith(UiUtil.activityComparator)
                            .take(3)
                            .forEachIndexed { index, activity ->
                                mActions[index + 2] = Actions.startActivityAction(activity)
                            }
                }
                // TODO more.
                else -> {
                    mActions[2] = "Volume/VolumeDown"
                    mActions[3] = "Volume/Mute"
                    mActions[4] = "Volume/VolumeUp"
                }
            }
        }
    }

    fun createEmpty(): Layout {
        return Layout(mActions = arrayOfNulls(25))
    }
}
