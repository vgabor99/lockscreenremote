package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

/**
 * Prepare UI data from hub config.
 */
interface IUiDataBuilder {
    fun build(
            hubInfo: HubInfo,
            config: Data.Config): List<UiData.ActivityConfig>
}