package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.persistence.IRepo
import javax.inject.Inject

internal class HubDataProcessor @Inject constructor(
        private val repo: IRepo,
        private val uiDataBuilder: IUiDataBuilder,
        private val missingIconChecker: MissingIconChecker,
        private val analytics: IAnalytics) : IHubDataProcessor {

    override fun processHubConfig(hubInfo: HubInfo, config: Data.Config) {
        // Update config.
        repo.updateConfig(hubInfo.uuid, config)
        // Create UI data.
        // This is a UI task and not at all persistence, but this is absolutely the
        // best place to do this :P
        // Create Activity configs.
        uiDataBuilder.rx().build(hubInfo, config)
                .map {
                    it.forEach { activityConfig ->
                        repo.updateActivityConfig(activityConfig.hub.uuid, activityConfig.activity.id, activityConfig)
                    }
                }
                .subscribe()
        // Check and create default layouts.
        config.activity.forEach { activity ->
            repo.existsLayout(hubInfo.uuid, activity.id)
                    .map { exists ->
                        if (!exists) {
                            repo.updateLayout(hubInfo.uuid, activity.id, Layouts.createDefault(config, activity))
                        }
                    }
                    .subscribe()
        }
        // Do some analytics.
        missingIconChecker.check(config)
        analytics.stats(IAnalytics.NUM_ACTIVITIES, config.activity.size.toLong())
    }
}