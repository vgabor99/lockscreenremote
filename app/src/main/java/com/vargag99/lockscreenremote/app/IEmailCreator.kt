package com.vargag99.lockscreenremote.app

import android.content.Intent
import io.reactivex.Single
import java.io.File

/**
 * Prepare the Intent that is used to send email.
 */
interface IEmailCreator {
    fun prepareDiagnosticInfo(): Single<File>
    fun createEmailIntent(attachment: File?): Intent
}