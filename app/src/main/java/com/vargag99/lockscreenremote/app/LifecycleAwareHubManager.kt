package com.vargag99.lockscreenremote.app

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.vargag99.lockscreenremote.hubManager.DummyHubManager
import com.vargag99.lockscreenremote.hubManager.HubAction
import com.vargag99.lockscreenremote.hubManager.HubManagerState
import com.vargag99.lockscreenremote.hubManager.IHubManager
import com.vargag99.lockscreenremote.hubManager.IManagedHubManager
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

/**
 * Lifecycle-aware HubManager proxy.
 * On lifecycle START, creates a real HubManager.
 * On lifecycle STOP, gracefully stops the real manager and switches to a dummy.
 */
@Singleton
class LifecycleAwareHubManager @Inject constructor(private val provider: Provider<IManagedHubManager>) :
    IHubManager, DefaultLifecycleObserver {
    private val started = PublishSubject.create<Boolean>()
    private val delegate = BehaviorSubject.create<IManagedHubManager>()

    init {
        val cur = started
            .distinctUntilChanged()
            .switchMap { started ->
                // Create a new hub (real or dummy) as needed.
                // The delegate will pull and cache the latest one. The "using" operator closes unreferenced old managers.
                fun resourceSupplier() = if (started) provider.get() else dummyDelegate
                fun observableSupplier(hubManager: IManagedHubManager) =
                    Observable.just(hubManager).concatWith(Observable.never())

                fun disposer(hubManager: IManagedHubManager) = hubManager.close()
                Observable.using(::resourceSupplier, ::observableSupplier, ::disposer)
            }
        cur.subscribe(delegate)
        AppLifecycleOwner.registry.addObserver(this)
    }

    override fun getState(): Observable<HubManagerState> =
        delegate.switchMap { it.getState().concatWith(Observable.never()) }

    override fun requestExitSignal(): Observable<Unit> =
        delegate.switchMap { it.requestExitSignal().concatWith(Observable.never()) }

    override fun executeAction(action: HubAction) {
        delegate.value?.executeAction(action)
    }

    @Suppress("unused")
    override fun onStart(owner: LifecycleOwner) {
        Timber.d("onStart")
        started.onNext(true)
    }

    @Suppress("unused")
    override fun onStop(owner: LifecycleOwner) {
        Timber.d("onStop")
        started.onNext(false)
    }

    companion object {
        private val dummyDelegate = DummyHubManager()
    }
}