package com.vargag99.lockscreenremote.app

import android.content.SharedPreferences
import com.vargag99.lockscreenremote.generic.Optional
import io.reactivex.Observable

/**
 * Reactive extensions for Preferences
 */
class PreferencesRx(private val preferences: Preferences) {
    val hubId = rx(Preferences.HUB_ID) { Optional(preferences.hubId) }
    val analyticsEnabled = rx(Preferences.ANALYTICS_ENABLED) { preferences.analyticsEnabled }
    val crashlyticsEnabled = rx(Preferences.CRASHLYTICS_ENABLED) { preferences.crashlyticsEnabled }
    // TODO more, if needed

    private fun <T : Any> rx(key: String, getter: () -> T): Observable<T> = Observable.create { emitter ->
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, changedKey ->
            if (key == changedKey) {
                emitter.onNext(getter())
            }
        }
        preferences.preferences.registerOnSharedPreferenceChangeListener(listener)
        emitter.setCancellable { preferences.preferences.unregisterOnSharedPreferenceChangeListener(listener) }
        emitter.onNext(getter())
    }
}

fun Preferences.rx() = PreferencesRx(this)