package com.vargag99.lockscreenremote.app

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.FileProvider
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.persistence.IRepo
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

class EmailCreator @Inject constructor(
    @ApplicationContext private val context: Context,
    private val repo: IRepo,
    private val appInfo: IAppInfo) : IEmailCreator {
    override fun prepareDiagnosticInfo(): Single<File> {
        // Update diagnostic info.
        return repo.updateDiagnosticInfo(appInfo.diagnosticInfo)
                .andThen(Single.fromCallable {
                    // Generate filename and ensure that the file can be created.
                    File(context.cacheDir, "/shared/diagnostics.zip").also {
                        it.parentFile?.mkdirs()
                    }
                })
                .flatMap {
                    // Zip all files. We always use the same target name so don't bother to clean up.
                    repo.zip(it.toString()).andThen(Single.just(it))
                }
                .subscribeOn(Schedulers.io())
    }

    override fun createEmailIntent(attachment: File?): Intent {
        return Intent(Intent.ACTION_SEND).apply {
            type = "message/rfc822"
            putExtra(Intent.EXTRA_EMAIL, arrayOf("lockscreenremote@gmail.com"))
            putExtra(Intent.EXTRA_SUBJECT, appInfo.appLabel + " " + appInfo.appVersionString)
            putExtra(Intent.EXTRA_TEXT, context.getString(R.string.email_text))
            if (attachment != null) {
                val contentUri = FileProvider.getUriForFile(context, "com.vargag99.lockscreenremote.fileprovider", attachment)
                // Grant permissions to all apps that can handle this intent
                val resInfoList = context.packageManager.queryIntentActivities(this, PackageManager.MATCH_DEFAULT_ONLY)
                for (resolveInfo in resInfoList) {
                    val packageName = resolveInfo.activityInfo.packageName
                    context.grantUriPermission(packageName, contentUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
                }
                putExtra(Intent.EXTRA_STREAM, contentUri)
            }
        }
    }
}