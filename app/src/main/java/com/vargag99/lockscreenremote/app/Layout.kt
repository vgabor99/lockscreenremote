package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.generic.Keep

/**
 * Layout for the remote (commands assigned to the button grid).
 * For now we support a fixed 5x5 layout (the first row is only 3 items, the other two will always be empty).
 * But I included a version, time will tell if it was useful.
 */

@Keep
data class Layout(
        @JvmField var mVersion: Int = CURRENT_VERSION, // Legacy name, but already persisted
        @JvmField var mActions: Array<String?> = arrayOf() // // Legacy name, but already persisted
) {

    companion object {
        const val CURRENT_VERSION = 1
    }

}
