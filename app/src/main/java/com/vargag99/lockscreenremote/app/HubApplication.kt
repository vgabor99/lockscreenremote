package com.vargag99.lockscreenremote.app

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.vargag99.lockscreenremote.BuildConfig
import com.vargag99.lockscreenremote.generic.CrashlyticsTree
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import java.io.IOException
import java.net.SocketException

/**
 * Application.
 */
@HiltAndroidApp
class HubApplication : Application() {
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        // Install RX error handler
        installRxErrorHandler()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Timber.plant(CrashlyticsTree())
        // Disable Crashlytics in debug build.
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)
    }

    private fun installRxErrorHandler() {
        RxJavaPlugins.setErrorHandler { error ->
            val e = if (error is UndeliverableException) error.cause else error
            when (e) {
                is IOException, // fine, irrelevant network problem or API that throws on cancellation
                is SocketException, // fine, irrelevant network problem or API that throws on cancellation
                is InterruptedException -> {
                    return@setErrorHandler
                } // fine, some blocking code was interrupted by a dispose call

                is NullPointerException, // that's likely a bug in the application
                is IllegalArgumentException, // that's likely a bug in the application
                is IllegalStateException -> { // that's a bug in RxJava or in a custom operator
                    Thread.currentThread().uncaughtExceptionHandler?.uncaughtException(Thread.currentThread(), e)
                }
            }
            Timber.w(e, "Undeliverable exception ")
        }
    }
}
