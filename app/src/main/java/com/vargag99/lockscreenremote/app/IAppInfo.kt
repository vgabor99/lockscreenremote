package com.vargag99.lockscreenremote.app

/**
 * App information, used on the UI.
 */
interface IAppInfo {
    val appLabel: String
    val appVersionString: String
    val diagnosticInfo: DiagnosticInfo
}