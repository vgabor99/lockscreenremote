package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.generic.Keep

/**
 * Prepared UI data.
 * Information about hub, activity and commands, already processed and ready for UI usage.
 */
@Keep
object UiData {

    const val TYPE_START_ACTIVITY = 1
    const val TYPE_BUTTON_COMMAND = 2

    @Keep
    data class Hub(
            @JvmField var uuid: String,
            @JvmField var friendlyName: String
    )

    @Keep
    data class Activity(
            @JvmField var id: String,
            @JvmField var label: String,
            @JvmField var icon: Icon.Id
    )

    @Keep
    data class ActionGroup(
            @JvmField var name: String,
            @JvmField var label: String,
            @JvmField var items: MutableList<ActionItem>
    )

    @Keep
    data class ActionItem(
            @JvmField var type: Int, // TYPE_START_ACTIVITY or TYPE_BUTTON_COMMAND
            @JvmField var action: String, // Action identity.
            @JvmField var label: String, // User-friendly label.
            @JvmField var icon: Icon.Id, // User-facing icon.
            @JvmField var command: String // Command payload.
    )

    @Keep
    data class ActivityConfig(
            @JvmField var hub: Hub,
            @JvmField var activity: Activity,
            @JvmField var actionGroups: MutableList<ActionGroup>
    )
}
