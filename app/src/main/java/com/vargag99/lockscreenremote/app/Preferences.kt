package com.vargag99.lockscreenremote.app

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Nullable
import androidx.preference.PreferenceManager
import com.vargag99.lockscreenremote.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

/**
 * Preferences wrapper, provides typed preference handling.
 * Note, not all preferences are visible to the user on the preferences screen.
 */
class Preferences @Inject constructor(
    @ApplicationContext private val context: Context
    ) {
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var firstRunDone: Boolean
        get() = preferences.getBoolean(FIRST_RUN_DONE, false)
        set(value) = preferences.edit()
                .putBoolean(FIRST_RUN_DONE, value)
                .apply()

    var showItsUpThere: Boolean
        get() = preferences.getBoolean(SHOW_ITS_UP_THERE, true)
        set(value) = preferences.edit()
                .putBoolean(SHOW_ITS_UP_THERE, value)
                .apply()

    var hubId: String?
        @Nullable
        get() = preferences.getString(HUB_ID, null)
        set(value) = preferences.edit()
                .putString(HUB_ID, value)
                .apply()

    var lastMissingIconCheck: Long
        get() = preferences.getLong(LAST_MISSING_ICON_CHECK, 0)
        set(value) = preferences.edit()
                .putLong(LAST_MISSING_ICON_CHECK, value)
                .apply()

    var versionCode: Int
        get() = preferences.getInt(VERSION_CODE, 0)
        set(value) = preferences.edit()
                .putInt(VERSION_CODE, value)
                .apply()

    var analyticsEnabled: Boolean
        get() = preferences.getBoolean(ANALYTICS_ENABLED, false)
        set(value) = preferences.edit()
                .putBoolean(ANALYTICS_ENABLED, value)
                .apply()

    var crashlyticsEnabled: Boolean
        get() = preferences.getBoolean(CRASHLYTICS_ENABLED, false)
        set(value) = preferences.edit()
                .putBoolean(CRASHLYTICS_ENABLED, value)
                .apply()

    fun setDefaults() {
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false)
    }

    companion object {
        const val HUB_ID = "HUB_ID"
        const val FIRST_RUN_DONE = "FIRST_RUN_DONE"
        const val SHOW_ITS_UP_THERE = "SHOW_ITS_UP_THERE"
        const val LAST_MISSING_ICON_CHECK = "LAST_MISSING_ICON_CHECK"
        const val PRO_LICENSE = "PRO_LICENSE"
        const val VERSION_CODE = "VERSION_CODE"
        const val ANALYTICS_ENABLED = "ANALYTICS_ENABLED"
        const val CRASHLYTICS_ENABLED = "CRASHLYTICS_ENABLED"
    }

}
