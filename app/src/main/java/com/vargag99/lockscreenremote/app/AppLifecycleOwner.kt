package com.vargag99.lockscreenremote.app

import androidx.lifecycle.*
import timber.log.Timber
import kotlin.properties.Delegates

/**
 * Lifecycle owner that combines tracking started activities and created services.
 * Needed because one of our UI components (the notification) lives in a service.
 * (Services manage tracking themselves, actually).
 * This object has three events, ON_CREATE, ON_START, ON_STOP
 * We get started if any of the activities are started OR a service is created.
 */
object AppLifecycleOwner : LifecycleOwner {
    data class State(val servicesCreated: Int, val activityStarted: Boolean) {
        val appStarted: Boolean
            get() = activityStarted || servicesCreated > 0
    }

    var state: State by Delegates.observable(State(0, false)) { _, old, new ->
        val oldStarted = old.appStarted
        val newStarted = new.appStarted
        when {
            !oldStarted && newStarted -> Lifecycle.Event.ON_START
            oldStarted && !newStarted -> Lifecycle.Event.ON_STOP
            else -> null
        }?.let {
            Timber.d("event $it")
            registry.handleLifecycleEvent(it)
        }
    }
    val registry = LifecycleRegistry(this)
    override fun getLifecycle() = registry

    init {
        registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        ProcessLifecycleOwner.get()
                .lifecycle
                .addObserver(object : DefaultLifecycleObserver {
                    @Suppress("unused")
                    override fun onStart(owner: LifecycleOwner) {
                        Timber.d("onStart")
                        state = state.copy(activityStarted = true)
                    }

                    @Suppress("unused")
                    override fun onStop(owner: LifecycleOwner) {
                        Timber.d("onStop")
                        state = state.copy(activityStarted = false)
                    }
                })
    }

    fun onServiceCreated() {
        Timber.d("onServiceCreated")
        state = state.copy(servicesCreated = state.servicesCreated + 1)
    }

    fun onServiceDestroyed() {
        Timber.d("onServiceDestroyed")
        state = state.copy(servicesCreated = state.servicesCreated - 1)
    }
}