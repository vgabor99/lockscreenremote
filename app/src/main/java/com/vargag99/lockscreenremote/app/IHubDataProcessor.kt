package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

/**
 * Hub data processor.
 */
interface IHubDataProcessor {

    /**
     * Received config from the hub, process it.
     */
    fun processHubConfig(hubInfo: HubInfo, config: Data.Config)
}