package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data

/**
 * Handle the missing icons (analytics report).
 */
interface IMissingIconChecker {
    fun check(config: Data.Config)
}