package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.hubManager.HubAction

/**
 * Create and consume actions.
 * The action string is a serialized form of a user intent. This is persisted into the Layout,
 * then transferred to the notification buttons, and finally executed when the button is clicked.
 */

object Actions {
    const val START_ACTIVITY_PREFIX = "vargag99_startActivity" // Magic prefix that does not conflict with any ControlGroup name.

    fun startActivityAction(activity: Data.Activity): String {
        // "vargag99_startActivity/activityId"
        return START_ACTIVITY_PREFIX + '/'.toString() + activity.id
    }

    fun buttonAction(controlGroup: Data.ControlGroup, function: Data.Function): String {
        // "ControlGroup/function"
        return controlGroup.name + '/'.toString() + function.name
    }

    fun decodeAction(activityConfig: UiData.ActivityConfig, action: String?): HubAction? {
        val item = findActionItem(activityConfig, action)
        return when (item?.type) {
            UiData.TYPE_START_ACTIVITY -> HubAction.StartActivity(item.command)
            UiData.TYPE_BUTTON_COMMAND -> HubAction.ButtonPress(item.command)
            else -> null
        }
    }

    fun findActionItem(activityConfig: UiData.ActivityConfig, action: String?): UiData.ActionItem? {
        activityConfig.actionGroups.forEach { actionGroup ->
            val item = actionGroup.items.find { action == it.action }
            if (item != null) {
                return item
            }
        }
        return null
    }
}
