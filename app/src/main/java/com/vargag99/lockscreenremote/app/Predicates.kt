package com.vargag99.lockscreenremote.app

import com.google.common.base.Predicate
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo

/**
 * Predicates.
 */

object Predicates {

    class ActivityId(private val activityId: String?) : Predicate<Data.Activity> {

        override fun apply(input: Data.Activity?): Boolean {
            return input?.id == activityId
        }
    }

    class ControlGroupName(private val name: String?) : Predicate<Data.ControlGroup> {

        override fun apply(input: Data.ControlGroup?): Boolean {
            return input?.name == name
        }
    }

    class FunctionName(private val name: String?) : Predicate<Data.Function> {

        override fun apply(input: Data.Function?): Boolean {
            return input?.name == name
        }
    }

    class HubInfoUuid(private val uuid: String?) : Predicate<HubInfo> {

        override fun apply(input: HubInfo?): Boolean {
            return input?.uuid == uuid
        }
    }
}
