package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.analytics.IAnalytics
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.persistence.IRepo
import io.reactivex.Completable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MissingIconChecker @Inject constructor(private val preferences: Preferences,
                                             private val repo: IRepo,
                                             private val analytics: IAnalytics) : IMissingIconChecker {

    override fun check(config: Data.Config) {
        // We do periodic checking infrequently (weekly), as configurations change rarely.
        val lastCheck = preferences.lastMissingIconCheck
        val now = System.currentTimeMillis()
        if (lastCheck + 7 * 24 * 60 * 60 * 1000 < now) {
            preferences.lastMissingIconCheck = now
            checkAndReport(config)
        }
    }

    private fun checkAndReport(config: Data.Config) {
        // Load the ones that are already reported.
        repo.getMissingIcons()
                .observeOn(Schedulers.computation())
                .map { missingIcons -> missingIcons.value ?: MissingIcons() }
                .flatMapCompletable { missingIcons ->
                    // Check activities.
                    var changed = false
                    config.activity.forEach { activity ->
                        if (Icon.forActivity(activity) === Icon.Id.act_Unknown) {
                            if (missingIcons.activityType.add(activity.type)) {
                                changed = true
                                reportMissingActivityIcon(activity.type, activity.label)
                            }
                        }
                    }
                    // Check functions.
                    config.activity.forEach { activity ->
                        activity.controlGroup.forEach { controlGroup ->
                            controlGroup.function.forEach { function ->
                                if (Icon.forFunction(function.name) === Icon.Id.btn_Unknown) {
                                    if (missingIcons.function.add(function.name)) {
                                        changed = true
                                        reportMissingFunctionIcon(controlGroup.name, function.name)
                                    }
                                }
                            }
                        }
                    }
                    // Save if changed, avoid duplicate reports.
                    if (changed) {
                        repo.updateMissingIcons(missingIcons)
                    } else {
                        Completable.complete()
                    }
                }
                .subscribeBy(
                        onError = { Timber.w(it, "checkAndReport failed") }
                )
    }

    private fun reportMissingActivityIcon(activityType: String, activityLabel: String) {
        Timber.d("reportMissingActivityIcon activityType=%s", activityType)
        analytics.missingIcon(IAnalytics.MISSING_ACTIVITY_ICON, "$activityType/$activityLabel")
    }

    private fun reportMissingFunctionIcon(controlGroupName: String, functionName: String) {
        Timber.d("reportMissingFunctionIcon functionName=%s", functionName)
        analytics.missingIcon(IAnalytics.MISSING_FUNCTION_ICON, "$functionName/$controlGroupName")
    }

}
