package com.vargag99.lockscreenremote.app

import android.content.Context
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.app.Actions.START_ACTIVITY_PREFIX
import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class UiDataBuilder @Inject constructor(
    @ApplicationContext private val context: Context
    ) : IUiDataBuilder {
    private val POWER_STATE_ON = "On"

    override fun build(
            hubInfo: HubInfo,
            config: Data.Config): List<UiData.ActivityConfig> {
        // Hub
        val uiHub = UiData.Hub(
                uuid = hubInfo.uuid,
                friendlyName = hubInfo.friendlyName
        )
        // Devices
        val devices = config.device.associateBy({ it.id }, { it })
        // Start activity action group
        val uiStartActivityActionGroup = createStartActivityGroup(context, config)
        // Activities
        return config.activity.map { activity ->
            // Lookup for objects that are already added.
            val uiAllActionGroups = mutableMapOf<String, UiData.ActionGroup>()
            val uiAllActions = mutableSetOf<String>()
            // Activity
            val uiActivity = UiData.Activity(
                    id = activity.id,
                    label = UiUtil.getActivityTitle(context, activity),
                    icon = Icon.forActivity(activity)
            )
            // ActivityConfig
            val uiActivityConfig = UiData.ActivityConfig(
                    hub = uiHub,
                    activity = uiActivity,
                    actionGroups = mutableListOf()
            )
            // Action items from activities.
            uiActivityConfig.actionGroups.add(uiStartActivityActionGroup)
            // Action items from activity.
            activity.controlGroup.forEach { cg ->
                mergeActions(uiActivityConfig.actionGroups, cg, uiAllActionGroups, uiAllActions)
            }
            // Action items from all powered-on devices.
            getPoweredOnDevices(activity.fixit, devices).forEach { device ->
                device.controlGroup.forEach { cg ->
                    mergeActions(uiActivityConfig.actionGroups, cg, uiAllActionGroups, uiAllActions)
                }
            }
            uiActivityConfig
        }
    }

    private fun getPoweredOnDevices(
            fixit: Map<String, Data.PowerState>?,
            devices: Map<String, Data.Device>): List<Data.Device> {
        return fixit?.values?.mapNotNull { (id, Power) ->
            if (POWER_STATE_ON == Power) {
                devices[id]
            } else null
        } ?: emptyList()
    }

    private fun mergeActions(
            target: MutableList<UiData.ActionGroup>,
            cg: Data.ControlGroup,
            allActionGroups: MutableMap<String, UiData.ActionGroup>,
            allActions: MutableSet<String>) {
        // Filter out the Power and input actions.
        // Hub logic is corrupted if we let the user manually switch power or inputs.
        when (cg.name) {
            "Power" // Power commands.
                , "Miscellaneous" // Various commands including Input. And no icons for the rest.
            -> return
        }
        // Get action group, making as needed.
        var uiActionGroup: UiData.ActionGroup? = allActionGroups[cg.name]
        if (uiActionGroup == null) {
            uiActionGroup = UiData.ActionGroup(
                    name = cg.name,
                    label = UiUtil.camelCaseSplit(cg.name),
                    items = mutableListOf()
            )
            allActionGroups[uiActionGroup.name] = uiActionGroup
            target.add(uiActionGroup)
        }
        cg.function.forEach { function ->
            // Add actionItems, unless already present (we are adding in priority order).
            val action = Actions.buttonAction(cg, function)
            if (!allActions.contains(action)) {
                val uiActionItem = UiData.ActionItem(
                        type = UiData.TYPE_BUTTON_COMMAND,
                        action = action,
                        label = function.label,
                        icon = Icon.forFunction(function.name),
                        command = function.action
                )
                allActions.add(action)
                uiActionGroup.items.add(uiActionItem)
            }
        }
    }

    private fun createStartActivityGroup(context: Context, config: Data.Config): UiData.ActionGroup {
        return UiData.ActionGroup(
                name = START_ACTIVITY_PREFIX,
                label = context.getString(R.string.activities),
                items = mutableListOf()
        ).apply {
            items.addAll(config.activity.sortedWith(UiUtil.activityComparator).map { activity ->
                UiData.ActionItem(
                        type = UiData.TYPE_START_ACTIVITY,
                        action = Actions.startActivityAction(activity),
                        label = UiUtil.getActivityTitle(context, activity),
                        icon = Icon.forActivity(activity),
                        command = activity.id
                )
            })
        }
    }
}
