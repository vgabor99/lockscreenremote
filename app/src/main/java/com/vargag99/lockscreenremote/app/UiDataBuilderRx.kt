package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.harmonyApi.Data
import com.vargag99.lockscreenremote.harmonyDiscovery.HubInfo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Reactive extensions for IUiDataBuilder
 */
class UiDataBuilderRx(private val uiDataBuilder: IUiDataBuilder) {

    fun build(hubInfo: HubInfo, config: Data.Config): Single<List<UiData.ActivityConfig>> =
            Single.fromCallable { uiDataBuilder.build(hubInfo, config) }
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
}

fun IUiDataBuilder.rx() = UiDataBuilderRx(this)