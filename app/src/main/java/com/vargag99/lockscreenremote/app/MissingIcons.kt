package com.vargag99.lockscreenremote.app

import com.vargag99.lockscreenremote.generic.Keep

/**
 * Missing icons report data.
 */

@Keep
class MissingIcons {
    @JvmField
    var activityType: MutableSet<String> = mutableSetOf()
    @JvmField
    var function: MutableSet<String> = mutableSetOf()
}
