package com.vargag99.lockscreenremote.app

import android.content.Context
import com.vargag99.lockscreenremote.ui.UiUtil
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class AppInfo @Inject constructor(
    @ApplicationContext private val context: Context
    ) : IAppInfo {
    override val appLabel: String
        get() = UiUtil.getAppLabel(context)
    override val appVersionString: String
        get() = UiUtil.getAppVersionString(context)
    override val diagnosticInfo: DiagnosticInfo
        get() = DiagnosticInfo(context)

}