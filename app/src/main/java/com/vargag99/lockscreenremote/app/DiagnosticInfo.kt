package com.vargag99.lockscreenremote.app

import android.content.Context
import androidx.annotation.NonNull

import com.vargag99.lockscreenremote.generic.Keep
import com.vargag99.lockscreenremote.ui.UiUtil

/**
 * Diagnostic information. Sent in with a bug report.
 */

@Keep
data class DiagnosticInfo(
        @JvmField val build: Build,
        @JvmField val app: App
) {

    constructor(@NonNull context: Context) : this(
            build = Build(),
            app = App(context)
    )

    @Keep
    data class Build(
            @JvmField val id: String = android.os.Build.ID,
            @JvmField val display: String = android.os.Build.DISPLAY,
            @JvmField val product: String = android.os.Build.PRODUCT,
            @JvmField val device: String = android.os.Build.DEVICE,
            @JvmField val board: String = android.os.Build.BOARD,
            @JvmField val manufacturer: String = android.os.Build.MANUFACTURER,
            @JvmField val brand: String = android.os.Build.BRAND,
            @JvmField val model: String = android.os.Build.MODEL,
            @JvmField val type: String = android.os.Build.TYPE,
            @JvmField val tags: String = android.os.Build.TAGS,
            @JvmField val version: BuildVersion = BuildVersion()
    )

    @Keep
    data class BuildVersion(
            @JvmField val release: String = android.os.Build.VERSION.RELEASE,
            @JvmField val sdkInt: Int = android.os.Build.VERSION.SDK_INT
    )

    @Keep
    data class App(
            @JvmField val packageName: String,
            @JvmField val versionName: String
    ) {
        constructor(@NonNull context: Context) : this(
                packageName = context.applicationInfo.packageName,
                versionName = UiUtil.getAppVersionString(context)
        )
    }

    companion object {
        val TAG: String = DiagnosticInfo::class.java.simpleName
    }

}
