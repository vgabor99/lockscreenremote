package com.vargag99.lockscreenremote.app

import androidx.annotation.NonNull
import com.google.common.collect.ImmutableMap
import com.vargag99.lockscreenremote.R
import com.vargag99.lockscreenremote.generic.Keep
import com.vargag99.lockscreenremote.harmonyApi.Data
import java.util.regex.Pattern

/**
 * Icons for commands.
 * We persist the icon into UiData, so we cannot use R.drawable resource directly, because that is
 * not stable across builds. So we do an extra mapping to a stable Id.
 */

object Icon {

    private val functionMap = ImmutableMap.builder<String, Id>()
            // NumericBasic
            .put("Number0", Id.Number0)
            .put("Number1", Id.Number1)
            .put("Number2", Id.Number2)
            .put("Number3", Id.Number3)
            .put("Number4", Id.Number4)
            .put("Number5", Id.Number5)
            .put("Number6", Id.Number6)
            .put("Number7", Id.Number7)
            .put("Number8", Id.Number8)
            .put("Number9", Id.Number9)
            .put("Dot", Id.NumberDot)
            .put("NumberEnter", Id.NumberEnter)
            .put("Hyphen", Id.Hyphen)

            // Volume
            .put("Mute", Id.Mute)
            .put("VolumeDown", Id.VolumeDown)
            .put("VolumeUp", Id.VolumeUp)

            // Volume
            .put("VolumeCenterDown", Id.VolumeCenterDown)
            .put("VolumeCenterUp", Id.VolumeCenterUp)
            .put("VolumeRearDown", Id.VolumeRearDown)
            .put("VolumeRearUp", Id.VolumeRearUp)

            // Channel
            .put("PrevChannel", Id.PrevChannel)
            .put("ChannelDown", Id.ChannelDown)
            .put("ChannelUp", Id.ChannelUp)

            // NavigationBasic
            .put("DirectionDown", Id.DirectionDown)
            .put("DirectionLeft", Id.DirectionLeft)
            .put("DirectionRight", Id.DirectionRight)
            .put("DirectionUp", Id.DirectionUp)
            .put("Select", Id.Select)

            // NavigationExtended
            .put("Guide", Id.Guide)
            .put("Info", Id.Info)
            .put("PageDown", Id.PageDown)
            .put("PageUp", Id.PageUp)
            .put("Cancel", Id.Cancel)
            .put("Exit", Id.Exit)

            // NavigationDVD
            .put("Return", Id.Return)
            .put("Menu", Id.Menu)
            .put("Subtitle", Id.Subtitle)
            .put("Back", Id.Back)

            // GameType3
            .put("Home", Id.Home)

            // NavigationDSTB
            .put("List", Id.List)
            .put("Search", Id.Search)
            .put("A", Id.A)
            .put("B", Id.B)
            .put("C", Id.C)
            .put("D", Id.D)
            .put("Favorite", Id.Favorite)
            .put("Live", Id.Live)

            // TransportBasic
            .put("Stop", Id.Stop)
            .put("Play", Id.Play)
            .put("Rewind", Id.Rewind)
            .put("Pause", Id.Pause)
            .put("FastForward", Id.FastForward)
            .put("Eject", Id.Eject)

            // TransportExtended
            .put("SkipBackward", Id.SkipBackward)
            .put("SkipForward", Id.SkipForward)

            // TransportRecording
            .put("Record", Id.Record)

            // Teletext
            .put("Teletext", Id.Teletext)

            // Radio Tuner
            .put("PrevPreset", Id.PrevPreset)
            .put("NextPreset", Id.NextPreset)
            .put("ScanUp", Id.ScanUp)
            .put("ScanDown", Id.ScanDown)
            .put("DirectTune", Id.DirectTune)

            // Display Mode
            .put("Aspect", Id.Aspect)

            // ColoredButtons
            .put("Green", Id.Green)
            .put("Red", Id.Red)
            .put("Blue", Id.Blue)
            .put("Yellow", Id.Yellow)

            // Sound Mode
            .put("SoundMode", Id.SoundMode)

            // Setup
            .put("Sleep", Id.Sleep)
            .put("Setup", Id.Setup)

            // Google TV Navigation
            .put("Settings", Id.Settings)

            .build()

    // Activity label matching. The order is important (first match wins).
    private val labelPatterns = arrayOf(
            PatternMatcher("(?i)(?:^|\\W)amazon(?:$|\\W)", Id.act_Amazon),
            PatternMatcher("(?i)(?:^|\\W)netflix(?:$|\\W)", Id.act_Netfix),
            PatternMatcher("(?i)(?:^|\\W)hulu(?:$|\\W)", Id.act_Hulu),
            PatternMatcher("(?i)(?:^|\\W)spotify(?:$|\\W)", Id.act_Spotify),
            PatternMatcher("(?i)(?:^|\\W)(playstation|ps\\d)(?:$|\\W)", Id.act_Playstation),
            PatternMatcher("(?i)(?:^|\\W)xbox(?:$|\\W)", Id.act_Xbox),
            PatternMatcher("(?i)(?:^|\\W)vevo(?:$|\\W)", Id.act_Vevo),
            PatternMatcher("(?i)(?:^|\\W)twitch(?:$|\\W)", Id.act_Twitch),
            PatternMatcher("(?i)(?:^|\\W)crackle(?:$|\\W)", Id.act_Crackle),
            PatternMatcher("(?i)(?:^|\\W)kodi(?:$|\\W)", Id.act_Kodi),
            PatternMatcher("(?i)(?:^|\\W)plex(?:$|\\W)", Id.act_Plex),
            PatternMatcher("(?i)(?:^|\\W)apple", Id.act_Apple),
            PatternMatcher("(?i)(?:^|\\W)chrome", Id.act_Chrome),
            PatternMatcher("(?i)(?:^|\\W)youtube(?:$|\\W)", Id.act_Youtube),
            PatternMatcher("(?i)(?:^|\\W)wii(?:$|\\W)", Id.act_Wii),
            PatternMatcher("(?i)(?:^|\\W)radio(?:$|\\W)", Id.act_Radio),
            PatternMatcher("(?i)(?:^|\\W)roku(?:$|\\W)", Id.act_Roku),
            PatternMatcher("(?i)(?:^|\\W)tivo(?:$|\\W)", Id.act_Tivo),
            PatternMatcher("(?i)(?:^|\\W)disney(?:$|\\W)", Id.act_Disney),
            PatternMatcher("(?i)(?:^|\\W)android", Id.act_Android))

    // Activity type matching. The order is important (first match wins).
    private val typePatterns = arrayOf(
            PatternMatcher("(?:^|\\W)Television(?:$|\\W)", Id.act_Tv),
            PatternMatcher("(?:^|\\W)Game(?:$|\\W)", Id.act_Game),
            PatternMatcher("(?:^|\\W)Dvd(?:$|\\W)", Id.act_Movie),
            PatternMatcher("(?:^|\\W)Cd(?:$|\\W)", Id.act_Music),
            PatternMatcher("(?:^|\\W)Generic(?:$|\\W)", Id.act_Generic))

    // Activity type exact matching.
    private val activityMap = ImmutableMap.builder<String, Id>()
            .put("PowerOff", Id.act_PowerOff)
            .put("VirtualTelevisionN", Id.act_Tv)
            .put("VirtualDvd", Id.act_Movie)
            .put("VirtualCdMulti", Id.act_Music)
            .put("VirtualGeneric", Id.act_Generic)

            .build()

    //
    // Icon id for function.
    //

    fun forFunction(function: String): Id {
        return functionMap[function] ?: Id.btn_Unknown
    }

    //
    // Icon id for activity.
    //

    fun forActivity(activity: Data.Activity): Id {
        return labelPatterns.findMatch(activity.label) ?: // Activity label pattern match.
        activityMap[activity.type] ?: // Activity type exact match.
        typePatterns.findMatch(activity.type) ?: // Activity type pattern match.
        Id.act_Unknown// Final catch-all.
    }

    internal class PatternMatcher(regexp: String, val id: Id) {
        val pattern: Pattern = Pattern.compile(regexp, 0)
    }

    private fun Array<PatternMatcher>.findMatch(text: String): Id? {
        return find { it.pattern.matcher(text).find() }?.id
    }

    //
    // Stable icon id (maps to drawable id).
    //
    @Keep
    enum class Id constructor(@JvmField val mResId: Int) { // Legacy name, already persisted.

        //
        // Activities
        //

        act_Unknown(R.drawable.act_unknown),

        act_Amazon(R.drawable.act_amazon),
        act_Netfix(R.drawable.act_netflix),
        act_Hulu(R.drawable.act_hulu),
        act_Spotify(R.drawable.act_spotify),
        act_Playstation(R.drawable.act_playstation),
        act_Xbox(R.drawable.act_xbox),
        act_Vevo(R.drawable.act_vevo),
        act_Twitch(R.drawable.act_twitch),
        act_Crackle(R.drawable.act_crackle),
        act_Kodi(R.drawable.act_kodi),
        act_Plex(R.drawable.act_plex),
        act_Apple(R.drawable.act_apple),
        act_Chrome(R.drawable.act_chrome),
        act_Youtube(R.drawable.act_youtube),
        act_Wii(R.drawable.act_wii),
        act_Radio(R.drawable.act_radio),
        act_Roku(R.drawable.act_roku),
        act_Tivo(R.drawable.act_tivo),
        act_Disney(R.drawable.act_disney),
        act_Android(R.drawable.act_android),
        act_PowerOff(R.drawable.act_power_off),
        act_Tv(R.drawable.act_tv),
        act_Movie(R.drawable.act_movie),
        act_Music(R.drawable.act_music),
        act_Game(R.drawable.act_game_controller),
        act_Generic(R.drawable.act_generic),

        //
        // Functions
        //

        btn_Unknown(R.drawable.btn_unknown),

        // NumericBasic
        Number0(R.drawable.btn_number_0),
        Number1(R.drawable.btn_number_1),
        Number2(R.drawable.btn_number_2),
        Number3(R.drawable.btn_number_3),
        Number4(R.drawable.btn_number_4),
        Number5(R.drawable.btn_number_5),
        Number6(R.drawable.btn_number_6),
        Number7(R.drawable.btn_number_7),
        Number8(R.drawable.btn_number_8),
        Number9(R.drawable.btn_number_9),
        NumberDot(R.drawable.btn_number_dot),
        NumberEnter(R.drawable.btn_keyboard_return),
        Hyphen(R.drawable.btn_hyphen),

        // Volume
        Mute(R.drawable.btn_volume_off),
        VolumeDown(R.drawable.btn_volume_down),
        VolumeUp(R.drawable.btn_volume_up),

        // Volume Extended
        VolumeCenterDown(R.drawable.btn_volume_down),
        VolumeCenterUp(R.drawable.btn_volume_up),
        VolumeRearDown(R.drawable.btn_volume_down),
        VolumeRearUp(R.drawable.btn_volume_up),

        // Channel
        PrevChannel(R.drawable.btn_prev),
        ChannelDown(R.drawable.btn_chevron_down),
        ChannelUp(R.drawable.btn_chevron_up),

        // NavigationBasic
        DirectionDown(R.drawable.btn_arrow_down_thick),
        DirectionLeft(R.drawable.btn_arrow_left_thick),
        DirectionRight(R.drawable.btn_arrow_right_thick),
        DirectionUp(R.drawable.btn_arrow_up_thick),
        Select(R.drawable.btn_circle_outline),

        // NavigationExtended
        Guide(R.drawable.btn_television_guide),
        Info(R.drawable.btn_information),
        PageDown(R.drawable.btn_chevron_double_down),
        PageUp(R.drawable.btn_chevron_double_up),
        Cancel(R.drawable.btn_cancel),

        Exit(R.drawable.btn_exit),

        // NavigationDVD
        Return(R.drawable.btn_keyboard_return),
        Menu(R.drawable.btn_menu),
        Subtitle(R.drawable.btn_subtitles),
        Back(R.drawable.btn_back),

        // GameType3
        Home(R.drawable.btn_home),

        // NavigationDSTB
        List(R.drawable.btn_list),
        Search(R.drawable.btn_search),
        A(R.drawable.btn_a),
        B(R.drawable.btn_b),
        C(R.drawable.btn_c),
        D(R.drawable.btn_d),
        Favorite(R.drawable.btn_favourite),
        Live(R.drawable.btn_live_tv),

        // TransportBasic
        Stop(R.drawable.btn_stop),
        Play(R.drawable.btn_play),
        Rewind(R.drawable.btn_rewind),
        Pause(R.drawable.btn_pause),
        FastForward(R.drawable.btn_fast_forward),

        Eject(R.drawable.btn_eject),

        // TransportExtended
        SkipBackward(R.drawable.btn_skip_previous),
        SkipForward(R.drawable.btn_skip_next),

        // TransportRecording
        Record(R.drawable.btn_record),

        // Teletext
        Teletext(R.drawable.btn_teletext),

        // Radio Tuner
        PrevPreset(R.drawable.btn_chevron_left),
        NextPreset(R.drawable.btn_chevron_right),
        ScanUp(R.drawable.btn_chevron_double_right),
        ScanDown(R.drawable.btn_chevron_double_left),
        DirectTune(R.drawable.btn_tune),

        // Display Mode
        Aspect(R.drawable.btn_aspect_ratio),

        // ColoredButtons
        Green(R.drawable.btn_green),
        Red(R.drawable.btn_red),
        Blue(R.drawable.btn_blue),
        Yellow(R.drawable.btn_yellow),

        // Sound Modes
        SoundMode(R.drawable.btn_equalizer),

        // Setup
        Sleep(R.drawable.btn_sleep),
        Setup(R.drawable.btn_wrench),

        // Google TV Navigation
        Settings(R.drawable.btn_settings)

    }

}
