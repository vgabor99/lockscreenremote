
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable,Signature,*Annotation*

-dontpreverify
-repackageclasses 'com.vargag99.lockscreenremote'
-allowaccessmodification
-optimizations !code/simplification/arithmetic

#Guava
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe
-dontwarn com.google.common.**
-dontwarn java.lang.ClassValue

#Logger
-dontwarn org.slf4j.**

#asmack
-keep class de.measite.smack.AndroidDebugger { *; }
-keep class * implements org.jivesoftware.smack.initializer.SmackInitializer
-keep class * implements org.jivesoftware.smack.provider.IQProvider
-keep class * implements org.jivesoftware.smack.provider.PacketExtensionProvider
-keep class * extends org.jivesoftware.smack.packet.Packet
-keep class org.jivesoftware.smack.** { *; }
-keep class org.jivesoftware.smackx.** { *; }

# @Keep annotation (same as @android.support.annotation.Keep but with runtime retention)
-keep class com.vargag99.lockscreenremote.generic.Keep
-keep @com.vargag99.lockscreenremote.generic.Keep class * {*;}
-keepclasseswithmembers class * {
    @com.vargag99.lockscreenremote.generic.Keep <methods>;
}
-keepclasseswithmembers class * {
    @com.vargag99.lockscreenremote.generic.Keep <fields>;
}
-keepclasseswithmembers class * {
    @com.vargag99.lockscreenremote.generic.Keep <init>(...);
}

# ObjectAnimator uses this
-keepclassmembernames class androidx.appcompat.graphics.drawable.DrawerArrowDrawable {
    void setProgress(float);
    float getProgress();
}